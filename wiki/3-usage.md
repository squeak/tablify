# Usage

```js
tablify(options)
```

Or in more details:
```javascript
var tablify = require("tablify");
tablify({

  //
  //                              GENERAL SETTINGS
  name: "tablify-test-db",
  $container: $tablifyContainer,
  $toolbarContainer: $tablifyContainer,

  //
  //                              DISPLAY SETTINGS

  display: {
    color: "red",
  },

  //
  //                              LIST OF COLUMS IN TABLE DISPLAY

  columns: [
    {
      key: "date",
      grouping: function (model) {
        var date = model.get("date");
        if (date) return $$.match(date, /^\d\d\d\d-\d\d/);
      },
      reverse: true,
      sort: ["date"],
    },
    {
      key: "person",
      grouping: function (model, sortValue) {
        return $$.match(sortValue, /^./);
      },
    },
    {
      key: "country",
      grouping: true,
      display: function (entry, options) {
        return myCustomFagDisplayFunc(entry.country)
      },
    },
    {
      key: "comment",
    },
  ],

  //
  //                              TYPE OF ENTRIES THAT CAN BE ADDED IN DB

  entryTypes: [
    {
      name: "person-type", // the type entries will have
      title: function (entry) { return entry.person; },
      subtitle: function (entry) { return entry.country; },
      icon: "accessibility",
      color: function (entry) { return $$.random.color(1); },

      // LIST OF INPUTS IN EDITION WINDOW
      edit: {
        structure: [
          {
            type: "date",
            name: "date",
          },
          {
            type: "normal",
            name: "person",
          },
          {
            type: "select",
            name: "country",
            choices: ["China", "Italy", "Ethiopia", "India"],
          },
          {
            type: "long",
            name: "comment",
          },
        ],
      },

      // VIEW WINDOW SETUP
      view: {
        fields: [
          { deepKey: "date", },
          { deepKey: "person", },
          { deepKey: "country", },
          {
            display: function (entry) { return $$.string.htmlify(entry.comment); },
            css: {
              background: "black",
              color: "white",
            },
          },
        ]
      },

    }
  ],

  //                              ¬
  //

  // ... check out the whole list of tablifyOptions in the API page
});
```
