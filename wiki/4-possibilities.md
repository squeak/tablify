# What you can use tablify for?

Tablify make it easy to do all the following things, from a GUI, with no or minimum programming knowledge:
  - handle a database's entries, edit them, add new ones
  - browse entries lists
  - display entries individually in a nice way
  - search and filter entries in the database
  - bulk modify entries with raw json edition
  - bulk modify entries with processing methods
  - synchronize databases with remotes
  - and more...

When you setup tablify, you can customize how all the previous actions will take place, to make it as adapted as possible to your needs.
