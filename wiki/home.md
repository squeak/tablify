
Display and edit a database's entries in a nice way!

### With features like:
  - customizable display: *table*, *gallery*, *free*...
  - powerful edition with nice and customizable display, raw edition, bulk processing...
  - totally customize how to show entries views.

### and the power of pouchdb:
Tablify will automatically create a database in the browser's local storage (using [pouchdb](https://pouchdb.com/)). This make it very convenient and straightforward to synchronize with [couchdb](https://couchdb.apache.org/) databases.

### Wanna see a tablify powered app?
A great demonstration of the power of tablify is [the dato app](https://publicdato.eauchat.org/), an app you can use to store and organize any kind of content and customize the way to interact with it, view it, edit it...
