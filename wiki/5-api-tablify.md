# API - tablify

🛈 This page is written using squyntax notation, information/documentation about it is available [here](https://squeak.eauchat.org/libs/squyntax/). 🛈


## Tablify options

Here are all the options you can pass to tablify function:

```javascript
tablifyOptions = <{

  //
  //                              GENERAL

  !name: <string> « this will be used to name the pouch database and keep information of this database in the browser local storage »,
  !$container: <yquerjObject, dom element, should also work with jquery objects>,
  ?class: <string|function(ø){@this=tablifyApp}:string>,
  ?pouchsInCache: <{ [pouchName <string>]: <pouchdb> }> «
    a list of pouch databases, if one with the same name as the one set in those options, will not create a new pouchdb but use this one
  »,
  ?keyboardifyContext: <string> « instructs tablify in which keyboardify contexts it operates, so it can know where to rightly get its shortcuts list »,

  ?$editorContainer: <yquerjObject> « custom dom element in which to append editor windows »,
  ?$viewContainer: <yquerjObject> « custom dom element in which to append view windows »,

  ?remotes: <tablifyRemote[]|function(){@this=tablifyApp}:<tablifyRemote[]>> «
    remotes to synchronize db with (if synchronizationTools is true, these are added to the ones the user can add)
    each entry should contain valid couchdb database url, if the database is not public, you may also need to set username and password
  »,
  ?processRemotes: <function(remotes<tablifyRemote[]>):<tablifyRemote[]>> « pass any function here to modify the list of remotes »,

  ?synchronizationTools: <boolean|"statusOnly">@default=true «
    if true, will display turning arrow button to display/consult sync status and edition button
    if "statusOnly", will display only turning arrow
    if false, will not display sync tools
  »,
  ?synchronizationCredentialsList: <{
    [<string> « couch db or server url » ]: {
      username: <string>,
      password: <string>,
    },
  }>

  ?pathsDatabase: <boolean>@default=false «
    if true, all entries will (have to) have a path key
    ⚠ the path value in entries must be an array of path strings
    (if this is true, and no path input is set in an entry type, it will be added automatically)
    ⚠⚠⚠ if you set this to true, you should probably not forget to add in "entryTypes", the type of your folder-like entries, also you should probably specify their name in "pathsDatabaseFolderEntry" so that tablify can auto-configure them to behave like folders
  »,
  ?pathsDatabaseFolderEntry: <string> « if "pathsDatabase" is true, you can specify here the name of your folder-like entries, this option will add the appropriate fields to their type »,

  ?disableEditing: <boolean>@default=false « if true, entries will not be editable »,

  //
  //                              ENTRIES TYPE CONFIGURATION

  ?availableTypes: <string[]|function():<string[]>>@default=_.pluck(tablifyOptions.entryTypes, "name") «
    useful only if editor is a function needing to receive a type to return the appropriate postify configuration
    will show a popup to let the user chose which type of entry they want to create
  »,
  !entryTypes: <entryType[]> «
    available entry types, a list of object, each containing settings defining how to display entries, edit them...
    you should list here all the entries types that can be found in your database
    Note: you can omit this option, but tablify will fall back on the "defaultType" option to figure out the display of your entries, and it will probably not give the result you expect
  »,
  ?defaultType: <entryType> « the default settings to use, if type cannot be found or some values are missing »,

  //
  //                              SORT

  ?display: <{

    //
    //                              APPEARANCE

    // title and layout
    ?title: <string>@default=tablifyOptions.name « the title displayed in the footer bar »,

    // color and background
    ?color: <string> « color to give to the database footer »,
    ?background: <string|{ !attachmentId: <string>, ... }> «
      | background url
      | object containing the id of the attachment that is the background, the attachment should be stored as a couchdb attachment in tablifyOptions._attachments[attachmentId]
    »,
    ?backgroundCredits: <htmlString|function(tablifyOptions.display.background):<htmlString>> « some credits to display for the background »,
    ?icon: <string> « an icon to prepend to database title »,
    ?cssRules: <cssRules> « pass here a list of custom css rules to apply to this table (useful for programmatic usage of tablify) »,
    ?defaultEntryIcon: <string> « the icon to use by default on entries, if they don't have any icon set »,

    //
    //                              LAYOUT

    ?layouts: <supportedLayout[]|function(ø){@this=tablifyApp}:<supportedLayout[]>>@default=supportedLayouts «
      the list of available layouts for this database,
      if there is more than one, it will display a button to choose which one the user want to use,
      if the next option (currentLayout) is not defined, the first value in this list will be used as current layout,
      in the "free" layout, the position of the entry in the page is saved in the entry under entry.system.position, it's an object of type: <{ x: <nubmer>, y: <number>, z: <number> }
    »,
    ?currentLayout: <supportedLayout>@default=tablifyOptions.layout.available[0] « the layout currently used »,
    ?columns: <column[]> « settings for table display »,

    //
    //                              ORGANIZATION

    // sorting, grouping, filtering, limiting
    ?sorting: <sorting> « is not used for table display, each columns sorting is used instead »,
    ?sortReverse: <boolean>@default=false,
    ?grouping: <grouping>,
    ?groupingCustomDisplay: <groupingCustomDisplay>,
    ?searching: <searching>,
    ?filter: <function(model):<boolean>> « constant filter applied to the database (current filtering is applied on top of it) »,
    ?limit: <number>@default=undefined,
    ?rememberLimit: <number>@default=true « if true, tablify will use local storage to remember the set limit, if no previous setting on that, will use the chosen limit »

    // current state
    ?currentFilter: <see filter> « the filter currently displayed »,
    ?currentPath: <string> « filter only entries contained in this directory »,
    ?rest: <integer> « number of entries to skip »,
    ?currentSelectedColumn: <>@default:tablifyOptions.display.columns[0].key,

    //
    //                              ADVANCED

    ?fab: <"all"|"touchDevices"|"none">@default="touchDevices" « when to display the add/edit entries fab »,
    ?buttons: <uify.button·options[]> « a list of custom buttons to display in toolbar »,
    ?hiddenEntries: <object|function(backboneModel):<boolean>> «
      entries matching this pattern will be hidden by default, to toggle their display the user must press shift+u
      if this is a function, if the function returns true, the entry will be hidden
      if this is an object, if all key pairs of the object are present in the entry, the entry will be hidden
    »,
    ?bodyHeader: <string|function($bodyHeaderContainer){@this=tablifyApp}:<void>> «
      some message to display at the top of the body (before list of entries)
      if string, will append this string to $bodyHeaderContainer
      passing a function let you do what you want to $bodyHeaderContainer
    »,

    //                              ¬
    //

  }>,

  //
  //                              PROCESS DISPLAY OF FOLDER

  ?folderDisplayProcess: <function(folderDisplayObject<tablifyOptions.display>):<tablifyOptions.display> « custom processing to apply to a folder display object »,

  //
  //                              ACTIONS AND SHORTCUTS

  ?actions: <actionsConfig|function():<actionConfig>> « list of entry actions supported and the way to trigger them »,
  ?additionalShortcuts: <{
    !keycombo: <string> « the key combo that will trigger the shortcut »,
    !keycomboId: <string> « an id to uniquely identify the shortcut »,
    !description: <string> « what this shortcut is doing »,
    !action: <function(App<tablifyApp>)|keyof tablifyApp>,
  }[]> «
    a list of custom shortcuts to use
    any shortcut that has the same keycombo than a default one will overwrite the default
  »,

  //
  //                              TOOLBAR

  |$toolbarContainer: <yquerjProcessable>
  |toolbarToUse: <uify.toolbar return> « custom toolbar to use for tablify icons, if none provided one will be created in $toolbarContainer, and if not provided, in $container »,
  ?toolbarOptions: <uify.toolbar/options> « custom options to pass to the toolbar (only for a newly created toolbar = ignored when toolbarToUse is defined) »,
  ?toolbarButtons: <uify.button·options[]> « list of custom buttons to add in toolbar »,
  ?toolbarHelpCustomize: <function(defaultMenuButtons<uify.button·options[]>){@this=tablifyApp}:<uify.button·options[]> « use this to customize the list of buttons to show in the help menu »,
  ?toolbarBatchRemoveDisable: <boolean> « if this is true, will not show entries batch remove button in the toolbar »,

  //
  //                              TUTORIAL

  ?tutorial: <{
    !presentationText: <string> « the text that will be shown in the dialog asking the user if they are ready to start »,
    !steps: <function(defaultSteps<tutorialStep[]>):tutorialStep[]> «
      use this to customize the default tutorial on how to use this page
      just return the list of steps you want to be shown when the user asks to see the help tutorial
    »,
  }>
  ?$tutorialContainer <yquerjProcessable> « custom container in which to put tutorial bubbles »,

  //
  //                              ADDITIONAL OPTIONS

  ?rowEvents: <{ [eventName]: <eventFunc(event)this=Entry> }> « events fired when something happens to rows »,
  ?rowSelect: <function(model<backboneModel>){@this=tablifyApp}> « fired when a row is selected »,

  ?callback: <function(App)> « fired when app is first created, after collection is fetched and displayed »,
  ?renderCallback: <function(App)> « fired every time the list of entries is rendered »,

  ?plugins: <function(App, callback)[]> «
    a list of functions to extend or modify tablify functionalities
    each function will in order be ran before the first refreshing of the collection (displaying of entries)
    each function must run its callback, to notify tablify to continue loading of the app after all have finished running
  »,

  ?changeCallback: <changeCallbackFunction>,

  //
  //                              EVENTS

  onEntrySelect: <function(ø){@this=tablifyApp}> « ran when one or multiple entries are selected »,
  onEntryOpen: <function(ø){@this=tablifyApp}> « ran when an entry is opened »,
  onEntryEdit: <function(ø){@this=tablifyApp}> « ran when an entry is opened for edition »,

  //                              ¬
  //

}>
```

Types used in `tablifyOptions`:
```javascript

//
//                              ENTRY TYPE

entryType = <{

  name: <string> « the name of this type as specified in entry "type" key »,

  //
  //                              DISPLAY

  ?display: <{

    // GENERAL
    ?title: <typeConfigDisplay> « title shown in edit and view windows menu bars, or in entry vignette... »,
    ?subtitle: <typeConfigDisplay> « subtitle shown in edit and view windows menu bars, or in entry vignette... »,
    ?color: <typeConfigDisplay> « color of the entry everywhere it is displayed or edited »,
    ?icon: <typeConfigDisplay> « icon of the entry everywhere it is displayed or edited »,
    ?image: <typeConfigDisplay> « image of the entry everywhere it is displayed or edited »,
    ?additional: <{

      // GENERAL
      ?secondaryColor: <typeConfigDisplay> « this color will be used in view and edit windows menu bars »,

      // LIST-LIKE VIEW
      ?buttons: <uify·button.options[]> « custom buttons to display in listed entries, in addition to the edit quill (showed only in list-like view) »,

      // GEO SHAPES
      ?geoKey: <typeConfigDisplay> « if this entry should be displayed on a map, at which key are geo coordinates information (and geometry infos) stored in these entries »,
      ?weight: <typeConfigDisplay> «
        If this entry is a displayable shape path, how to define the weight of its stroke (stroke weight is counted in pixels).
        If it is a marker/point shape, will determine how big to display it (point weight is counted in tens of pixels).
      »,
      ?strokeDashArray: <typeConfigDisplay> «
        If this entry is a displayable shape path, how to define the style of its stroke.
        This lets you create complex dash patterns. You can set a single number or multiple ones to make more complex dash patterns (e.g. 5,3,2).
        Search stroke-dasharray svg option on internet for more details.,
      »,
      ?fillColor: <typeConfigDisplay> « If this entry is a displayable shape path, use this to customize the color of its filling (by default it would be the same as generally set color). »,
      ?fillOpacity: <typeConfigDisplay> « If this entry is a displayable shape path, use this to customize the opacity of its filling. »,

    }>,

    // TYPE DISPLAY WHEN CREATING AN ENTRY
    creationMenu: <{
      name: <string> « he name to display when creating a new entry of this type »,
      icon: <string> « an icon to display by the name of the type »,
      key: <string> « a shortcut key to press to select that type in the entry creation menu »,
    }>,

  }>,

  //
  //                              ACTIONS

  ?action: <{

    ?edit: <function(
      model <backboneModel> « model of the entry to edit »,
      typeConfig <entryType> « type configuration of the entry to edit »,
      isNewEntry <boolean> « true if the entry is a newly created entry »,
    ){@this=tablifyApp}> « modify the default behaviour when user asks to edit an entry (default behaviour is to open edition window == postify) »,

    ?view: <function(
      model <backboneModel> « model of the entry to view »,
      typeConfig <entryType> « type configuration of the entry to edit »,
    ){@this=tablifyApp} « modify the default behaviour when user asks to view an entry (default behaviour is to open view window == viewify) »

    ?storeInHash: <<"edit"|"view">[]>@default=["edit", "view"] « actions that should be stored in url hash »,

  }>,

  //
  //                              EVENTS

  ?event: <{

    ?saved: <function(
      entryAsSaved <couchEntry> « the entry as it has been saved »,
      entryBeforeModifs <couchEntry> « the entry how it was before being modified »,
      isNewEntry <boolean> « true if the entry is a newly created entry »,
    ){@this=tablifyApp}:<void>> « a callback that will be executed if an entry of this type has been saved »,

    ?removed: <function(
      entryRemoved <couchEntry> « the entry that has been removed »,
    ){@this=tablifyApp}:<void>> « a callback that will be executed if an entry of this type has been removed »,

    ?moved: <function(
      pathIdBefore <string> « the id of the folder in which this entry was before »,
      pathIdAfter <string> « the id of the folder in which this entry has been put »,
    ){@this=tablifyApp}:<void>> « a callback that will be executed if an entry of this type has been moved »,

    ?changed: <function(
      entry <couchEntry> « the entry that has been changed »,
      isDeleted <boolean> « true if the entry was deleted »,
    ){@this=tablifyApp}:<void>> « a callback that will be executed when an entry of this type has been modified (moved, removed or saved) »,

  }>,

  //
  //                              EDIFY

  edit: <{
    !structure: <inputify·options[]> « list of inputs to display to edit this entry »,
    ?buttons: <uify·button.options[]> « custom buttons to add to edition window »,
    ?doNotAutoAddPath: <boolean> « if true, will not add path input in edition window »,
    // ... other keys supported by postify
    // this object will be passed to postify (prefilled by tablify, be careful not to put things here that could interfere with postify)
  }>,

  //
  //                              VIEWIFY

  view: <{
    !fields: <viewify·field[]> « view window fields (see in https://squeak.eauchat.org/libs/viewify/?api for details) »,
    ?buttons: <uify.buttons·options> « a list of custom buttons to display in the menu bar »,
  }>,

  //                              ¬
  //

}>;

//
//                              TYPE CONFIG DISPLAY FUNCTION OR STRING

typeConfigDisplay = <string|function(entry, typeConfig){@this=backboneModel}:<string>>;

//
//                              CHANGE CALLBACK FUNCTION

changeCallbackFunction = <function(entry <couchEntry>):<void>> «
  executed at any change of entry in the database,
  this is executed as well on entry deletions, in the entry, check if the "_deleted" key is present to know if it was a deletion
»;

//
//                              TABLIFY REMOTE

tablifyRemote = <{
  !url: <string> « valid couchdb database url (for example: https://couchdb-domain.tld/database-name/) »,
  ?username: <string> « if database is private, the name of a user on this couch server that has access to this database »,
  ?password: <string> « if database is private, the password for this user »,
  ?filter: <function(entry):<boolean>|object> «
    a filter to apply to the synchronization,
    this means that any entry that doesn't pass this filter, will not be synchronized from and to the remote,
    if this is a function, if the function returns true, entry is synced, else not
    if this is an object, it will filter entries that should be synced comparing them with this passed filter using _.where underscore method (see https://underscorejs.org/#where for details)
  »,
}>

//
//                              COLUMN

column = <{

  // GENERAL
  !key: <string>,
  ?label: <string> « if specified, this value will be the displayed columns title (=header) »,
  ?class: <string|function(model<backboneModel>):string>,

  // SORT
  ?grouping: <grouping>,
  ?groupingCustomDisplay: <groupingCustomDisplay>,
  ?sorting: <sorting>,
  ?sortReverse: <boolean>@default=false,
  ?searching: <searching>,

  // DISPLAY AND ACTIONS
  ?display: <function(model<backboneModel>, $cell<yquerjObject&undefined>):<any|void>> «
    there are two ways to use this function:
      - return with it what you want to be assigned as html in $cell (if you return something else than a string or a number, the value will be processed with $$.json.pretty and then with $$.string.htmlify)
      - do not return and do whatever change you want to $cell (if you return something, the return may override the other changes you made)
  »,
  ?cssProperties: <string> « pass here a list of custom css rules to apply to this table column cells (useful for programmatic usage of tablify) »,
  ?events: <{ [eventName]: <eventFunc(event)this=Entry> }> « events fired when something happens to cells »,
  ?click: <function(
    model<backboneModel>,
    $cell<yquerjObject>,
    event
  )this=Entry> « fired when cell is clicked (redundant with events options before, but is here for convenience (it has more arguments passed)) »,
  ?renderCallback: <function(
    model<backboneModel>,
    $cell<yquerjObject>,
  )this=Entry> « executed when a cell is rendered »,

  // ¬

}>;

//
//                              SORTING

sorting = <
  |string
  |string[]
  |function(testEntry<couchEntry>):<string|number|<string|number>[]> «
    if you want to enable manual sorting of entries, you should use the following code: "return tablifyApp.enableManualSorting(testEntry);"
  »
> «
  | deep key             — will fetch the value in entry, if value is an array, will display one entry for each value in the array
  | list of deep keys    — will join all gotten values (whatever they are) to make a sorting string
  | function to sort by  — if returned value is an array, will display one entry for each value in the array
  if grouping of entries is enabled, the grouping is first applied, and entries are then sorted in each group with this method
»;

//
//                              SEARCHING

searching = < function(model<backboneModel>){@this=<tablifyApp>}:<string> > «
  should return a string containing additional info to improve entries matching when the user search/filter the database
»;

//
//                              GROUPING

grouping = <
  |true
  |string
  |function(model<backboneModel>, sortValue<string|number>):<string|number>
> «
  | if true will group by sortValue,
  | if string, will fetch entry[grouping],
  | if function, will use the returned value
  NOTE: global grouping is not so recommended because it is indifferent to sorting, and therefore a same group title may display multiple times in the display, columns grouping is likely to function better
»;

groupingCustomDisplay = <function(
  $groupingIntertitleDiv <yquerjObject> « the intertitle container, where you should add the custom contents you want »,
  groupIntertitle <any> « the intertitle that would be displayed by default »,
)> « a custom function to define what will be displayed as group intertitle »;

//
//                              TUTORIAL STEP

tutorialStep = <{
  !title: <htmlString>,
  !text: <htmlString>,
  !$target: <function(tablifyApp<tablify·return>):<yquerjProcessable>>,
  ?placement: <string|function(tablifyApp<tablify·return>):<string>>@default="left" « see tippy placement options to know all the placement options »,
  ?nextCondition: <function(tablifyApp<tablify·return>):<boolean>> « if this function is defined, it should return true otherwise will not pass to next step »,
  ?previousCondition: <function(tablifyApp<tablify·return>):<boolean>> « same as nextCondition but to pass back to previous »,
  ?tippy: <any> «
    any additional tippy option,
    you can use here any tippy.js option, however be careful with what you set here, because it will overwrite options passed by tutorial and you could break tutorial if you set options like: 'content', 'interactive', 'content'...
  »,
}>;

//
//                              CSS RULES

cssRules = <{
  !selector: <string> « The selector of the elements you want to apply the rules to. (e.g. ".my-class", or "&.tablify-gallery" if you wanted to customize display of the tablify element when in gallery display). »,
  !rule: <string> « The rule(s) to set (e.g. "background-color: red;"...) »,
  ?mediaQuery: <string> « A custom media query to wrap your rule in and use it only in some devices (e.g. "@media (min-width: 750px)"). »,
}[]>;

//
//                              SUPPORTED LAYOUTS

supportedLayout = <keyof supportedLayouts>;
supportedLayouts = ["table", "gallery", "free"];

//
//                              ACTIONS CONFIG

/**
  DESCRIPTION:
    events triggering actions on entries
    methodName is the name of an entry method
    each string descriptor must contain the type of event plus a selector (for example the class of a subelement in an entry view) on which the event must happen, for this action to be triggered
    for more info check backbone events (https://backbonejs.org/#events)
    jquery events are supported and also the additional "longclick" event (triggered only if tapping is longer than 1 second)
  EXAMPLE:
    {
      edit: "click .edit-button",
      open: "dblclick",
      select: "click",
    }
*/
actionsConfig = <{
  [methodName]: <string>,
}>@default=<{
  generalClickEvent: $$.isTouchDevice() ? "click .image" : "click",
  open: $$.isTouchDevice() ? "click .titles" : "dblclick",
  enterOrganizeSpecialMode: "longclick",
}>;

//                              ¬
//

```

## The `tablifyApp` object — returned when you run `tablify(opts)`

Tablify function returns a tablifyApp object (= backbone view object), it contains the following methods:
```javascript
tablifyApp = <{
  // ... methods of Backbone.View
  config: <tablifyOptions>,
  Collection: <{
    // ... methods of Backbone.Collection
    model: <backboneModel>,
    url: <string>,
    pouch: <pouchdb>,
    // ... see all methods in collection.js
  }>,
  shortcuts: <{
    keycombo: <string> « the key combo that will trigger the shortcut »,
    keycomboId: <string> « an id to uniquely identify the shortcut »,
    description: <string> « what this shortcut is doing »,
    action: <function(App<tablifyApp>, e)|keyof tablifyApp>,
  }[]> « list of all global shortcuts for the tablify app »,
  toolbar: <toolbarified> « see https://squeak.eauchat.org/libs/tablify/ »,
  $el: <yquerjObject> « the tablify element, containing table, windows... »,
  $header: <yquerjObject> « table header »,
  $body: <yquerjObject> « table body »,
  $footer: <yquerjObject> « table footer »,
  $entries: <yquerjObject> « jquery list of entries currently displayed »,
  $pathbar: <yquerjObject> « the path bar (defined only if tablifyOptions.pathsDatabase was set to true) »,
  $editorContainer: <yquerjObject> « entry editor container »,
  currentPath: <string> « the path of the currently open folder »,
  currentPathId: <string> « the id of the currently open folder »,
  currentDisplay: <tablifyConfig·display> « the currently used display settings (depends on db display + folder specifics display) »,
  // ... this is a non exhaustive list of keys that tablifyApp contains, feel free to contribute to extend this documentation
  // ... for a list of tablifyApp methods, see the api methods page
}>;
```
