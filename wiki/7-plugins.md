# Plugins

You can build any plugin you would like to add to tablify.
You just need to pass it in your config. Each plugin should be a function. That function will be executed on start with tablifyApp and tablifyConfig passed as argument.
There are some built-in plugins that you can use. Just import them with `require("tablify/plugin/pluginName")` and pass them in your config.

## Built-in plugins

### entryMenu

**Description:** Allows to create a column displaying a little menu when you click it.
**Usage:**
  - pass the plugin to config
  - add the following column to tablifyConfig.columns where you want entries menus to appear:
    ```javascript
    {
      !key: "entry_menu",
      ?menuOptions: <object to pass to uify.menu>,
      ?menu: {
        title: <string>,
        icomoon: <string>,
        click: <function(model, $cell, event)this=Entry:void>,
      }[],
      // ... any key valid for tablifyConfig.columns objects
    }
    ```
