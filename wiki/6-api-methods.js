
function meth (name) {
  return {
    display: {
      title: name,
    },
    path: "core/app/"+ name,
    methodsAutofill: {
      type: "all",
      prefix: "tablifyApp",
    },
  };
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {

  title: "API - tablifyApp methods",
  type: "api",

  moduleName: "tablify",

  documentation: {
    display: {
      comment: "This list of methods is not fully exhaustive, you're welcome to contribute to improve it.",
    },
    methods: [
      meth("batchActions"),
      meth("displayLists"),
      meth("dom"),
      meth("edition"),
      meth("importExport"),
      meth("layout"),
      meth("modes"),
      meth("paths"),
      meth("processEntries"),
      meth("refresh"),
      meth("search"),
      meth("selection"),
      meth("synchronization"),
      meth("tutorial"),
      meth("url"),
      meth("utilities"),
      meth("view"),
    ],
  },

};
