const _ = require("underscore");
const $$ = require("squeak/node");
const utilities = require("./utilities");
const getAffectedChildren = require("../core/utilities/getAffectedChildren");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var tests = {

  //
  //                              BASIC TEST

  "basic": {
    entriesInCollection: [
      { _id: "id1", type: "folder",  path: [ "_/"       ]  },
      { _id: "id11",                 path: [ "id1"      ]  },
      { _id: "id12",                 path: [ "id1"      ]  },
      { _id: "id2", type: "folder",  path: [ "_/"       ]  },
      { _id: "id21",                 path: [ "id2"      ]  },
      { _id: "id22",                 path: [ "id2"      ]  },
      { _id: "id99", type: "folder", path: [ "_/", "id2" ] },
    ],
    entryToRemove: { _id: "id2", path: [ "_/" ] },
  },

  //
  //                              ADVANVED USED CASES TEST

  "advanced use cases": {
    entriesInCollection: [
      { _id: "id1",  type: "folder", path: [ "id3",          ] },
      { _id: "id27", type: "folder", path: [ "_/",           ] },
      { _id: "id2",  type: "folder", path: [ "id27",         ] },
      { _id: "id3",  type: "folder", path: [ "id2",  "id27", ] },
      { _id: "id4",  type: "folder", path: [ "id1",  "id3",  ] },
      { _id: "id5",  type: "folder", path: [ "_/",   "id4",  ] },
    ],
    entryToRemove: { _id: "id27", path: [ "_/" ] },
  },

  //
  //                              LOOP TEST

  "loop": {
    entriesInCollection: [
      { _id: "id1", type: "folder", path: [ "_/"         ] },
      { _id: "id2", type: "folder", path: [ "id1", "id3" ] },
      { _id: "id3", type: "folder", path: [ "id2"        ] },
    ],
    entryToRemove: { _id: "id2", path: [ "_/" ] },
  },

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

_.each(tests, function (test, testName) {

  var result = getAffectedChildren(utilities.makeCollection(test.entriesInCollection), test.entryToRemove);

  $$.log.line();
  $$.log.style("::"+ testName +"::", "#F0F", true)
  $$.log();

  $$.log.style("Entries: ", "#0F0");
  $$.log(test.entriesInCollection);
  $$.log.style("Removed entry: ", "#0F0");
  $$.log(test.entryToRemove);

  $$.log();
  $$.log.style("To remove: ", "#0F0");
  $$.log(result.toRemove);
  $$.log.style("To modify: ", "#0F0");
  $$.log(result.toModify);

  $$.log();
  $$.log.line();

});
