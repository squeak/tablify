const _ = require("underscore");
const $$ = require("squeak/node");
const utilities = require("./utilities");
const getFullPaths = require("../core/utilities/getFullPaths");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var collectionWithLoops = [
  { _id: "id1", type: "folder", path: [ "_/"                ], name: "hello",  },
  { _id: "id2", type: "folder", path: [ "id1", "id3", "id5" ], name: "world",  },
  { _id: "id3", type: "folder", path: [ "id2"               ], name: "and",    },
  { _id: "id4", type: "folder", path: [ "id2"               ], name: "space",  },
  { _id: "id5", type: "folder", path: [ "id1"               ], name: "new",    },
];

var tests = {

  //
  //                              LOOP TEST

  "loop": {
    entriesInCollection: collectionWithLoops,
    entryToGetPathsOf: collectionWithLoops[1],
  },

  //
  //                              NON LOOP TEST

  "non-loop": {
    entriesInCollection: collectionWithLoops,
    entryToGetPathsOf: collectionWithLoops[4],
  },

  //                              ¬
  //

};


//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

_.each(tests, function (test, testName) {

  var result = getFullPaths(utilities.makeCollection(test.entriesInCollection), test.entryToGetPathsOf);

  $$.log.line();
  $$.log.style("::"+ testName +"::", "#F0F", true)
  $$.log();

  $$.log.style("Entries: ", "#0F0");
  $$.log(test.entriesInCollection);
  $$.log.style("Entry to get paths of: ", "#0F0");
  $$.log(test.entryToGetPathsOf);

  $$.log();
  $$.log.style("Full path: ", "#0F0");
  $$.log(result);

  $$.log();
  $$.log.line();

});
