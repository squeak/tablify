# tablify

A full featured and highly customizable tool to display database entries, edit and visualize them.

For the full documentation, installation instructions... check [tablify documentation page](https://squeak.eauchat.org/libs/tablify/).
