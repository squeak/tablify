var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");

function makeMenuOptions (Entry, $cell, columnConfig, config) {

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  var defaultMenuButtons = [
    // refresh entry
    {
      title: "refresh entry view",
      icomoon: "spinner9",
      click: function (model, $cell, evt) {
        var Entry = this;
        Entry.refreshView();
      },
    },
    // log entry
    {
      title: "log entry",
      icomoon: "terminal",
      click: function (model, $cell, evt) {
        $$.log.info(model);
      },
    },
    // copy entry id
    {
      title: "copy entry id",
      icomoon: "copy",
      click: function (model, $cell, evt) {
        prompt("couchdb id", model.id);
      },
    },
    // open entry in couchdb
    {
      title: "open entry in couchdb",
      icomoon: "lab",
      click: function (model, $cell, evt) {
        if (!config.remotes || !config.remotes.length) return alert("No remote to open!");
        // get couch url removing db name, username and password
        var couchUrl = config.remotes[0].url.replace(/[^\/]*$/, "").replace(/^https\:\/\/[^@]*@/, "https://");
        var dbCouchName = $$.match(config.remotes[0].url, /[^\/]*$/);
        // open entry in fauxton
        $$.open(couchUrl +"_utils/#database/"+ dbCouchName +"/"+ model.id, evt.ctrlKey);
      },
    },
  ];

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // add menu buttons passed in config
  var allMenuButtons = $$.array.merge(defaultMenuButtons, columnConfig.menu);

  // set context (this) and arguments for buttons click function // NOTE: need to map and clone otherwise bug: not getting the right entry on additional buttons defined in config
  var allMenuButtonsWithNiceClickEvent = _.map(allMenuButtons, function (menuButtonWithNotNiceClickEvent) {
    var menuButton = _.clone(menuButtonWithNotNiceClickEvent);
    menuButton.click = _.bind(menuButtonWithNotNiceClickEvent.click, Entry, Entry.model, $cell);
    return menuButton;
  });

  // make all menu options
  return $$.defaults({
    buttons: allMenuButtonsWithNiceClickEvent,
  }, columnConfig.menuOptions);

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (App) {

  var entryMenuColumnConfig = _.findWhere(App.currentDisplay.columns, { key: "entry_menu", });

  if (!_.isObject(entryMenuColumnConfig)) $$.log.detailedError(
    "tablify/plugin/entryMenu",
    "Cannot create menu, missing 'entry_menu' entry in tablifyConfig.columns",
    "Current list of columns is: ",
    App.currentDisplay.columns
  )

  else entryMenuColumnConfig.renderCallback = function (model, $cell) {
    var Entry = this;
    // show menu on click
    $cell.click(function () {
      uify.menu(makeMenuOptions(Entry, $cell, entryMenuColumnConfig, App.config));
    });
  };

};
