var _ = require("underscore");
var $$ = require("squeak");
var toolbarify = require("toolbarify");

var tablifyToolbar = require("./tablify");
var postifyToolbar = require("./postify");
var synchronizationToolbar = require("./synchronization");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (config, Collection, App) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET ALL TOOLBAR BUTTONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  var allTools = [];

  // EDITOR TOOLBAR
  if (!config.disableEditing) allTools.push(postifyToolbar(App));

  // DEFAULT TOOLBAR
  allTools.push(tablifyToolbar(App));

  // SYNCHRONIZATION TOOLBAR
  if (config.url || config.synchronizationTools) allTools.push(synchronizationToolbar(App));

  // BUTTONS FROM CONFIG
  if (config.toolbarButtons) allTools.push(config.toolbarButtons);

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CREATE TOOLBAR
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // USE EXISTING TOOLBAR
  if (config.toolbarToUse) var toolbar = config.toolbarToUse;
  // CREATE NEW TOOLBAR
  else {
    var toolbarOptions = $$.defaults({
      name: "tablifyToolbar",
      $el: config.$toolbarContainer || config.$container,
      position: "right",
    }, config.toolbarOptions);
    var toolbar = toolbarify(toolbarOptions);
  };

  // MAKE FINAL LIST OF BUTTONS
  var toolbarButtonsOptions = _.flatten(allTools, true);

  _.each(toolbarButtonsOptions, function (toolbarButtonOptions) {
    // add context to buttons
    if (!toolbarButtonOptions.context) toolbarButtonOptions.context = App;
  });

  // ADD BUTTONS IN TOOLBAR
  toolbar.add.buttons(toolbarButtonsOptions);
  return toolbar;

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
