var _ = require("underscore");
var $$ = require("squeak");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (App) {
  return [
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    //
    //                              OPEN ENTRY

    // {
    //   icomoon: "envelope-open",
    //   position: "bottom",
    //   title: "open selected entry/entries",
    //   click: function () {
    //     App.openSelectedEntries();
    //   },
    //   key: "enter",
    // },

    //
    //                              ADD ENTRY

    {
      name: "newEntry",
      icomoon: "plus",
      position: "bottom",
      title: "add new entry",
      click: function () { App.createNewEntry(); },
      key: "shift + n",
      keyid: "tablify--new_entry",
    },

    //
    //                              EDIT ENTRY

    {
      name: "editEntry",
      icomoon: "quill",
      position: "bottom",
      title: "edit selected entry/entries",
      click: function () { App.editSelectedEntries(); },
      key: "shift + e",
      keyid: "tablify--edit_entry",
    },

    //
    //                              ADD MULTIPLE ENTRIES

    {
      name: "advancedEdits",
      icomoon: "database",
      position: "bottom",
      title: "advanced database actions",
      key: "shift + d",
      keyid: "tablify--advanced_edits",
      clickMenu: {
        buttons: function () {
          return [

            {
              title: "import entries from file",
              icomoon: "folder-upload",
              key: "i",
              click: function () { App.importEntriesFromFile(); },
            },

            {
              title: App.selected && App.selected.length ? "export selected entries to file" : "export entries to file",
              key: "e",
              icomoon: "folder-download",
              click: function () { App.exportEntriesToFile(); },
            },

            {
              type: "space",
            },

            {
              title: "add multiple entries",
              icomoon: "add", // asterisk
              key: "a",
              click: function () { App.batchAdd(); },
            },

            {
              title: "batch modify the selected entries in raw view",
              key: "b",
              icomoon: "bath", // batman
              click: function () { App.batchModify(); },
            },

            {
              title: "process and modify the selected entries",
              key: "y",
              icomoon: "magic-wand", // linode, barcode4
              click: function () { App.processAndModifyMultipleEntries(); },
            },

            {
              title: "remove all selected entries",
              key: "r",
              icomoon: "trash",
              condition: function () { return !App.config.toolbarBatchRemoveDisable; },
              click: function () { App.batchRemove(); },
            },

          ];
        },
      },
    },

    //
    //                              SEPARATOR

    {
      type: "separator",
      position: "bottom",
    },

    //                              ¬
    //

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  ];
};
