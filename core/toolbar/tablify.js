var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");
var keyboardify = require("keyboardify");
var layouts = require("../layouts");
var graphify = require("graphify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  STRINGIFY KEY COMBO (SO HTMLSANITIZED DOSEN'T COMPLAIN)
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function stringifyKeycombo (keycombo) {
  return _.isArray(keycombo) ? keycombo.join(", ") : keycombo;
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  EDIT SHORTCUT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function openShortcutEditionDialog (keycomboId, modifiedCallback, revertToDefaultCallback) {
  var chosenKeycombo;

  var confirmDialog = uify.confirm({
    content: function ($container) {

      // display temporary text in dialog
      $container.html("press the key combo you'd like to use");

      // enter in a custom keyboardify context
      keyboardify.setContext("tablify-edit_shortcut_temporary_context");

      // display all keystrokes pressed in dialog
      keyboardify.bind(function (e) {
        chosenKeycombo = _.chain(e.pressedKeys)
          .map(function (keystr) { return keystr.toLowerCase() })
          .uniq()
          .value()
          .join(" + ")
        ;
        $container.html(chosenKeycombo);
      });

    },
    ok: function () {
      keyboardify.cancelContext("tablify-edit_shortcut_temporary_context");
      if (chosenKeycombo) modifiedCallback(chosenKeycombo);
    },
    cancel: function () {
      keyboardify.cancelContext("tablify-edit_shortcut_temporary_context");
    },
    buttons: [
      {
        title: "default",
        key: "d",
        click: function () {
          keyboardify.cancelContext("tablify-edit_shortcut_temporary_context");
          revertToDefaultCallback();
        },
      },
      {
        title: "set manually",
        key: "m",
        click: function () {
          var confirmResult = prompt("Write here the keyboard shortcut you want to use (using keyboardjs syntax).");
          if (confirmResult) {
            chosenKeycombo = confirmResult
            confirmDialog.$content.html(confirmResult);
          };
          return false;
        },
      },
      "cancel",
      "ok",
    ],
  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE LIST OF SHORTCUTS TABLE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makeListOfShortcutsTable (keyboardShortcutsForThisContext, dialogObject) {

  //
  //                              CREATE TABLE

  var $table = dialogObject.$content.table({
    class: "tablify-shortcuts-table",
  });

  //
  //                              MAKE TABLE HEADER

  var $tableHead = $table.thead();
  var $tableHeadRow = $tableHead.tr();
  $tableHeadRow.th({ class: "combo", text: "key combo", });
  $tableHeadRow.th({ class: "action", text: "action", });

  //
  //                              MAKE TABLE BODY

  var $tableBody = $table.tbody();
  _.each(keyboardShortcutsForThisContext, function (shortcutObject) {

    var $tr = $tableBody.tr({
      class: (shortcutObject.isNotEditable ? " not_editable" : ""),
    });

    // key combo column
    var $combo = $tr.td({
      htmlSanitized: stringifyKeycombo(shortcutObject.keycombo),
      class: "combo"+ (shortcutObject.bindingManuallyModified ? " modified_shortcut" : ""),
    }).dblclick(function () {
      if (!shortcutObject.isNotEditable && shortcutObject.keycomboId) openShortcutEditionDialog(shortcutObject.keycomboId, function (chosenShortcut) {
        keyboardify.modifyBind(shortcutObject.keycomboId, chosenShortcut, true);
        $combo.htmlSanitized(stringifyKeycombo(chosenShortcut));
        $combo.addClass("modified_shortcut");
        if (shortcutObject.uifyRefreshButton) shortcutObject.uifyRefreshButton(shortcutObject);
        uify.toast.success("New key combo will be: "+ chosenShortcut);
      }, function () {
        keyboardify.clearSavedModifiedBinding(shortcutObject.keycomboId);
        $combo.htmlSanitized(stringifyKeycombo(shortcutObject.keycombo));
        $combo.removeClass("modified_shortcut");
        if (shortcutObject.uifyRefreshButton) shortcutObject.uifyRefreshButton(shortcutObject);
        uify.toast.success("Key combo reset to it's default value.");
      });
    });

    // description column
    var $action = $tr.td({
      htmlSanitized: shortcutObject.description,
      class: "action",
    });

    // show related button when hovering over it's line in table
    if (shortcutObject.$uifyRelatedButton) {
      $tr.hover(function () {
        if (shortcutObject.$uifyRelatedButton) shortcutObject.$uifyRelatedButton.addClass("enhance");
      }, function () {
        if (shortcutObject.$uifyRelatedButton) shortcutObject.$uifyRelatedButton.removeClass("enhance");
      });
    };

  });

  // make toolbar more visible when mouse hovers over the table
  $table.hover(function () {
    dialogObject.$overlay.addClass("dialog_overlay_not_covering_toolbar");
  }, function () {
    dialogObject.$overlay.removeClass("dialog_overlay_not_covering_toolbar");
  });

  //
  //                              EDIT SHORTCUTS BANNER

  dialogObject.$content.div({
    class: "editshortcuts_textbanner",
    text: "double click on a key combo to edit a shortcut",
  });

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DISPLAY SHORTCUTS TABLE DIALOG
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function displayShortcutsTableDialog (App) {

  //
  //                              FETCH LIST OF SHORTCUTS FROM KEYBOARDIFY

  var keyboardShortcutsForThisContext = _.filter(keyboardify.list, function (shortcutObject) {
    return (
      // include all shortcuts saved in current keyboardify context
      shortcutObject.context === App.config.keyboardifyContext
      // include windify shortcuts
      || shortcutObject.boundBy === "windify"
    );
  });

  //
  //                              MANUALLY ADD SOME CUSTOM SHORTCUTS (CLICKING SHORTCUTS)

  _.each([
    {
      isNotEditable: true,
      keycombo: "alt + click",
      description: "move entries (only on paths databases)",
    },
    {
      isNotEditable: true,
      keycombo: "shift + click",
      description: "select multiple entries consecutively",
    },
    {
      isNotEditable: true,
      keycombo: "ctrl + click",
      description: "select multiple entries",
    },
  ], function (shortcutObject) {
    keyboardShortcutsForThisContext.unshift(shortcutObject);
  });

  //
  //                              SHOW SHORTCUTS LIST DIALOG

  uify.alert({
    title: "List of shortcuts",
    contentClass: "shortcuts-list",
    content: function () {
      makeListOfShortcutsTable(keyboardShortcutsForThisContext, this);
    },
    ok: function () {
      // make sure to remove enhancing on buttons if dialog is closed without hovering off of some cells
      _.each(keyboardShortcutsForThisContext, function (shortcutObject) {
        if (shortcutObject.$uifyRelatedButton) shortcutObject.$uifyRelatedButton.removeClass("enhance");
      });
    },
  });

  //                              ¬
  //

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (App) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  HELP MENU BUTTONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  var defaultHelpMenuButtons = [
    {
      icomoon: "question-circle-o",
      title: "how to use this app",
      key: "h",
      click: function () {
        App.startTutorial();
      },
    },
    {
      icomoon: "keyboard-o",
      title: "list of shortcuts",
      key: "s",
      click: function () {
        displayShortcutsTableDialog(App);
      },
    },
  ];

  if (App.config.toolbarHelpCustomize) var helpMenuButtons = App.config.toolbarHelpCustomize.call(App, _.clone(defaultHelpMenuButtons))
  else var helpMenuButtons = _.clone(defaultHelpMenuButtons);

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  return [
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    //
    //                              TOP HELP AND SEPARATOR

    {
      name: "helpMenu",
      icomoon: "question2", // question-circle-o
      position: "top",
      title: "info, help, tutorials",
      key: "shift + i",
      keyid: "tablify--help_menu",
      clickMenu: {
        buttons: helpMenuButtons,
      },
    },

    //
    //                              GRAPHS AND STATS

    {
      name: "graphify",
      icomoon: "stats-bars",
      position: "top",
      title: "stats and graphs",
      key: "shift + g",
      keyid: "tablify--graphify",
      click: function () {

        // create grpahify panel (first time)
        if (!App.graphified) App.graphified = graphify({
          title: "Database stats and graphs",
          collection: App.Collection.toJSON(),
          graphs: $$.getValue(App.config, "graph.graphs"),
        })

        // toggle panel and refresh it if collection changed since it's creation
        else {
          App.graphified.toggle();
          if (App.graphified.toRefresh) {
            App.graphified.refresh(App.Collection.toJSON());
            delete App.graphified.toRefresh;
          };
        };

      },
    },

    {
      type: "separator",
      position: "top",
    },

    //
    //                              CHANGE LAYOUT BUTTON

    {
      name: "displayLayoutChoose",
      condition: function () { return App.getAvailableLayouts().length > 1; },
      title: "choose display style",
      icomoon: "grid", // layout12, table2, th-list
      position: "bottom",
      key: "shift + b",
      keyid: "tablify--layout_choice",
      cssIcon: { transform: "scale(1.5)", },
      clickMenu: function () {
        //--------------------
        return {
          buttons: _.map(App.getAvailableLayouts(), function (layoutName) {
            return {
              title: layoutName,
              icomoon: layouts[layoutName].icon,
              click: function () { App.switchLayout(layoutName); },
            };
          }),
        };
        //--------------------
      },
    },

    //
    //                              LIMIT NUMBER OF ENTRIES SHOWN

    {
      icomoon: "filter", // list-alt
      position: "bottom",
      title: "limit number of entries displayed",
      clickMenu: {
        buttons: [
          { title: "10",     value: 10,   },
          { title: "25",     value: 25,   },
          { title: "50",     value: 50,   },
          { title: "100",    value: 100,  },
          { title: "200",    value: 200,  },
          { title: "500",    value: 500,  },
          { title: "all",    value: null, },
        ],
        click: function (buttonOptions) {
          App.limit.call(App, buttonOptions.value);
        },
      },
      key: "shift + l",
      keyid: "tablify--limit",
    },

    //
    //                              SEARCH/FILTER DB

    {
      name: "searchEntries",
      icomoon: "search",
      position: "bottom",
      title: "find entries in database",
      click: function () {
        App.searchDialog();
      },
      key: "shift + f",
      keyid: "tablify--search",
    },

    //                              ¬
    //


  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  ];
};
