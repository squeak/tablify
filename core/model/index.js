var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/string");
var getType = require("../utilities/getType");
var getFullPaths = require("../utilities/getFullPaths");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (appConfig, modelOptions) {

  var modelDefaults = {
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    // set backbone id from
    idAttribute: "_id",
    isBackboneModel: true,
    // cache: {}, // be careful with what you're doing if you want to implement caching, because new models may end up with cache of older models !! ;/

    //
    //                              UTILITIES

    getValue: function (deepKeyOrFunc) {
      if (_.isFunction(deepKeyOrFunc)) return deepKeyOrFunc(this)
      else return $$.getValue(this.attributes, deepKeyOrFunc);
    },

    setValue: function (deepKey, value) {
      return $$.setValue(this.attributes, deepKey, value);
    },

    // getValueOrResult: function (keyOrFunc, defaultValue) {
    //   return $$.getValueOrResult(this.attributes, keyOrFunc, defaultValue);
    // },

    //
    //                              FETCH THIS ENTRY WITH ATTACHMENTS FROM DATABASE

    /**
      DESCRIPTION: make sure a model contains it's attachments (is async, but only if some attachments have to be gotten, otherwise it's synchronous)
      ARGUMENTS: ( callback <function(ø)> « nothing passed to callback, but model has been modified », )
      RETURN: <void>
    */
    fetchWithAttachments: function (callback) {
      var model = this;
      if (!model.collection) $$.log.error("This is logged because model.collection is undefined, model value is:", model); // why sometimes model.collection is undefined ???????
      model.collection.getEntryWithAttachments(model.attributes, function (fetchedEntry) {
        // save fetched attachments (in case this is asked twice on same entry)
        if (fetchedEntry._attachments) model.attributes._attachments = fetchedEntry._attachments;
        // done callback
        callback(fetchedEntry);
      });
    },

    //
    //                              GET TYPE

    /**
      DESCRIPTION: get the type config of this model
      ARGUMENTS: ( ø )
      RETURN: <tablify·entryType>
    */
    getTypeConfig: function () {
      var model = this;
      // !!! do not use caching here, it creates bugs because new models seem to have cache of older models !!!
      // if (!model.cache.typeConfig) model.cache.typeConfig = getType(model.get("type"), appConfig);
      // return model.cache.typeConfig;
      return getType(model.get("type"), appConfig);
    },

    //
    //                              GET GENERAL

    /**
      DESCRIPTION: get display options
      ARGUMENTS: (
        ?typeConfig <tablify·entryType> « if not defined, will get it automatically, but multiple times, so it is more efficient to provide it »,
      )
      RETURN: <any>
    */
    getWindifyDisplayValues: function (typeConfig) {
      var model = this;
      return {
        title: model.getDisplayValue("title", typeConfig),
        subtitle: model.getDisplayValue("subtitle", typeConfig),
        icon: model.getDisplayValue("icon", typeConfig),
        image: model.getDisplayValue("image", typeConfig),
        bodyColor: model.getDisplayValue("color", typeConfig),
        titleBarColor: model.getAdditionalDisplayValue("secondaryColor", typeConfig),
      };
    },

    /**
      DESCRIPTION: get one display options
      ARGUMENTS: (
        !whatToGet <"title"|"subtitle"|"icon"|"image"|"color"> «
          note that if you ask for image, you must first fetch the model with it's attachments (using model.fetchWithAttachments) before running this
        »,
        ?typeConfig <tablify·entryType> « if not defined, will get it automatically »,
      )
      RETURN: <any>
    */
    getDisplayValue: function (whatToGet, typeConfig) {
      var model = this;
      if (!typeConfig) typeConfig = model.getTypeConfig();
      var valueToDisplay = $$.result.call(model, $$.getValue(typeConfig, "display."+ whatToGet), model.attributes, typeConfig);
      return $$.string.make(valueToDisplay, { htmlifyObjects: true, });
    },

    /**
      DESCRIPTION: get one additional display options
      ARGUMENTS: (
        !whatToGet <"title"|"subtitle"|"icon"|"image"|"color"|"additional"> «
          note that if you ask for image, you must first fetch the model with it's attachments (using model.fetchWithAttachments) before running this
        »,
        ?typeConfig <tablify·entryType> « if not defined, will get it automatically »,
      )
      RETURN: <any>
    */
    getAdditionalDisplayValue: function (whatToGet, typeConfig) {
      var model = this;
      if (!typeConfig) typeConfig = model.getTypeConfig();
      return $$.result.call(model, $$.getValue(typeConfig, "display.additional."+ whatToGet), model.attributes, typeConfig);
    },

    //
    //                              GET FULL PATHS (for folder entries)

    getFullPaths: function () {
      var model = this;
      // COULD CACHE TO MAKE IT FASTER, BUT WOULD NEED TO REMOVE CACHE ON MODIFICATION OF ANY FOLDER OF THE DB, SO ONLY DO IT IF REALLY USEFUL AND IMPROVE PERFORMANCES
      // // cache entry for speed, this cache is removed if any folder is modified in the collection (see App.syncSetup)
      // if (!model.cache.fullPaths) model.cache.fullPaths = getFullPaths(model.collection, model.attributes);
      // return model.cache.fullPaths;
      return getFullPaths(model.collection, model.attributes);
    },

    //
    //                              SAVE MODEL TO POUCH

    /**
      DESCRIPTION: save the model to pouch database
      ARGUMENTS: ( ?callback <function(result <object> « result returned by couch »)> « executed on save success (only) » )
      RETURN: <void>
    */
    saveToPouch: function (callback) {
      var model = this;
      model.collection.put(model.attributes, callback);
    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  };

  return $$.defaults(modelDefaults, modelOptions);

};
