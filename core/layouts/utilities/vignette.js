var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/color");
var $ = require("yquerj");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GET STYLE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function setColor ($elem, model, color) {
  $elem.css({
    color: color,
    backgroundColor: "",
  });
};

function setHoveredColor ($elem, model, color) {
  $elem.css($$.color.contrastStyle(color));
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (model, Entry) {

  //
  //                              GET CARD OPTIONS

  var typeConfig = model.getTypeConfig();

  //
  //                              MAKE CARD DISPLAY

  var $container = Entry.$el;

  //
  //                              IMAGE/ICON

  var $image = $container.div({ class: "image", });

  // ICON
  var iconName = model.getDisplayValue("icon", typeConfig);
  $image.addClass("icon-"+ (iconName || Entry.App.config.display.defaultEntryIcon));



  // IMAGE
  if (_.isFunction($$.getValue(typeConfig, "display.image"))) model.fetchWithAttachments(function () {
    var imageUrl = model.getDisplayValue("image", typeConfig);
    if (imageUrl) {
      $image.css("background-image", 'url("'+ imageUrl +'")');
      $image.removeClass("icon-"+ (iconName || Entry.App.config.display.defaultEntryIcon));
    };
  });

  // SHOW A SMALL ICON IF ENTRY IS IN MULTIPLE FOLDERS AT SAME TIME
  var pathArray = model.get("path");
  if (pathArray && pathArray.length > 1) $image.div({
    class: "image-multifolder_icon",
    text: "⭓",
  });

  //
  //                              TITLE, SUBTITLE, COLOR

  // create titles container
  var $titles = $container.div({ class: "titles", });

  // TITLE
  var title = model.getDisplayValue("title", typeConfig);
  if (title) {
    $container.attr("title", title); // TODO: this will not display well if title is html
    $titles.div({
      class: "title",
      htmlSanitized: title,
    });
  };

  // SUBTITLE
  var subtitle = model.getDisplayValue("subtitle", typeConfig);
  if (subtitle) var $subtitle = $titles.div({
    class: "subtitle",
    htmlSanitized: subtitle,
  });

  // COLOR
  var color = model.getDisplayValue("color", typeConfig);
  if (color) {
    setColor(Entry.$el, model, color);
    Entry.$el.hover(function () {
      setHoveredColor($(this), model, color);
    }, function () {
      setColor($(this), model, color);
    });
  };

  //
  //                              RETURN VIGNETTE

  return Entry.$el;

  //                              ¬
  //

};
