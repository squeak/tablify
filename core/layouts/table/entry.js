var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/string");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (model, Entry) {
  var App = this;

  // create cell for each column
  _.each(App.currentDisplay.columns, function (columnConfig) {

    var $cell = Entry.$el.td({ class: columnConfig.key, });
    if (columnConfig.class) $cell.addClass($$.result(columnConfig.class, model));
    if (columnConfig.uuidClass) $cell.addClass(columnConfig.uuidClass);

    // make cell content
    if (columnConfig.display) var content = columnConfig.display.call(App, model, columnConfig.key, $cell)
    else var content = model.getValue(columnConfig.key);
    if (!_.isUndefined(content)) $cell.htmlSanitized($$.string.make(content, { htmlifyObjects: true, }));

    // click and callback
    if (columnConfig.click) $cell.click(function (event) {
      event.stopPropagation();
      columnConfig.click.call(Entry, model, $cell, event);
    });
    if (columnConfig.renderCallback) columnConfig.renderCallback.call(Entry, model, $cell);

  });


};
