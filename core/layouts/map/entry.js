var _ = require("underscore");
var $$ = require("squeak");
var makeVignette = require("../utilities/vignette");
var uify = require("uify");
var shapes = require("./shapes");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (model, Entry) {
  var App = this;

  var $container = Entry.$el;

  // get config for this entry type
  var typeConfig = model.getTypeConfig();

  // figure out coordiantes
  var coordinatesKey = model.getAdditionalDisplayValue("geoKey", typeConfig);
  var geo = model.getValue(coordinatesKey);
  if (geo) var shape = shapes[geo.shape];

  // if a shape is defined create it
  if (shape) {
    var shapeElement = shape.create(App.leafletMap, geo, model, typeConfig);
    Entry.resetElement(shapeElement);
    Entry.$el.addClass("tablify-map--elem--"+ geo.shape);
  }

  // else TODO
  else {

    // TODO: support if
    //    - unknown type of shape
    //    - missing geoKey in type
    //    - missing geo infos in this entry
    // log/alert different type of errors (maybe)
    // display them somewhere/somehow still

  };

};
