var _ = require("underscore");
var $$ = require("squeak");
var L = require("leaflet");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE SHAPE OPTS SHORTCUT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makeShapeOpts (model, typeConfig, additionalOptions) {

  var shapeOpts = $$.defaults(
    {
      color: model.getDisplayValue("color", typeConfig) || "black",
      weight: model.getAdditionalDisplayValue("weight", typeConfig),
      dashArray: model.getAdditionalDisplayValue("strokeDashArray", typeConfig),
      fillColor: model.getAdditionalDisplayValue("fillColor", typeConfig),
      fillOpacity: model.getAdditionalDisplayValue("fillOpacity", typeConfig),
    },
    additionalOptions
  );

  return _.omit(shapeOpts, function (val) { return _.isUndefined(val) || _.isNull(val); });

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  POINT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  point: {
    create: function (map, geoValue, model, typeConfig) {

      // CUSTOM SIZE
      var weight = model.getAdditionalDisplayValue("weight", typeConfig);
      var size = weight ? (+weight * 10) : 20;
      var iconName = model.getDisplayValue("icon", typeConfig) || "circle2";

      // CREATE POINT ICON
      var icon = L.divIcon({
        className: "tablify-leaflet-icon icon-"+ iconName,
        iconSize: L.point(size, size),
        iconAnchor: L.point(size/2, size/2),
      });

      // CREATE MARKER
      var marker = L.marker(geoValue.c, {
        icon: icon,
      }).addTo(map);
      map.allTablifyGeometries.push(marker);

      // GET ICON JQUERY OBJECT
      var $icon = $(marker._icon)

      // COLOR MARKER
      $icon.css("color", model.getDisplayValue("color", typeConfig) || "black");

      // CUSTOMIZE MARKER SIZE
      $icon.css({
        fontSize: size,
        width: size,
        height: size,
      });

      // IMAGE ICON FOR MARKER
      if (_.isFunction($$.getValue(typeConfig, "display.image"))) model.fetchWithAttachments(function () {
        var imageUrl = model.getDisplayValue("image", typeConfig);
        if (imageUrl) {
          $icon.css("background-image", 'url("'+ imageUrl +'")');
          $icon.removeClass("icon-"+ iconName);
          $icon.addClass("marker-with-image");
        };
      });

      // RETURN MARKER ELEMENT
      return marker._icon;

    },
    // focus: function (map) {
    //   map.panTo(this.marker.getLatLng());
    // },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CIRCLE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  circle: {
    create: function (map, geoValue, model, typeConfig) {

      // ADD CIRCLE TO MAP
      var circle = L.circle(geoValue.c, makeShapeOpts(model, typeConfig, { radius: geoValue.radius, })).addTo(map);
      map.allTablifyGeometries.push(circle);

      // RETURN CIRCLE PATH ELEMENT
      return circle._path;

    },
    // focus: function (map) {
    //   map.fitBounds(this.circle.getBounds());
    // },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  POLYLINE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  polyline: {
    create: function (map, geoValue, model, typeConfig) {

      // CREATE POLYLINE AND ADD IT TO MAP
      var polyline = L.polyline(geoValue.c, makeShapeOpts(model, typeConfig)).addTo(map);
      map.allTablifyGeometries.push(polyline);

      // RETURN CIRCLE PATH ELEMENT
      return polyline._path;

    },
    // focus: function (map) {
    //   map.fitBounds(this.polyline.getBounds());
    // },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  POLYGON
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  polygon: {
    create: function (map, geoValue, model, typeConfig) {

      // CREATE POLYGON AND ADD IT TO MAP
      var polygon = L.polygon(geoValue.c, makeShapeOpts(model, typeConfig)).addTo(map);
      map.allTablifyGeometries.push(polygon);

      // RETURN POLYGON PATH ELEMENT
      return polygon._path;

    },
    // focus: function (map) {
    //   map.fitBounds(this.polygon.getBounds());
    // },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
