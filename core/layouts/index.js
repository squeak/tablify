
/**
  DESCRIPTION: all layouts that tablify can handle, any added layout should follow the same schemes
  TYPE:
    <{
      [<string> « name of the layout »]: {

        // GENERAL
        !tagName: <string> « must be a valid html tag name supported by yquerj »,
        !icon: <string>,

        // HEADER AND ENTRY MAKING
        !makeHeader: <function(){@this=App},
        !makeEntry: <function(model, Entry){@this=App},

        // SORTING, REVERSE, GROUPING
        ?getSortingAndReverse: <function(displayConfig<tablify·config.display>):<{
          sorting: <sorting> « see README for type detils »,
          sortReverse: <boolean>,
        }>> « if this layout needs a customized way to get the current sorting method and reverse status »,
        ?getGrouping: <function(displayConfig<tablify·config.display>):<grouping>> « if this layout needs a customized way to get the current grouping method »,

      }
    }>
*/
module.exports = {
  gallery: require("./gallery"),
  list: require("./list"),
  thinlist: require("./thinlist"),
  tinylist: require("./tinylist"),
  microlist: require("./microlist"),
  map: require("./map"),
  table: require("./table"),
  free: require("./free"),
  thumbnails: require("./thumbnails"),
};
