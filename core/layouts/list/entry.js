var _ = require("underscore");
var $$ = require("squeak");
var makeVignette = require("../utilities/vignette");
var uify = require("uify");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (model, Entry) {
  var App = this;

  var typeConfig = model.getTypeConfig();
  var $vignette = makeVignette(model, Entry);

  // DISPLAY CUSTOM BUTTONS ASKED
  var additionalButtons = model.getAdditionalDisplayValue("buttons", typeConfig);
  if (additionalButtons) _.each(additionalButtons, function (buttonConfig) {
    uify.button($$.defaults(buttonConfig, {
      $container: $vignette,
      condition: function () {
        if (_.isFunction(buttonConfig.condition)) return buttonConfig.condition.call(model, App, typeConfig)
        else if (!_.isUndefined(buttonConfig.condition)) return buttonConfig.condition
        else return true;
      },
      click: function (event) {
        if (_.isFunction(buttonConfig.click)) buttonConfig.click.call(model, App, typeConfig, event);
        event.stopPropagation();
      },
    }));
  });

  // DISPLAY EDIT QUILL
  if (!App.config.disableEditing) uify.button({
    $container: $vignette,
    icomoon: "quill",
    title: "edit this entry",
    click: function (event) {
      event.stopPropagation();
      App.openEntryEditor(model);
    },
  });

};
