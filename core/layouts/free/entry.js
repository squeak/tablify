var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/dom");
var makeVignette = require("../utilities/vignette");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  POSITION VIGNETTE LIKE IT WAS SAVED BEFORE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var randomPositionsCache = { x: {}, y: {} };

function positionize ($vignette, model, $container, currentPathId) {
  var position = model.getValue("system.positionIn_"+ currentPathId) || {};

  // random position if nothing was set before
  if (_.isUndefined(position.x)) {
    if (_.isUndefined(randomPositionsCache.x[model.id])) randomPositionsCache.x[model.id] = $$.random.number(0, 0.8);
    position.x = randomPositionsCache.x[model.id];
  };
  if (_.isUndefined(position.y)) {
    if (_.isUndefined(randomPositionsCache.y[model.id])) randomPositionsCache.y[model.id] = $$.random.number(0, 0.8);
    position.y = randomPositionsCache.y[model.id];
  };

  $vignette.css("top", Math.round($container.height() * position.y) +"px");
  $vignette.css("left", Math.round($container.width() * position.x) +"px");
  $vignette.css("zIndex", position.z);

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SAVE MODELS DEBOUNCED METHOD
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var pendingSavingModelsPositions = {};
var saveModels = _.debounce(function (App) {

  //
  //                              SET POSITION IN MODELS AND PREPARE LIST OF ENTRIES TO SAVE

  var modelsToSave = [];
  _.each(pendingSavingModelsPositions, function (position, modelId) {
    var model = App.Collection.get(modelId);
    model.setValue("system.positionIn_"+ App.currentPathId, position);
    modelsToSave.push(model.attributes);
  });
  pendingSavingModelsPositions = {};

  //
  //                              SAVE ENTRIES TO DB

  App.Collection.pouch.bulkDocs(modelsToSave).then(function (result) {
    var errors = _.where(result, { error: true, });
    if (errors.length) {
      $$.log.error("Some entries failed to save.");
      $$.log("Failures: ", errors);
      $$.log("Full response: ", result);
    };
  }).catch(function (err) {
    $$.log.error("Failed to save entries to database.");
    $$.log.error(err);
    $$.log.error("Entries attempted to be saved:", modelsToSave);
  });

  //                              ¬
  //

}, 5000)

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (model, Entry) {
  var App = this;

  var $vignette = makeVignette(model, Entry);

  // initialize position as it was saved
  positionize($vignette, model, App.$body, App.currentPathId);

  // get the highest z-index in the list of entries' positions
  var maxZindex = _.max(App.Collection.map(function(model){ return model.getValue("system.position.z"); }));

  // make entries draggable
  $$.dom.makeDraggable({
    $target: $vignette,
    zAlso: "tablify-layout-free-vignette",
    zInitialMax: maxZindex > 2 ? maxZindex : 2,
    onDrag: function (position) {

      // get position in ratios
      pendingSavingModelsPositions[model.id] = {
        x: $$.round(position.x / App.$body.width(), 3),
        y: $$.round(position.y / App.$body.height(), 3),
        z: position.z,
      };

      // call debounced saveModels callback
      saveModels(App);

    },
    condition: function (event) {
      return event.altKey || App.isSpecialMode;
    },
  });

};
