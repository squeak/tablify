var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/array");
require("squeak/extension/dom");
require("squeak/extension/path");
var keyboardify = require("keyboardify");
var postify = require("postify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  KEYBOARD SHORTCUTS LIST
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var shortcutsList = [
  //
  //                              SELECT PREVIOUS

  {
    keycomboId: "tablify--select_previous",
    keycombo: ["up", "left"],
    description: "select previous entry (use shift to select multiples)",
    action: function (App, e) {
      var previousEntry = App.getPreviousEntry();
      if (!previousEntry) return;
      if (e.shiftKey) App.addToSelection(previousEntry.id)
      else App.select(previousEntry);
    },
  },

  //
  //                              SELECT NEXT

  {
    keycomboId: "tablify--select_next",
    keycombo: ["down", "right"],
    description: "select next entry (use shift to select multiples)",
    action: function (App, e) {
      var nextEntry = App.getNextEntry();
      if (!nextEntry) return;
      if (e.shiftKey) App.addToSelection(nextEntry.id);
      else App.select(nextEntry);
    },
  },

  //
  //                              SELECT FIRST

  {
    keycomboId: "tablify--select_first",
    keycombo: "pageup",
    description: "select first (use shift to select multiples)",
    action: function (App, e) {
      var firstEntry = App.currentDisplayedTable[0];
      if (!firstEntry) return;
      if (e.shiftKey) App.addToSelection(firstEntry.id)
      else App.select([firstEntry]);
    },
  },

  //
  //                              SELECT LAST

  {
    keycomboId: "tablify--select_last",
    keycombo: "pagedown",
    description: "select last (use shift to select multiples)",
    action: function (App, e) {
      var lastEntry = App.currentDisplayedTable[App.currentDisplayedTable.length - 1];
      if (!lastEntry) return;
      if (e.shiftKey) App.addToSelection(lastEntry.id)
      else App.select([lastEntry]);
    },
  },

  //
  //                              GO TO PARENT

  {
    keycomboId: "tablify--go2parent",
    keycombo: "backspace",
    description: "go to parent directory",
    action: function (App, e) {
      if (App.currentPath) {
        var parentPath = $$.path.decompose(App.currentPath).parentPath;
        if (parentPath) App.openPath(parentPath);
      };
    },
  },

  //
  //                              HOME DIRECTORY

  {
    keycomboId: "tablify--go2home",
    keycombo: "shift + backspace",
    description: "go to home directory",
    action: function (App, e) { App.openPath("/"); },
  },

  //
  //                              OPEN CURRENTLY SELECTED DIRECTORY

  {
    keycomboId: "tablify--open_selected",
    keycombo: "enter",
    description: "open currently selected directory",
    action: function (App, e) {
      App.openSelectedEntries();
    },
  },

  //
  //                              SELECT ALL VISIBLE ENTRIES

  {
    keycomboId: "tablify--select_all",
    keycombo: "shift + a",
    description: "select all visible entries",
    action: function (App, e) {
      App.select(App.currentDisplayedTable);
    },
  },

  //
  //                              TOGGLE VISIBILITY OF HIDDEN ELEMENTS

  {
    keycomboId: "tablify--show_hidden_elements",
    keycombo: "shift + u",
    description: "toggle visibility of hidden elements",
    action: function (App, e) {
      App.toggleDisplayOfHiddenEntries();
    },
  },

  //                              ¬
  //
];

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (config, App) {

  //
  //                              GET ALL SHORTCUTS (INCLUDING CUSTOM ONES PASSED TO TABLIFY CONFIG)

  var allShortcuts = $$.array.merge(shortcutsList, config.additionalShortcuts)

  //
  //                              CREATE BINDING FOR ALL SHORTCUTS USING KEYBOARDIFY

  _.each(allShortcuts, function (shortcutObject) {

    // get function from key in App or function
    var actionFunction = _.isString(shortcutObject.action) ? App[shortcutObject.action] : shortcutObject.action;

    // make sure to unbind any previously set action with same keycombo
    keyboardify.unbind(shortcutObject.keycombo);

    // create shortcut event
    keyboardify.bind(shortcutObject.keycombo, function (event) {
      event.preventDefault();
      actionFunction(App, event);
    }, undefined, undefined, shortcutObject);

  });

  //
  //                              RETURN

  return allShortcuts;

  //                              ¬
  //

}
