var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");
var dialogify = require("dialogify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GENERATE FILE TO DOWNLOAD
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/*
  DESCRIPTION: propose to the user to download a file with the given content
  ARGUMENTS: (
    !fileName <string> « the name to give to the file to download »,
    !text <string> « the content of the file to download »,
  )
  RETURN: <void>
  WARNING:
    ⚠️ be careful, using this function, it will open a link and has as a side effect to clear the console logs, and maybe other things
    so if you have to debug things it may be inconvenient and confusing
*/
function downloadTextFile (filename, text) {

  // create an <a> element with text to download as href
  var elem = document.createElement("a");
  elem.style.display = "none";
  elem.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(text));
  elem.setAttribute("download", filename);
  // if "download" attribute is not supported by browser, open file in new tab
  if (typeof elem.download === "undefined") elem.setAttribute("target", "_blank");

  // USING THE FOLLOWING AND REMOVING ALL THE PREVIOUS elem.setAttribute IS ANOTHER WAY TO GO THAT WILL NOT REFRESH PAGE AND IT'S CONSOLE LOGS
  // however, I didn't manage to make the file naming and the content disposition work, so the only way to force it to download is to use application/octet-stream and it's all quite ugly
  // elem.setAttribute("href", "data:application/octet-stream;headers=Content-Disposition%3A%20attachment%3B%20filename%3D%22my-file.txt;charset=utf-8," + encodeURIComponent(text));


  // add it to body
  document.body.appendChild(elem);
  // click element to download the content
  elem.click();
  // remove element
  document.body.removeChild(elem);
  delete elem;

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  IMPORT ENTRIES FROM FILE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: proposse to import entries from a json file that the user can choose in it's system
    ARGUMENTS: (
      ?shouldConfirm <function(entriesToImport <couchEntry[]>, callback <function(ø)>)> « if this is defined, will add entries only if callback is ran »
    )
    RETURN: <void>
  */
  importEntriesFromFile: function (shouldConfirm) {
    var App = this;

    if (!shouldConfirm) shouldConfirm = function (entriesToImport, callback) { callback(); };

    dialogify.input({
      title: "Choose file to import",
      input: {
        inputifyType: "file",
        cssContainer: {
          paddingTop: "2em",
        },
        cssInput: {
          "justify-content": "center",
          "text-align": "center",
        },
        file: {
          extensions: ["json"],
        },
      },
      ok: function (value) {

        if (value && value.isFile && value.blob) {

          // interpret chosen file json
          value.blob.text().then(function (fileText) {

            var entriesToImport = $$.json.parseIfJson(fileText, function (err) {
              $$.log.detailedError(
                "tablify/App.importEntriesFromFile",
                "Failed to parse json file to import entries.",
                err
              );
              uify.toast.error("Failed to parse json file to import entries.");
              return null;
            });

            // import entries
            if (entriesToImport) {

              if (entriesToImport.length) shouldConfirm(entriesToImport, function () {
                App.Collection.bulkPut(entriesToImport, {
                  success: "Successfully imported %%count%% entries.",
                  warningSuccess: "Imported %%count%% entries successfully.",
                  warningFail: "Failed to import %%count%% entries.",
                  error: "Error trying to import %%count%% entries.",
                });
              })
              else uify.toast("The file you chose to import doesn't contain any entry.");

            };

          }).catch(function (err) {
            uify.toast.error("Failed to interpret the file you chose.");
            $$.log.error(err);
          });

        }

        // if any error, failed
        else uify.toast.error("Failed to import file.");

      },
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EXPORT ENTRIES TO FILE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: propose the user to download the entries as json file
    ARGUMENTS: ({
      ?entries <couchEntry[]> « entries to export, if undefined, will do it with currently selected entries »,
      ?fileName <string> « a cutom name to give to the file to save (without extension) »,
      ?process <function(
        entry <couchEntry> « this has been cloned recursively (9 times), so you can modify it without impacting db entry »
      ):<object>> «
        a function to process content of entries to save
        the default method removes _id and _rev
      »,
    })
    RETURN: <void>
  */
  exportEntriesToFile: function (options) {
    var App = this;

    //
    //                              FIGURE OUT LIST OF ENTRIES TO EXPORT

    var entriesList = $$.getValue(options, "entries") || _.clone(_.pluck(App.selected, "attributes")) || [];
    if (!entriesList.length) return uify.confirm({
      content: "Do you want to export all entries in the database?",
      ok: function () {
        App.exportEntriesToFile($$.defaults(options, {
          entries: App.Collection.toJSON(),
        }));
      },
    });

    //
    //                              GET FULL OPTIONS

    var defaultOptions = {

      // DEFAULT FILE NAME: "<db-title> - <number-of-entries-exported> entries"
      fileName: ($$.getValue(App, "config.display.title") || App.config.name) +" - "+ entriesList.length +" entries",

      // DEFAULT PROCESSING: remove _rev
      process: function (entry) {
        // delete entry._id; // TODO: should it be made possible to export without uuids? is it necessary in some cases? or maybe it could be made possible to import entries without their id
        delete entry._rev;
        return entry;
      },

    };
    options = $$.defaults(defaultOptions, options);

    //
    //                              MAKE LIST OF ENTRIES TO EXPORT AND PROPOSE TO DOWNLOAD JSON FILE

    App.Collection.getEntriesWithAttachments(entriesList, function (entriesWithAttachments) {

      // PROCESS ENTRIES AS SPECIFIED IN OPTIONS
      var entriesToExport = [];
      _.each(entriesWithAttachments, function (entry) {
        if (!_.findWhere(entriesToExport, { _id: entry._id, })) {
          entriesToExport.push(options.process($$.clone(entry, 9)));
        }
        else $$.log.info("[tablify] Prevented reexporting entry: ", entry);
      });

      // PROPOSE TO DOWNLOAD JSON FILE
      downloadTextFile(
        // filename with extension
        options.fileName +".json",
        // file content
        $$.json.pretty(entriesToExport)
      );

    });

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
