var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/color");
require("squeak/extension/url");
var $ = require("yquerj");
var uify = require("uify");
var layouts = require("../layouts");
var Sortablejs = require("sortablejs");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DETECT STATE OF CTRL AND ALT KEYS PRESSING
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var currentMovement = "move"; // move|alias|sort
if (document.onkeydown) var formerOnKeydownFunction = document.onkeydown;
document.onkeydown = function (e) {
  keydownup(e);
  if (formerOnKeydownFunction) formerOnKeydownFunction(e);
};
if (document.onkeyup) var formerOnKeyupFunction = document.onkeyup;
document.onkeyup = function (e) {
  keydownup(e);
  if (formerOnKeyupFunction) formerOnKeyupFunction(e);
};

function keydownup (e) {
  if (e.ctrlKey) var newCurrentMovement = "alias"
  else if (e.altKey) var newCurrentMovement = "sort"
  else var newCurrentMovement = "move";
  if (newCurrentMovement !== currentMovement) {
    $("html").removeClass("tablify-movement-move tablify-movement-alias tablify-movement-sort");
    $("html").addClass("tablify-movement-"+ newCurrentMovement);
    currentMovement = newCurrentMovement;
  };
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GET ATTACHED IMAGE URL
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function getAttachedImageUrl (image, entry) {
  var imageId = _.isString(image) ? image : image.attachmentId;
  if (entry._attachments && entry._attachments[imageId]) {

    // image file
    var file = entry._attachments[imageId];
    if (!file.content_type.match(/^image/)) throw "not_image"
    else if (!file.data) throw "need_fetch"
    else return $$.url.makeBase64(file.data, file.content_type);

  };
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  PATHS BAR
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function refreshPathsBar (App) {

  //
  //                              INIT

  // reset pathbar to empty
  App.$pathbar.empty();

  // if not a pathsDatabase, just do nothing
  if (!App.config.pathsDatabase) {
    App.$pathbar.hide();
    App.table.removeClass("path_database");
    return;
  }
  else {
    App.table.addClass("path_database");
    App.$pathbar.show();
  };

  // get current path parts
  if (App.currentPath) var pathParts = _.compact( App.currentPath.split("/") );

  //
  //                              HOME BUTTON

  if (App.currentPath && App.currentPath != "/") {
    var buttonHome = uify.button({
      icomoon: "angle-double-left", // home
      $container: App.$pathbar,
      title: "go to root\n(shift + backspace)",
      class: "tablify-entries_can_be_dropped_here",
      click: function () { App.openPath("/"); },
    });
    buttonHome.$button.data("path", "/")
  };

  //
  //                              PARENT BUTTON

  if (App.currentPath && App.currentPath != "/" && App.currentPath != "UNREACHABLE" && App.currentPath != "ALL") {
    var pathOfParentFolder = pathParts.length > 1 ? "/"+ _.initial(pathParts).join("/") +"/" : "/";
    var buttonParent = uify.button({
      icomoon: "angle-left", // arrow-left
      $container: App.$pathbar,
      title: "go to parent folder\n(backspace)",
      class: "tablify-entries_can_be_dropped_here",
      click: function () { App.openPath(pathOfParentFolder); },
    });
    buttonParent.$button.data("path", pathOfParentFolder)
  };

  //
  //                              PATH

  var $path = App.$pathbar.div({ class: "path", });
  _.each(pathParts, function (subPathName, index) {
    var pathToGoTo = "/"+ _.first(pathParts, index+1).join("/") +"/";
    $path.div({
      class: "path_between",
      text: "/",
    });
    $path.div({
      class: "path_part tablify-entries_can_be_dropped_here",
      data: { path: pathToGoTo, },
      htmlSanitized: subPathName,
    }).click(function () {
      App.openPath(pathToGoTo);
    });
  });

  $path.div({
    class: "path_between",
    text: "/",
  });

  //
  //                              OPEN THIS FOLDER CONFIG BUTTON

  if (App.currentPath && App.currentPath != "/" && App.currentPath != "UNREACHABLE" && App.currentPath != "ALL") {
    uify.button({
      icomoon: "cog",
      $container: App.$pathbar,
      title: "edit this folder's configuration",
      click: function () { App.openEntryEditor(App.Collection.get(App.currentPathId)); },
    });
  };

  //
  //                              ALL PATHS BUTTON

  if (App.currentPath != "ALL") {
    uify.button({
      icomoon: "filter2",
      css: { transform: "rotate(180deg)", },
      $container: App.$pathbar,
      title: "see all entries flat",
      click: function () { App.openPath("ALL"); },
    });
  };

  //
  //                              UNREACHABLE PATHS BUTTON

  if (App.currentPath != "UNREACHABLE") {
    uify.button({
      icomoon: "chain-broken",
      $container: App.$pathbar,
      class: "unreachable-paths-button",
      title: "check unreachable paths",
      click: function () { App.openPath("UNREACHABLE"); },
    });
  };

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GROUPING INTERTITLE VALUE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/*
  DESCRIPTION: get the value of grouping intertitle row
  ARGUMENTS: (
    !grouping <true|key|function(model):<string>> « it will group by sortValue (true), by key, or by result of the given function »,
    !model: <backboneModel>,
    !sortValue: <string|number> « the value of the current sorting for this model »,
  )
  RETURN: <string>
*/
function getGroupingIntertitleValue (grouping, model, sortValue) {
  if (grouping === true) return sortValue
  else if (_.isFunction(grouping)) return grouping(model, sortValue)
  else return model.getValue(grouping);
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  HEADER
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: update display of header
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  makeHeader: function () {
    var App = this;

    // refresh paths bar display
    refreshPathsBar(App);

    // empty table head in case of refresh
    App.$header.empty();

    // make header for this layout
    if (layouts[App.currentDisplay.currentLayout].makeHeader) layouts[App.currentDisplay.currentLayout].makeHeader.call(App);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FOOTER
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: update display of footer
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  makeFooter: function () {
    var App = this;

    // empty table footer in case of refresh
    App.$footer.empty();
    if (App.currentDisplay.color) App.$footer.css($$.color.contrastStyle(App.currentDisplay.color));

    // (re)create footer
    if (App.currentDisplay.thumbnail) App.$footer.img({ class: "footer-image", src: getAttachedImageUrl(App.currentDisplay.thumbnail, App.config), })
    else if (App.currentDisplay.icon) App.$footer.div({
      class: "footer-icon icon-"+ App.currentDisplay.icon,
    });

    // title
    App.$footer.div({
      class: "footer-title",
      htmlSanitized: App.currentDisplay.title || App.config.name,
    });

    // special mode
    if (App.isSpecialMode) App.$footer.div({
      class: "footer-special_mode",
      title: "click to exit special mode",
    }).click(function () {
      App.exitSpecialMode();
    }).span({ htmlSanitized: "Special '"+ App.isSpecialMode +"' mode is active.", }).span({
      class: "footer-cross_icon icon-cross",
    });

    // filter
    if (App.currentFilter) App.$footer.div({
      class: "footer-filter",
      title: "click to clear current filter",
    }).click(function () {
      App.filter();
    }).span({ htmlSanitized: "A filter is currently applied.", }).span({
      class: "footer-cross_icon icon-cross",
    });

    // counts
    App.$footer.div({
      class: "footer-count",
      htmlSanitized: (App.selected && App.selected.length ? App.selected.length +" entries selected, " : "")+ App.currentDisplayedTable.length +" entries displayed of "+ App.Collection.length +" in the database",
    }).click(function () {
      App.toggleDisplayOfHiddenEntries();
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  BODY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: update display of body
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  makeBody: function () {
    var App = this;

    // setup background
    App.makeBackground();

    // empty container in case of reloading
    App.$body.empty();

    // add customized display message on top of body
    if (App.currentDisplay.bodyHeader) {
      var $bodyHeader = App.$body.div({ class: "tablify-customized_body_header", });
      if (_.isFunction(App.currentDisplay.bodyHeader)) App.currentDisplay.bodyHeader.call(App, $bodyHeader)
      else $bodyHeader.htmlSanitized((App.currentDisplay.bodyHeader + "").replace(/\n/g, "<br>"));
    };

    // custom make body for this layout
    if (layouts[App.currentDisplay.currentLayout].makeBody) layouts[App.currentDisplay.currentLayout].makeBody.call(App);

    // make object containing the list of entries to display, and other info about the current sorting
    var listDisplayObject = App.generateDisplayLists();
    App.currentDisplayedTable = listDisplayObject.list;

    // make body for this layout
    App.addAllRows(listDisplayObject);

    // display message to propose to create entries if no entries at all in the database
    if (!App.Collection.models.length) {
      var $noEntriesMessage = App.$body.div({ class: "no_entries-message", });
      $noEntriesMessage.div ({ htmlSanitized: 'There are no entries in this database, you can create some with the <span class="icon-plus"></span> in the toolbar.', });
      uify.button({
        $container: $noEntriesMessage,
        // icomoon: "question2",
        title: "<b>please show me how to use this page</b>",
        inlineTitle: "right",
        click: function () { App.startTutorial(); },
      });
    };

    // make body fab
    App.makeFab();

  },

  /**
    DESCRIPTION: update display of background
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  makeBackground: function () {
    var App = this;

    // get background url from string or attachment
    var backgroundUrl = App.getBackgroundUrl();

    // background image
    App.table.css("background-image", backgroundUrl ? "url("+ backgroundUrl +")" : "");

    // background credits
    if (backgroundUrl && App.currentDisplay.backgroundCredit) {
      if (App.$backgroundCredit) App.$backgroundCredit.remove();
      App.$backgroundCredit = App.table.div({
        class: "background-credits",
        htmlSanitized: $$.result(App.currentDisplay.backgroundCredit, backgroundUrl),
      });
    };

  },

  /**
    DESCRIPTION: get url of background to use
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  getBackgroundUrl: function () {
    var App = this;

    var backgroundUrl = App.currentDisplay.background;

    // no background
    if (!backgroundUrl) return "";

    // background as attachment
    else if (_.isObject(backgroundUrl)) {

      // get attachments object from folder or main config
      var currentFolderModel = App.Collection.get(App.currentPathId);
      if (currentFolderModel) var attachmentsLocation = currentFolderModel.get("_attachments") || {};
      else var attachmentsLocation = App.config._attachments || {};

      // get attachment from _attachments object
      var attachment = attachmentsLocation[backgroundUrl.attachmentId];

      // missing attachment
      if (!attachment) return "";
      // make base64 url from attachment
      else return $$.url.makeBase64(attachment.data, attachment.content_type);

    }

    // return background url
    else return backgroundUrl;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE/REFRESH BODY'S FAB
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: make/refresh body's fab button
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  makeFab: function () {
    var App = this;

    // remove previous fab if there is one
    if (App.bodyFab) {
      App.bodyFab.$button.remove();
      delete App.bodyFab;
    };

    // create fab if touch device or explicitely asked
    if (App.config.display.fab == "all" || (App.config.display.fab == "touchDevices" && $$.isTouchDevice())) {

      // new entry if nothing selected
      if (!App.selected || !App.selected.length) App.bodyFab = uify.fab({
        $container: App.$body,
        icomoon: "plus",
        title: "add new entry",
        click: function () {
          App.createNewEntry();
        },
      })

      // edit entries if something selected
      else App.bodyFab = uify.fab({
        $container: App.$body,
        icomoon: "quill",
        title: "edit selected entry/entries",
        click: function () {
          App.editSelectedEntries();
        },
      });

    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ADD ALL ROWS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  addAllRows: function (listDisplayObject) {
    var App = this;

    // (re)initialize App.$entries and App.entries list
    App.$entries = $();
    App.entries = [];

    //
    //                              ADD EACH GROUP AND IT'S ROWS

    var currentGrouping = App.getGrouping("grouping");
    if (currentGrouping) {

      // GET LIST OF ENTRIES BY GROUP
      var groupsToDisplay = _.groupBy(App.currentDisplayedTable, function (model, modelIndexInCurrentList) {
        return getGroupingIntertitleValue(currentGrouping, model, listDisplayObject.sortValues[modelIndexInCurrentList]);
      });

      // GET LIST OF GROUPS IN ORDER (WITH UNDEFINED AT START)
      var orderedListOfGroups = _.sortBy(_.keys(groupsToDisplay), function (groupName) {
        if (!groupName || groupName == "undefined") return ""
        else return groupName;
      });
      if (App.getSortingAndReverse().sortReverse) orderedListOfGroups.reverse();

      // RESET currentDisplayedTable TO REPUT ENTRIES IN IT IN ORDER (GROUPING HAS MODIFIED ORDER)
      App.currentDisplayedTable = [];
      // DISPLAY GROUPS AND THEIR ENTRIES
      _.each(orderedListOfGroups, function (groupName) {
        // display group title
        if (
          !layouts[App.currentDisplay.currentLayout].noGrouping
          && groupName
          && groupName != "undefined"
        ) App.addGroupIntertitleRow(groupName);
        // display group entries
        _.each(groupsToDisplay[groupName], function (model) {
          App.currentDisplayedTable.push(model);
          App.addRow(model);
        });
      });

    }

    //
    //                              JUST ADD ALL ROWS

    else _.each(App.currentDisplayedTable, function (model, modelIndexInCurrentList) {
      App.addRow(model);
    });

    //
    //                              APPEND SEE MORE BUTTON

    if (listDisplayObject.hasMoreAfter) App.appendSeeMoreButton();
    // TODO: still missing support for hasMoreBefore, to implement some day

    //
    //                              ALLOW TO DRAG ENTRIES ONTO EACH OTHER (TO MOVE THEM)

    // if (App.config.pathsDatabase) App.enableMoveOnDrag();
    if (App.config.pathsDatabase && App.$entries.length) App.enableDragAgtions();

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DRAG ACTIONS ON ENTRIES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  enableDragAgtions: function () {
    var App = this;

    var initialOrder;

    // o            >            +            #            °            ·            ^            :            |
    //                                           BEFORE LAUNCHING SORTABLEJS HOOK addEventListener METHOD TO CUSTOMIZE IT

    var defaultAddEventListenerMethod = document.addEventListener;
    document.addEventListener = function () {
      // drop "mouseup" and "touchend" events bindings, because they cause clicking any element in the page other than entries to remove "selected" class on entries (while tablify still considers them selected, so UI-wise it sucks)
      if (arguments[0] !== "mouseup" && arguments[0] !== "touchend") defaultAddEventListenerMethod.apply(this, arguments);
    };

    // o            >            +            #            °            ·            ^            :            |
    //                                           LAUNCH SORTABLEJS

    App.sortableEntriesObject = Sortablejs.create(App.$body[0], {
      group: "tablify-sort-"+ App.config.name,
      // swapThreshold: 0.25,
      // invertSwap: true,
      multiDrag: true, // enable multi-drag (if multiple elements are selected at the same time, dragging one will drag them all together)
      multiDragKey: "ctrl+alt+cmd+shift+a", // just something impossible, so I can handle it manually
      selectedClass: "selected", // class of elements that should be dragged together
      fallbackTolerance: 3, // so that we can select items on mobile
      animation: 150,
      handle: $$.isTouchDevice() ? ".image" : undefined,
      onMove: function (evt, e) {
        // do not trigger UI move if not in "sort" mode
        if (
          (currentMovement === "sort" || App.isSpecialMode === "organize")
          && App.manualSortingIsEnabled && App.currentDisplay.currentLayout !== "free"
        ) return true
        else return false;
      },
      onSort: function (evt) {
        // trigger sort
        if (
          (currentMovement === "sort" || App.isSpecialMode === "organize")
          && App.manualSortingIsEnabled && App.currentDisplay.currentLayout !== "free"
        ) {

          // save new ordering of entries
          initialOrder = App.sortableEntriesObject.toArray();

          // regenerate list of entries and set their index in the model
          App.$entries = $();
          App.$body.children().each(function (i) {
            App.$entries.add(this);

            // save index if it changed
            var model = App.Collection.get(this.id);
            var previousIndex = model.getValue("system.positionIn_"+ App.currentPathId +".i");
            if (
              // index was never specified for this entry
              _.isUndefined(previousIndex) ||
              // index has changed for this entry
              previousIndex !== i
            ) {
              model.setValue("system.positionIn_"+ App.currentPathId +".i", i);
              model.saveToPouch();
            };

          });

        }
        // or reverse to how list was before starting dragging
        else App.sortableEntriesObject.sort(initialOrder, true);
      },
      onEnd: function (evt) {
        if (App.config.pathsDatabase && (currentMovement === "move" || currentMovement === "alias")) {

          // FIGURE OUT ELEMENTS TO MOVE/ALIAS
          var droppedElementsIds = evt.items.length > 1 ? _.pluck(evt.items, "id") : [evt.item.id];
          var droppedElements = _.map(droppedElementsIds, function (id) { return App.Collection.get(id); });

          // FIGURE OUT TARGET
          var $movedOnto = $(evt.explicitOriginalTarget);
          if ($movedOnto.hasClass("tablify-entry")) var targetId = evt.explicitOriginalTarget.id
          else if ($movedOnto.parents(".tablify-entry").length) var targetId = $movedOnto.parents(".tablify-entry")[0].id
          else if ($movedOnto.hasClass("tablify-entries_can_be_dropped_here")) var targetPath = $movedOnto.data().path
          else if ($movedOnto.parents(".tablify-entries_can_be_dropped_here").length) var targetPath = $movedOnto.parents(".tablify-entries_can_be_dropped_here").data().path
          else $$.log.error("The element you dropped entry on doesn't seem to be an appropriate target or target sub-element.", $movedOnto);

          // MOVE/ALIAS ENTRIES
          _.each(droppedElements, function (droppedElement) {
            // MOVE ELEMENT TO TARGET (IF IT'S AN APPROPRIATE FOLDER)
            // dragging onto pathbar
            if (targetPath) App.move(droppedElement, targetPath, currentMovement === "alias")
            // dragging onto another entry
            else if (targetId) {
              var droppedTarget = App.Collection.get(targetId);
              // move into folder, but do not move a folder into itself
              if (droppedTarget.get("type") === "folder" && droppedTarget.get("_id") !== droppedElement.get("_id")) App.move(droppedElement, droppedTarget, currentMovement === "alias");
            };
          });

        };

      },
    });

    // o            >            +            #            °            ·            ^            :            |
    //                                           REMOVE addEventListener HOOK

    document.addEventListener = defaultAddEventListenerMethod;

    // o            >            +            #            °            ·            ^            :            |
    //                                           SAVE INITIAL ORDERING

    initialOrder = App.sortableEntriesObject.toArray();

    // o            >            +            #            °            ·            ^            :            |
    //                                           SUPPORT BUTTONS IN PATH BAR

    Sortablejs.create(App.$pathbar[0], {
      group: {
        name: "tablify-sort-"+ App.config.name,
        pull: false,
      },
    });

    //                                           ¬
    // o            >            +            #            °            ·            ^            :            |

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ADD ONE ROW
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  addRow: function (model) {
    var App = this;

    // create view
    var entryView = new App.EntryViewMaker({ model: model, App: App, });
    // append view content
    entryView.render(App.$body);

    // add entry html object to list of entries
    App.$entries = App.$entries.add(entryView.$el);

    // add entryView to list of entries
    App.entries.push(entryView);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SEE MORE BUTTON
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  appendSeeMoreButton: function () {
    var App = this;
    App.$body.div({
      class: "see_after",
      htmlSanitized: "<span>+ display "+ App.currentDisplay.limit +" more entries +</span>",
    }).click(function (event) {
      event.stopPropagation()
      App.currentDisplay.limit += App.currentDisplay.limit;
      App.refresh();
    });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GROUPING INTERTITLES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: add grouping intertitle row
    ARGUMENTS: ( ?thisEntryGroupIntertitle <string> « value of the group intertitle » )
    RETURN: <void>
  */
  addGroupIntertitleRow: function (thisEntryGroupIntertitle) {
    var App = this;

    var currentGroupingCustomDisplay = App.getGrouping("groupingCustomDisplay");

    // create grouping intertitle div
    var $groupingIntertitleDiv = App.$body.div({ class: "tablify-group", });

    // has a custom group display
    if (_.isFunction(currentGroupingCustomDisplay)) currentGroupingCustomDisplay.call(App, $groupingIntertitleDiv, thisEntryGroupIntertitle)
    // no custom group display
    else $groupingIntertitleDiv.htmlSanitized(thisEntryGroupIntertitle || "unknown group");

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
