var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/storage");
var uify = require("uify");
require("uify/extension/differences");
var dialogify = require("dialogify");
var async = require("async");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  PROCESS AND MODIFY MULTIPLE ENTRIES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/*
  DESCRIPTION: create a dialog to allow the user to run a function on some entries to modify them
  ARGUMENTS: (
    !App <tablifyApp>,
    ?entriesToProcess <couchEntry[]> «
      if detined, will use this list of entries instead of trying to figure out which entries you wanted to process
    »,
  )
  RETURN: <void>
*/
function processAndModifyEntries (App, entriesToProcess) {

  // create dialog to write code for processing entries
  var dialog = dialogify.textarea({
    title: "Batch modification of "+ entriesToProcess.length +" entries",
    comment: "Write here the javascript code that will modify each entry.<br>The code you write here will be wrapped in a function statement and evaluated.<br>In this function, the variable 'entry' represent each currently modified entry at turn.<br>Don't forget to return the final entry you want to save to the database.<br><br>",
    height: "80",
    input: {
      monospace: true,
      cssContainer: { padding: 0, },
      cssInput: { fontSize: "0.7em", },
      defaultValue: $$.storage.session("modifFuncText"),
    },
    buttons: function (buttons) {
      buttons.unshift({
        title: "process with simplified confirm",
        class: "error",
        click: function (value, e) {
          var dialog = this;

          // get input's value
          var modifFuncText = dialog.inputified.get();

          // store the text that was written to sessionStorage
          $$.storage.session("modifFuncText", modifFuncText);

          // make modifications
          return processEntries(App, entriesToProcess, modifFuncText, true);

        },
      });
      return buttons;
    },
    ok: function (modifFuncText) {

      // store the text that was written to sessionStorage
      $$.storage.session("modifFuncText", modifFuncText);

      // make modifications
      return processEntries(App, entriesToProcess, modifFuncText);

    },
    cancel: function () {
      return confirm("Close window and lose modifications?");
    },
  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  PROCESS ENTRIES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function processEntries (App, entriesBefore, modifFuncText, basicConfirm) {

  //
  //                              EVALUATE THE FUNCTION THAT WAS PASSED

  try {
    eval("var funcToApply = function (entry) {"+ modifFuncText +"}");
  }
  catch (e) {
    uify.alert.error({
      content: e,
      // reopen the dialog on error
      ok: function () { processAndModifyEntries(App, entriesBefore); },
    })
    return;
  };

  //
  //                              MAKE PROCESS PROGRESS DIALOG

  var progressified = uify.progress({
    title: "Processing "+ entriesBefore.length +" entries",
    steps: entriesBefore.length,
  });

  //
  //                              PROCESS ENTRIES ASYNCHRONOUSLY

  async.waterfall([

    // clone entries
    function (next) {
      var entriesToProcess = _.map(entriesBefore, function (entry) { return $$.clone(entry, 10); });
      next(null, entriesToProcess);
    },

    // process entries
    function (entriesToProcess, next) {

      async.mapLimit(entriesToProcess, 5, function (entry, callback) {
        setTimeout(function () {
          async.asyncify(funcToApply)(entry, function (err, result) {
            progressified.refresh();
            callback(err, result);
          });
        }, 10);
      }, next);

    },

  ], function (err, entriesAfter) {

    if (err) {
      progressified.destroy();
      uify.toast.error("Error processing entries. See logs for details.");
      $$.log.error("[tablify] Error processing entries: ", err);
      processAndModifyEntries(App, entriesBefore);
      return;
    };

    App.calculateAndDisplayDifferences({
      entriesBefore: entriesBefore,
      entriesAfter: entriesAfter,
      basicConfirm: basicConfirm,
      ok: function (modifiedEntriesFinalList) {
        App.Collection.bulkPut(modifiedEntriesFinalList, {
          success: "Successfully modified %%count%% entries.",
          warningSuccess: "Modified %%count%% entries successfully.",
          warningFail: "Failed to modify %%count%% entries.",
          error: "Error trying to modify %%count%% entries.",
        });
      },
      cancel: function () { processAndModifyEntries(App, entriesBefore); },
    });

  });

  //                              ¬
  //

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY DIFFERENCES AND ASK CONFIRMATION
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: show a dialog detailing differences in the given list of modified entries
    ARGUMENTS: ({
      ?title: <string>,
      !entriesPairs <{
        !before: <couchEntry>,
        !after: <couchEntry>,
      },
      !ok <function>,
      !cancel <function>,
    })
    RETURN: <void>
  */
  displayDifferencesAndAskConfirmation: function (options) {
    var App = this;

    // comparisons dialog
    var dialogified = uify.dialog({
      title: options.title,
      width: 95,
      height: 95,
      overlayClickExits: false,
      buttons: function (butttons, allDefaultButtons) {
        return [
          {
            title: "toggle full result",
            click: function (e) {
              dialogified.$content.find(".uify-differences-after").toggle();
              return false;
            },
          },
          allDefaultButtons.cancel,
          allDefaultButtons.ok,
        ];
      },
      content: function ($contentContainer) {

        // make progress dialog (after dialog has been created)
        var progressified = uify.progress({
          title: "Preparing comparison display for "+ options.entriesPairs.length +" entries",
          steps: options.entriesPairs.length,
        });

        $contentContainer.addClass("tablify-entry_diffences-confirm");

        // display differences for each modified entry
        async.eachLimit(options.entriesPairs, 5, function (entryPair, next) {
          setTimeout(function () {
            uify.differences({
              $container: $contentContainer,
              before: entryPair.before,
              after: entryPair.after,
              displayAfter: "Entry to save:",
            });
            progressified.refresh();
            next(null);
          }, 10);
        }, function (err) {
          if (err) $$.log.error("[tablify] Error displaying entries"+ err);
        });

      },
      ok: options.ok,
      cancel: options.cancel,
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CALCULATE MODIFICATIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: calculate and display modifications on entries, and ask to confirm if proceed with them
    ARGUMENTS: ({
      entriesBefore: <couchEntry[]>,
      entriesAfter: <couchEntry[]>,
      basicConfirm: <boolean> « if true, will not display full featured comparison, just a confirm showing the number of entries modified »,
      ok: <function(
        <{
          before: <couchEntry>,
          after: <couchEntry>,
        }[]>
      )>,
      cancel: <function(ø)>,
    })
    RETURN: <void>
  */
  calculateAndDisplayDifferences: function (options) {
    var App = this;

    // make process progress dialog
    var progressified = uify.progress({
      title: "Calculations modifications on "+ options.entriesBefore.length +" entries",
      steps: options.entriesBefore.length,
    });

    // process asynchronously
    async.waterfall([

      // verify which entry was really modified
      function (next) {

        async.mapValuesLimit(options.entriesAfter, 5, function (modifiedEntry, index, callback) {
          setTimeout(function () {
            progressified.refresh();
            var entryBefore = modifiedEntry ? _.findWhere(options.entriesBefore, { _id: modifiedEntry._id }) : undefined;
            callback(null, {
              before: entryBefore,
              after: modifiedEntry,
              isModified: modifiedEntry && !_.isEqual(entryBefore, modifiedEntry),
            });
          }, 10);
        }, next);

      },

      // keep only modified
      function (entriesObjects, next) {
        next(null, _.where(entriesObjects, { isModified: true, }));
      },

    ], function (err, entriesPairs) {

      if (err) {
        uify.toast.error("Error calculating modification on entries. See logs for details.");
        $$.log.error("[tablify] Error calculating modification on entries: ", err);
        progressified.destroy();
        return;
      };


      // show differences confirm
      if (entriesPairs.length) {
        var dialogTitle = "Are you sure you want to apply changes to "+ entriesPairs.length +" entries on "+ options.entriesBefore.length +"?";
        if (options.basicConfirm) uify.confirm({
          content: dialogTitle,
          type: "warning",
          ok: function () { return options.ok(_.pluck(entriesPairs, "after")); },
          cancel: options.cancel,
        })
        else App.displayDifferencesAndAskConfirmation({
          title: dialogTitle,
          entriesPairs: entriesPairs,
          ok: function () { return options.ok(_.pluck(entriesPairs, "after")); },
          cancel: options.cancel,
        });
      }
      else uify.alert("No modifications needed.");

    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  PROCESS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  processAndModifyMultipleEntries: function () {
    var App = this;

    // identify which entries should be processed
    if (!App.selected || !App.selected.length) {
      if (confirm("No entries selected.\nDo you want to batch process all entries in the database?")) var entriesToProcess = App.Collection.toJSON()
      else return
    }
    else var entriesToProcess = _.pluck(App.selected, "attributes");

    // open dialog to start inputing function to evaluate
    processAndModifyEntries(App, entriesToProcess);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
