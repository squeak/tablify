var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/string");
var dialogify = require("dialogify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CHECK SIBLING VALUE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function checkSiblingValueEqual (inputified, siblingKey, siblingValue) {
  return _.isEqual(inputified.getSiblingValue(siblingKey), siblingValue);
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SEARCH STRING FILTER
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/*
  DESCRIPTION: filter function to apply to a model to see if it matches a given query string
  ARGUMENTS: (
    !App,
    !model,
    !searchString <string|regexp> « the value to match in this entry »,
    ?ignoreCustomCharacters <boolean> « if true, will remove all non alphanumeric characters in strings to match »,
  )
  RETURN: <boolean> « true if matches the searched string »
*/
function searchStringFilteree (App, model, searchString, ignoreCustomCharacters) {

  // make filtering regular expression
  var filterRegexp = $$.regexp.make(searchString, "i"); // ignore case (will only be applied if string is not a regexp, or regexp like string, but that's the intended behaviour)

  // create string to find matches in from entry json
  var searchIn = JSON.stringify(model.toJSON());

  // add custom searches values to search string
  var additionalSearches = []
  if (App.currentDisplay.searching) additionalSearches.push(App.currentDisplay.searching.call(App, model));
  _.each(App.currentDisplay.columns, function (columnConfig) {
    if (columnConfig.searching) additionalSearches.push(columnConfig.searching.call(App, model));
  });
  if (additionalSearches.length) searchIn += _.map(additionalSearches, function (val) {
    return _.isString(val) ? val : JSON.stringify(val);
  }).join(" ");

  // if custom characters should be ignored
  if (ignoreCustomCharacters) searchIn = searchIn.replace(/\W*/g, "");

  // return matching
  return searchIn.match(filterRegexp);


};

/*
  DESCRIPTION: filter function to apply to a model to see if it matches a given query object
  ARGUMENTS: (
    !App,
    !model,
    !searchQuery <{
      [deepKey <string>]: <string|regexp> « the value to match at this key »,
    }>
    ?ignoreCustomCharacters <boolean> « if true, will remove all non alphanumeric characters in strings to match »,
  )
  RETURN: <boolean> « true if matches the searched query object »
*/
function whereLikeFilteree (App, model, searchQuery, ignoreCustomCharacters) {

  var hasBroken = $$.breakableEach(searchQuery, function (searchString, deepKey) {

    // make regexp from search string
    var filterRegexp = $$.regexp.make(searchString, "i"); // ignore case (will only be applied if string is not a regexp, or regexp like string, but that's the intended behaviour)

    // get value to search in as a string
    var searchIn = model.getValue(deepKey);
    if (!_.isString(searchIn)) searchIn = JSON.stringify(searchIn) || "";

    // add potential additional searches from column config
    var columnConfig = _.findWhere(App.currentDisplay.columns, { key: deepKey, });
    if (columnConfig && columnConfig.searching) {
      var additionalSearch = columnConfig.searching.call(App, model);
      searchIn += _.isString(additionalSearch) ? additionalSearch : JSON.stringify(additionalSearch);
    };

    // if custom characters should be ignored
    if (ignoreCustomCharacters) searchIn = searchIn.replace(/\W*/g, "");

    // anything doesn't match: break and stop verifs for this entry
    if (!searchIn.match(filterRegexp)) return "break";

  });

  return hasBroken === "break" ? false : true;

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY SEARCH DIALOG
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: display search dialog
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  searchDialog: function () {
    var App = this;

    dialogify.object({
      // o            >            +            #            °            ·            ^            :            |

      title: "Search entries in the database",

      structure: [

        //
        //                              FOLDER OR WHOLE DB

        {
          name: "where",
          type: "radio",
          // label: "Where do you want to search it?",
          labelLayout: "hidden",
          defaultValue: "folder",
          condition: function () { return App.config.pathsDatabase; },
          conditionIsImperative: true,
          modified: function () { this.doOnSiblings("recalculateDisplayCondition", "how") },
          choices: [
            {
              _isChoice: true,
              value: "folder",
              label: "in this folder",
            },
            {
              _isChoice: true,
              value: "database",
              label: "in the whole database",
            },
          ],
        },


        //
        //                              SEARCH STRING/REGEXP

        {
          name: "searchString",
          labelLayout: "hidden",
          type: "normal",
          focus: true,
          input: { placeholder: "What do you want to search?", },
          value: function () { return App.lastSearchedString; }, // somehow, if not in a function there are some bugs when input is refreshed
          modified: function (value) {
            App.lastSearchedString = value;
            this.doOnSiblings("recalculateDisplayCondition", "isRegexp");
          },
          condition: function () { return !this.getSiblingValue("whereLikeSearch"); },
          conditionIsImperative: true,
        },

        //
        //                              DEEP.KEY IN WHICH TO SEARCH FOR STRING MATCHES

        {
          name: "searchQuery",
          labelLayout: "stacked",
          label: "Search query",
          type: "object",
          value: function () { return App.lastSearchedQuery; }, // somehow, if not in a function there are some bugs when input is refreshed
          modified: function (value) { App.lastSearchedQuery = value; },
          help: "You may use deep keys as well.",
          condition: function () { return this.getSiblingValue("whereLikeSearch"); },
          conditionIsImperative: true,
        },

        //
        //                              SIMPLE OR ADVANCED

        {
          name: "advanced",
          type: "boolean",
          label: "Advanced options",
          defaultValue: false,
          modified: function () {
            this.doOnSiblings("recalculateDisplayCondition", ["advancedIntertitle", "how", "whereLikeSearch", "ignoreCustomCharacters", "isRegexp"]);
          },
        },

        {
          label: "ADVANCED OPTIONS",
          name: "advancedIntertitle",
          type: "intertitle",
          condition: function () { return checkSiblingValueEqual(this, "advanced", true); },
        },

        //
        //                              FILTER OR FIND

        {
          name: "how",
          type: "radio",
          label: "Filter or highlight?",
          labelLayout: "stacked",
          defaultValue: "filter",
          condition: function () {
            // advanced on
            // &&
            // either db is not a pathsDatabase or chose to search only in this folder
            return checkSiblingValueEqual(this, "advanced", true) && (!App.config.pathsDatabase || checkSiblingValueEqual(this, "where", "folder"));
          },
          conditionIsImperative: true,
          choices: [
            {
              _isChoice: true,
              value: "find",
              label: "find and highlight entry in this list",
            },
            {
              _isChoice: true,
              value: "filter",
              label: "filter entries",
            },
          ],
        },

        //
        //                              WHERE LIKE MANGO SEARCH

        {
          name: "whereLikeSearch",
          type: "boolean",
          label: "Use 'complex search' mode",
          help: "Use this if you don't want to search generally a simple value in entries. But instead, have a more fine grained search in which you can match some values in some specific fields.",
          defaultValue: false,
          condition: function () { return checkSiblingValueEqual(this, "advanced", true); },
          conditionIsImperative: true,
          modified: function () { this.doOnSiblings("recalculateDisplayCondition", ["searchString", "searchQuery"]); },
        },

        //
        //                              IGNORE SPACES AND SPECIAL CHARACTERS

        {
          name: "ignoreCustomCharacters",
          type: "boolean",
          label: "Ignore spaces and special characters",
          help: [
            "Any non alphabetical or number character is a special character",
            "For example: 'MrElliot' will match 'Mr. Elliot'.",
          ],
          defaultValue: false,
          condition: function () { return checkSiblingValueEqual(this, "advanced", true); },
          conditionIsImperative: true,
        },

        //
        //                              REGEXP OR STRING PASSED

        {
          name: "isRegexp",
          type: "boolean",
          locked: true,
          label: "The search string you passed is a regular expression",
          help: [
            "In the previous search field you can:",
            "&ensp;- pass a search string, which will be compared in a case insensitive way",
            "<small>(e.g. \"match this\" and \"Match THIS\" will match the same entries)</small>",
            "&ensp;- pass a regular expression, with any flags you want",
            "<small>(e.g. /^Start/ and /^Start/i may match different entries)</small>",
          ],
          value: function () { return $$.isRegExpString(this.getSiblingValue("searchString")); },
          condition: function () { return checkSiblingValueEqual(this, "advanced", true); },
          conditionIsImperative: true,
        },

        //                              ¬
        //

      ],

      // o            >            +            #            °            ·            ^            :            |

      ok: function (form) {

        // make filteree method
        if (form.whereLikeSearch) var filterFunc = function (model) { return whereLikeFilteree(App, model, form.searchQuery, form.ignoreCustomCharacters) }
        else var filterFunc = function (model) { return searchStringFilteree(App, model, form.searchString, form.ignoreCustomCharacters); };

        // find or filter entries
        if (form.how == "find") App.find(filterFunc)
        else {
          if (App.config.pathsDatabase && form.where == "database") App.filterAll(filterFunc)
          else App.filter(filterFunc);
        };

      },

      cancel: function () {
        // do nothing, (cancel is defined because otherwise it asks to confirm cancel)
      },

      // o            >            +            #            °            ·            ^            :            |
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FILTERING
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: filter entries to view
    ARGUMENTS: ( ?filterFunc <function(model):<boolean>>, )
    RETURN: <void>
  */
  filter: function (filterFunc) {
    var App = this;
    App.currentFilter = filterFunc;
    App.refresh();
  },

  /**
    DESCRIPTION: filter entries to view (in the whole database)
    ARGUMENTS: ( ?filterFunc <function(model):<boolean>>, )
    RETURN: <void>
  */
  filterAll: function (filterFunc) {
    var App = this;
    App.currentFilter = filterFunc;
    App.openPath("ALL", true);
  },

  /**
    DESCRIPTION: find one entry in view (select it)
    ARGUMENTS: (
      ?filter <string>,
      ?keyWhereToFindMatches <string> « if defined, will only search matches in value at this key »,
    )
    RETURN: <void>
  */
  find: function (filterFunc) {
    var App = this;
    var matchingModel = _.find(App.currentDisplayedTable, filterFunc);
    if (matchingModel) App.select(matchingModel, true)
    else uify.toast("No match found.");
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
