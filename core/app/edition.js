var _ = require("underscore");
var $$ = require("squeak");
var $ = require("yquerj");
var postify = require("postify");
var uify = require("uify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  ADD MISSING NAME AND PATH KEYS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function addMissingPathInEditionType (App, editOptions) {

  if (!editOptions.structure) editOptions.structure = [];

  var hasPath = _.find(editOptions.structure, function (strucEntry) { return strucEntry.key == "path" || strucEntry.name == "path"; });
  if (!hasPath) editOptions.structure.unshift({
    key: "path",
    // inputifyType: "paths",
    help: "Path to this entry.\nYou can specify multiple paths here, this means the entry will be accessible from different folders.",
    isAdvanced: true,
    hideAdvancedEvenIfDefined: true,
    inputifyType: "suggest",
    mandatory: true,
    choices: function (callback) { callback(App.Collection.getAllPathsChoices()); },
  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CHOSE NEW ENTRY TYPE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function figureOutShortcutLetter (lettersUsed, string) {
  if (!string) return;
  for (var i = 0; i < string.length; i++) {
    if (!lettersUsed[string[i]]) {
      lettersUsed[string[i]] = true;
      return string[i];
    };
  };
};

function choseNewEntryType (App, possibleTypes) {

  // GET LIST OF TYPES CONFIGURATIONS
  var typesToPropose = _.map(possibleTypes, function (typeName) {
    return _.findWhere(App.config.entryTypes, { name: typeName, }) || {};
  });

  // MAKE LIST OF USED LETTER FOR SHORTCUT
  var lettersUsed = {};
  _.each(typesToPropose, function (typeConfig) {
    var shortcutKey = $$.getValue(typeConfig, "display.creationMenu.shortcuts");
    if (shortcutKey) lettersUsed[shortcutKey] = true;
  });

  // MAKE MENU
  uify.menu({
    title: "What type of entry do you want to create?",
    buttons: _.map(typesToPropose, function (typeConfig) {

      var creationDisplayConfig = $$.getValue(typeConfig, "display.creationMenu") || {};

      // figure out name
      var typeName = creationDisplayConfig.name || typeConfig.name || "";

      // figure out shortcut
      if (creationDisplayConfig.shortcut) var typeShortcut = creationDisplayConfig.shortcut
      else var typeShortcut = figureOutShortcutLetter(lettersUsed, typeName);

      // return menu button config
      return {
        title: typeName,
        key: typeShortcut,
        icomoon: creationDisplayConfig.icon,
        click: function () { App.newEntry(typeConfig.name); },
      };

    }),
  });

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: open postify view for the given entry
    ARGUMENTS: (
      !model <backboneModel>,
      !typeConfig <entryType> « configuration for this entry type »,
      ?addToHash <function(ø)> « will add entry id to hash when executed »,
      ?removeFromHash <function(ø)> « will remove entry id from hash when executed (should be ran when entry is closed) »,
      ?isNewEntry <boolean> « pass true if the entry is a new one to create »,
    )
    RETURN: <void>
  */
  editInPostify: function (model, typeConfig, addToHash, removeFromHash, isNewEntry) {
    var App = this;

    if (addToHash) addToHash();

    // make default postify options
    var postifyOptions = {

      // general
      $container: App.$editorContainer,
      isNewEntry: isNewEntry,
      displayFab: App.config.display.fab,

      // saving
      save: function (entry, saveCallback) {
        App.Collection.put(entry, function (pouchResponse) {

          // save to pouch error
          if (!pouchResponse.ok) {
            uify.alert.error("Seems something went wrong when trying to save entry to pouch.<br>Check console for more logs.");
            $$.log.error("Seems something went wrong when trying to save entry to pouch:", pouchResponse);
            return;
          };

          // run postify callback
          var savedEntry = $$.clone(entry, 99);
          savedEntry._rev = pouchResponse.rev;
          var newModel = _.clone(model);
          newModel.attributes = savedEntry;
          saveCallback(savedEntry);

          // removed and saved callbacks if they exist
          if (entry._deleted) {
            var customRemovedFunction = $$.getValue(typeConfig, "event.removed");
            if (_.isFunction(customRemovedFunction)) customRemovedFunction.call(App, entry);
          }
          else {
            var customSavedFunction = $$.getValue(typeConfig, "event.saved");
            if (_.isFunction(customSavedFunction)) customSavedFunction.call(App, entry, model.attributes, isNewEntry);
          };

        });
      },
      closed: function () { if (removeFromHash) removeFromHash(); },

      // attach some things to be able to use them from some methods inside
      storage: {
        collection: App.Collection,
        tablifyApp: App,
        typeConfig: typeConfig,
      },

      // options that should be calculated by windify (so it will be recalculated at each refreshing of the window)
      windifyOptionsMaker: function (options) {
        var modelWithUpdatedContent = _.clone(model);
        if (options && options.entry) modelWithUpdatedContent.attributes = options.entry;
        return modelWithUpdatedContent.getWindifyDisplayValues(typeConfig);
      },

      // options that should be calculated by postify (so it will be recalculated at each refreshing of the window)
      postifyOptionsMaker: function () {
        // handle path database case
        if (App.config.pathsDatabase && !postifyOptions.doNotAutoAddPath) addMissingPathInEditionType(App, typeConfig.edit);
        // options passed in type config edit section
        return typeConfig.edit;
      },

    };

    model.fetchWithAttachments(function () {

      // add model attributes that now contain full attachments
      postifyOptions.entry = model.attributes; // attach it here, not before default because otherwise bug with $$.defaults function

      // create postify window
      var postified = postify(postifyOptions);

      // add class containing type to created window
      postified.windified.$window.addClass("postify-tablify--"+ typeConfig.name);

    });

  },

  /**
    DESCRIPTION: open editor for the given entry
    ARGUMENTS: (
      !model <backboneModel> « must have an _id set »,
      ?isNewEntry <boolean> « pass true if the entry is a new one to create »,
      ?storeInHash <boolean|"e"|"v"> «
        if defined, will bypass default settings of storeInHash for this type,
        you can also specify in which part of hash you want to store it passing a string
      »,
    )
    RETURN: <void>
  */
  openEntryEditor: function (model, isNewEntry, storeInHash) {
    var App = this;

    // get type config for this entry type
    var typeConfig = model.getTypeConfig();

    // if _id is customizable, remove default uuid _id from new entries
    if (isNewEntry) {
      var idCustomizedInTypeConfig = _.findWhere($$.getValue(typeConfig, "edit.structure"), { key: "_id" });
      if (idCustomizedInTypeConfig) {
        if (!idCustomizedInTypeConfig.mandatory) {
          var errorMessage = "Allowing to customize entries _id without making it mandatory is not supported, since entries must have an _id.";
          uify.alert.error(errorMessage);
          $$.log.error(errorMessage);
        }
        else delete model.attributes._id;
      };
    };

    // add to list of entries opened to edition in hash (except if type config requested not to)
    function addToHash () {
      if (_.isUndefined(storeInHash)) storeInHash = _.indexOf($$.getValue(typeConfig, "action.storeInHash"), "edit") != -1 && !isNewEntry;
      if (storeInHash) App.addInHashList(model.id, _.isString(storeInHash) ? storeInHash : "e");
    };
    // remove from list of entries opened to edition in hash (except if type config requested not to)
    function removeFromHash () {
      if (storeInHash) App.removeFromHashList(model.id, _.isString(storeInHash) ? storeInHash : "e");
    };

    // open custom edit system or postify
    var customEditFunction = $$.getValue(typeConfig, "action.edit");
    if (_.isFunction(customEditFunction)) customEditFunction.call(App, model, typeConfig, addToHash, removeFromHash, isNewEntry)
    else App.editInPostify(model, typeConfig, addToHash, removeFromHash, isNewEntry);

    // run any global entry editor opening callback
    if (App.config.onEntryEdit) App.config.onEntryEdit.call(App);

  },

  /**
    DESCRIPTION: open postify to propose to create a new entry in table
    ARGUMENTS: ( ?typeOrPreobject <string|object> « if database accepts multiple entries types, set entry type here » )
    RETURN: <void>
  */
  newEntry: function (typeOrPreobject) {
    var App = this;
    var newEntry = { _id: $$.uuid(), };
    if (_.isObject(typeOrPreobject)) newEntry = $$.defaults(newEntry, typeOrPreobject)
    else if (_.isString(typeOrPreobject)) newEntry.type = typeOrPreobject;
    if (App.config.pathsDatabase) {
      var currentDirectoryPathId = App.Collection.getPathIdFromPath(App.currentPath);
      if (currentDirectoryPathId) newEntry.path = [currentDirectoryPathId];
    };
    var newEntryModel = App.Collection.newModel(newEntry);
    App.openEntryEditor(newEntryModel, true);
  },

  /**
    DESCRIPTION: open the currently selected entry edition window
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  editSelectedEntries: function () {
    var App = this;
    if (App.selected && App.selected.length) _.each(App.selected, function (model) { App.openEntryEditor(model); });
  },

  /**
    DESCRIPTION: open UI to create a new entry
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  createNewEntry: function () {
    var App = this;

    // get list of available types
    var availableTypes = App.listAvailableTypes();

    // make list of type from function, chose type, create entry
    if (_.isFunction(availableTypes)) availableTypes(function (possibleTypes) { choseNewEntryType(App, possibleTypes); })
    // chose type, create entry
    else if (_.isArray(availableTypes) && availableTypes.length > 1) choseNewEntryType(App, availableTypes)
    // create entry
    else App.newEntry(availableTypes[0]);

  },

  /**
    DESCRIPTION: get the list of available types in the database or current folder
    ARGUMENTS: ( ø )
    RETURN: <string[]>
  */
  listAvailableTypes: function () {
    var App = this;

    // get available types from app config
    var availableTypes = App.config.availableTypes || _.pluck(App.config.entryTypes, "name");

    // get availableTypes from folder if necessary and they are valid
    if (App.currentPathId) {
      var currentFolderModel = App.Collection.get(App.currentPathId);
      if (currentFolderModel) {
        var folderAvailableTypes = currentFolderModel.get("availableTypes");
        if (folderAvailableTypes) {
          var folderAvailableValidTypes = _.intersection(availableTypes, folderAvailableTypes);
          availableTypes = folderAvailableValidTypes.length ? folderAvailableValidTypes : availableTypes;
        };
      };
    };

    // return available types
    return availableTypes;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
