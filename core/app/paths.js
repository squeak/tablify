var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/dom");
var uify = require("uify");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MOVE ENTRY TO A FOLDER
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION:
      move the passed model
        - inside of a folder (the folder and the entry must be in the same folder at the moment of moving)
        - to the given path
    ARGUMENTS: (
      !modelToMove <backboneModel>,
      !targetFolderModelOrJustPath <backboneModel|string>,
      ?alwaysAddPath <boolean> « if true, the target path will be added to the list of this model's paths, in addition to the current one »,
    )
    RETURN: <void>
  */
  move: function (modelToMove, targetFolderModelOrJustPath, alwaysAddPath) {
    var App = this;

    // make sure path array exist in model to move
    var modelPaths = modelToMove.get("path") || []

    // pathId of the current location
    var currentFolderId = App.Collection.getPathIdFromPath(App.currentPath);

    // if a path was passed, get the model it corresponds to
    if (_.isString(targetFolderModelOrJustPath)) var targetFolderId = App.Collection.getPathIdFromPath(targetFolderModelOrJustPath);
    else var targetFolderId = targetFolderModelOrJustPath.id;

    // in case cannot find folder
    if (!targetFolderId) return uify.alert.error("Could not figure out where you want to move this entry.");

    // if modelToMove has current path, replace it by new one, if not add new one to it's paths
    var currentPathIndex = _.indexOf(modelPaths, currentFolderId);
    if (alwaysAddPath || currentPathIndex === -1) modelPaths.push(targetFolderId)
    else modelPaths[currentPathIndex] = targetFolderId;

    // save model
    modelToMove.set("path", modelPaths);
    modelToMove.saveToPouch(function (resultEntry) {

      // if there is a move callback for this type, execute it
      var customMovedFunction = $$.getValue(modelToMove.getTypeConfig(), "event.moved");
      if (_.isFunction(customMovedFunction)) customMovedFunction.call(App, currentFolderId, targetFolderId, resultEntry);
    });

  },

  // //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  // //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  // //                                                  ENABLING DRAGGING ENTRIES MOVES THEM
  // //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //
  // enableMoveOnDrag: function () {
  //   var App = this;
  //
  //   if (App.$entries.length) $$.dom.makeDraggable({
  //     $target: App.$entries,
  //     $dropGroup: App.$entries.add(App.$pathbar.find(".tablify-entries_can_be_dropped_here")),
  //     zAlso: true,
  //     cloneDrag: true,
  //     onDrop: function ($droppingTarget, $droppedElement) {
  //
  //       var droppedElement = App.Collection.get($droppedElement.attr("id"));
  //
  //       var targetData = $($droppingTarget[0]).data() || {};
  //       if (targetData.path) App.move(droppedElement, targetData.path)
  //       else {
  //         var droppedTarget = App.Collection.get($droppingTarget.attr("id"));
  //         // move into folder, but do not move a folder into itself
  //         if (droppedTarget.get("type") == "folder" && droppedTarget.get("_id") !== droppedElement.get("_id")) App.move(droppedElement, droppedTarget)
  //       };
  //
  //     },
  //     // keyToPress: "altKey",
  //   });
  //
  // },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
