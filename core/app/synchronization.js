var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/storage");
require("squeak/extension/url");
var uify = require("uify");
var dialogify = require("dialogify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  STATUS DISPLAY
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var allPotentialStatusesClasses = ["disabled", "active", "error", "warning", "done"];
function updateStatusClasses (App) {
  // set sync status on button
  App.toolbar.get("syncStatus").$button.attr("sync-status", App.syncStatus());
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  TOGGLE EXCLUSION BUTTON
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/*
  DESCRIPTION: display a button allowing to suspend/unsuspend the synchronization of the given db
  ARGUMENTS: (
    !$container <yquerjObject>,
    !disableOrEnable <"disable"|"enable">,
    !syncProcess,
  )
  RETURN: <void>
*/
function displayToggleSyncExclusionButton ($container, disableOrEnable, syncProcess) {

  $container.br();
  uify.button({
    $container: $container,
    icomoon: "angellist",
    inlineTitle: "right",
    invertColor: true,
    title: disableOrEnable +" the synchronization to this remote on this device",
    css: {
      display: "inline-flex",
      marginTop: "1em",
    },
    click: function () {
      var excludedRemotes = $$.storage.local("tablifyExcludedRemotes") || [];

      // set db to be excluded from sync (and stop current synchronization)
      if (disableOrEnable === "disable") {
        excludedRemotes.push(syncProcess.remoteConfig.url);
        $$.storage.local("tablifyExcludedRemotes", excludedRemotes);
        syncProcess.cancelPouchSync();
        syncProcess.changeStatus("excluded");
      }

      // unset db to be excluded from sync (and start synchronization)
      else {
        $$.storage.local("tablifyExcludedRemotes", _.without(excludedRemotes, syncProcess.remoteConfig.url));
        syncProcess.resetPouchSync();
      };

    },
  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DISPLAY SYNC STATUS INFOS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function refreshSyncStatusInfos (App, syncProcess) {

  if (!syncProcess.$infos) return;
  else var $infos = syncProcess.$infos;

  // empty container in case of refresh
  $infos.empty();

  //
  //                              TITLE

  try {
    var decomposedUrl = $$.url.decompose(syncProcess.pwdUrl);
    if (decomposedUrl.password) decomposedUrl.password = "***";
    var urlToDisplay = $$.url.make(decomposedUrl);
  }
  catch (e) {
    var urlToDisplay = "invalid url: "+ syncProcess.pwdUrl;
  };
  $infos.div({
    class: "status_url",
    htmlSanitized: urlToDisplay,
  });
  $infos.div({
    class: "status_name",
    htmlSanitized: "Sync status: <b>"+ syncProcess.status +"</b>",
  });

  //
  //                              ERROR

  if (syncProcess.error) {

    // make reason text
    if (syncProcess.error.reason == "You are not a server admin.") var reasonText = "The remote database you chose to sync with, probably doesn't exist on the server.\nCreate it, or check that you entered the right url."
    else var reasonText = syncProcess.error.reason;

    // create short error message
    $infos.div({
      class: "status_comment message_error",
      text: reasonText,
    });

    $infos.br();
    uify.button({
      $container: $infos,
      icomoon: "key",
      title: "unlock this remote manually",
      inlineTitle: "right",
      invertColor: true,
      css: {
        display: "inline-flex",
        marginTop: "1em",
      },
      click: function () {
        App.proposeManualReloginToRemote(syncProcess);
      },
    });

    // create more button
    $infos.a({
      class: "more_button",
      text: " more",
    }).click(function (event) {
      event.stopPropagation()
      $(this).toggleClass("mode_open");
      $more.toggle();
    });
    var $more = $infos.pre({ class: "more_text", text: $$.json.pretty(syncProcess.error), }).toggle();

  }

  //
  //                              PAUSED

  else if (syncProcess.status == "paused" && isPausedError(syncProcess)) {

    // error message
    $infos.div({
      class: "status_comment message_error",
      htmlSanitized: "Cannot access remote database.<br>"+ [
        "Some possible reasons why this happened:",
        "- you're not connected to the internet,",
        "- the remote databases server is out of service or not connected to the internet,",
        "- CORS haven't been properly enabled on the databases server"
      ].join("<br>&nbsp&nbsp&nbsp&nbsp"),
    });

    // display button allowing to suspend this remote's synchronization
    displayToggleSyncExclusionButton($infos, "disable", syncProcess);

  }

  //
  //                              ACTIVE

  else if (syncProcess.status == "active" || syncProcess.status == "init") $infos.div({
    class: "status_comment message_info",
    htmlSanitized: syncProcess.status == "active" ? "Synchronization is ongoing..." : "Synchronization is currently being setup...",
  })

  //
  //                              EXCLUDED

  else if (syncProcess.status == "excluded") {
    // display info message
    $infos.div({
      class: "status_comment message_info",
      htmlSanitized: "Synchronization for this remote has been disabled",
    })
    // display button allowing to unsuspend this remote's synchronization
    displayToggleSyncExclusionButton($infos, "enable", syncProcess);
  }

  //
  //                              ALL GOOD

  else $infos.div({
    class: "status_comment message_ok",
    htmlSanitized: "Everything seem to be running right.<br><i>If status is 'paused', understand it as 'on pause, nothing to do because the synchronization is done and up to date'.</i>",
  });

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  PAUSED STATUS BUT STILL AN ERROR
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function isPausedError (syncProcess) {
  if (syncProcess.pouchSyncProcess) return syncProcess.pouchSyncProcess.push.state == "stopped" && syncProcess.pouchSyncProcess.pull.state == "stopped"
  else return true;
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SYNC STATUS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  syncStatus: function () {
    var App = this;
    // no sync requested
    if (!App.syncProcesses || !App.syncProcesses.length) return "disabled"
    // any process is active
    else if (_.some(App.syncProcesses, function (process) {
      return process.status == "active";
    })) return "active"
    // any process has an error
    else if (_.some(App.syncProcesses, function (process) {
      return process.status == "error" || process.status == "denied";
    })) return "error"
    // any process is broken (no connection or CORS problem) or any process is canceled
    else if (_.some(App.syncProcesses, function (process) {
      return (process.status !== "excluded" && isPausedError(process)) || process.status == "canceled";
    })) return "warning"
    // all good
    else return "done";
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET/SET SYNC SETTINGS FROM/TO STORAGE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get or set sync settings
    ARGUMENTS: (
      ?val <url[]> «
        pass a value here if you want to set value to storage,
        to get just pass nothing here
      »,
    )
    RETURN: <undefined|url[]>
  */
  syncSettings: function (val) {
    var App = this;
    var user = page && page.authenticatedUser ? page.authenticatedUser : "visitor";
    var settingsValue = $$.storage.local("tablify_sync_settings-"+ App.config.name +"-"+ user, val);
    if (!_.isUndefined(val)) {
      App.syncSetup();
      uify.toast.success("Modified synchronization configuration.");
    };
    return settingsValue;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SETUP SYNC(s) oN THIS POUCH
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: setup pouch to sync with remote databases and run changes when finished
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  syncSetup: function () {
    var App = this;

    // setup synchronization with remote databases
    App.setupSyncWithRemotes();

    // watch changes in this database
    App.watchChanges();

    // initialize verification that still offline on interval
    App.syncIntervalCheck = setInterval(function () {
      updateStatusClasses(App);
      if (App.syncInfosDialogIsVisible) _.each(App.syncProcesses, function (syncProcess) { syncProcess.refreshSyncStatusInfos(); });
    }, 2000);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  STOP ALL SYNCHRONIZATIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: interrupt all ongoing synchronization processes
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  syncCancel: function () {
    var App = this;

    _.each(App.syncProcesses, function (syncProcess) {
      syncProcess.cancelPouchSync();
    });

  },

  /**
    DESCRIPTION:
      interrupt all ongoing synchronization processes, forget passwords, and try to restart them
      this basically reloads the UI so you can choose to login again into synchronization
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  syncLogout: function () {
    var App = this;

    _.each(App.syncProcesses, function (syncProcess) {
      syncProcess.cancelPouchSync();
      var decomposedUrl = $$.url.decompose(syncProcess.pwdUrl);
      delete decomposedUrl.username;
      delete decomposedUrl.password;
      syncProcess.pwdUrl = $$.url.make(decomposedUrl);
      syncProcess.resetPouchSync();
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SETUP ALL REMOTE SYNCHRONIZATIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  setupSyncWithRemotes: function () {
    var App = this;

    // get list of all remotes to synchronize with
    var remotes = [];
    if (App.config.remotes) remotes.push($$.result.call(App, App.config.remotes));
    if (App.config.synchronizationTools === true) remotes.push(App.syncSettings());
    remotes = _.compact(_.flatten(remotes));
    // process remotes if asked
    if (_.isFunction(App.config.processRemotes)) remotes = App.config.processRemotes(remotes);
    // tag eventual excluded remotes for this device
    var excludedRemotes = $$.storage.local("tablifyExcludedRemotes");
    if (_.isArray(excludedRemotes)) _.each(remotes, function (remoteObject) {
      if (_.indexOf(excludedRemotes, remoteObject.url) !== -1) remoteObject.excluded = true
      else remoteObject.excluded = false;
    });

    // remove eventual previous sync processes
    if (App.syncProcesses) {
      _.each(App.syncProcesses, function (syncProcess) { if (syncProcess.pouchSyncProcess) syncProcess.pouchSyncProcess.cancel(); });
      delete App.syncProcesses;
    };
    if (App.syncIntervalCheck) clearInterval(App.syncIntervalCheck);

    // initialize live sync with databases
    App.syncProcesses = _.map(remotes, function (remote) {
      return App.syncWithOneRemote(remote);
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INDIVIDUAL REMOTE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: create syncProcess for the given remote, will check remote db, check rights on remote, ask password if necessary...
    ARGUMENTS: (
      remote <{
        !url: <string>,
        ?username: <string>,
        ?password: <string>,
        ?filter: <syncProcessType.filter>,
      }>
    )
    RETURN: syncProcessType <{
      !id: <string> « unique identifier for this process »,
      !url: <string>,
      !status: <string>,
      ?error: <{ message: <string>, ... }>,
      ?filter: <{ [keyToFilterBy]: <string> « value to filter by » }>,
      !changeStatus: <function(
        !newStatus <syncProcessType·status>,
        !err <syncProcessType·error>,
      )> « use this to change the status of this synchronization process »
      !resetPouchSyncWithNewCredentials: <function(!newCredentials <{ username: <string>, password: <string>, }>),
    }>
  */
  syncWithOneRemote: function (remote) {
    var App = this;

    // make full url with username and password
    if (remote.username) {
      var decomposedRemoteUrl = $$.url.decompose(remote.url);
      if (!decomposedRemoteUrl) var pwdUrl = "";
      else {
        decomposedRemoteUrl.username = remote.username;
        decomposedRemoteUrl.password = remote.password;
        var pwdUrl = $$.url.make(decomposedRemoteUrl);
      };
    }
    else var pwdUrl = remote.url;

    // make syncProcess object
    var syncProcess = {
      id: $$.uuid(),
      remoteConfig: remote, // this is not used inside tablify but is necessary for dato
      pwdUrl: pwdUrl,
      status: remote.excluded ? "excluded" : "init",
      filter: remote.filter,
      changeStatus: function (newStatus, err) {
        syncProcess.status = newStatus;
        syncProcess.error = err;
        updateStatusClasses(App);
        syncProcess.refreshSyncStatusInfos();
      },
      refreshSyncStatusInfos: function () { return refreshSyncStatusInfos(App, syncProcess); },
      cancelPouchSync: function () {
        // cancel sync process
        if (syncProcess.pouchSyncProcess) syncProcess.pouchSyncProcess.cancel();
        // setup canceled status
        syncProcess.changeStatus("canceled");
      },
      resetPouchSync: function () {
        // cancel previous sync process
        if (syncProcess.pouchSyncProcess) syncProcess.pouchSyncProcess.cancel();
        // setup initializing status again
        syncProcess.changeStatus("init");
        // reset up pouch sync
        App.setupPouchSync(syncProcess);
      },
      resetPouchSyncWithNewCredentials: function (newCredentials) {

        // decompose url
        var decomposedUrl = $$.url.decompose(syncProcess.pwdUrl);

        // cancel previous sync process
        if (syncProcess.pouchSyncProcess) syncProcess.pouchSyncProcess.cancel();

        // remake url with new credentials
        if (!(decomposedUrl.hostname == "localhost" || decomposedUrl.hostname == "127.0.0.1")) decomposedUrl.protocol = "https://"; // make sure it's https otherwise added login infos could be leaked
        decomposedUrl.username = newCredentials.username;
        decomposedUrl.password = newCredentials.password;
        syncProcess.pwdUrl = $$.url.make(decomposedUrl);

        // setup initializing status again
        syncProcess.changeStatus("init");

        // retry setting up the db with newly given credentials
        App.setupPouchSync(syncProcess);

      },
    };

    // if no valid url, stop now and make syncProcess.status an error
    if (!pwdUrl) {
      $$.log.detailedError(
        "tablify/synchronization:syncSetup",
        "Could not setup synchronization, could not figure out url with which to synchronize.",
        "Remote object passed: ", remote
      );
      syncProcess.changeStatus("error", {
        message: "Could not figure out the url of the remote!",
        remoteObjectPassed: remote,
      });
      return syncProcess;
    };

    // setup pouch synchronization for this remote (if not excluded from syncrhonization)
    if (!remote.excluded) App.setupPouchSync(syncProcess);

    return syncProcess;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SETUP POUCH SYNC
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: setup pouch synchronization of this database with the given remote url
    ARGUMENTS: ( syncProcess <syncProcessType> )
    RETURN: <void>
  */
  setupPouchSync: function (syncProcess) {
    var App = this;

    // figure out synchronization options
    var syncOptions = {
      live: true,
      retry: true,
      batch_size: 5,    // less big batch of documents to minimize changes for synchronizations to fail
      batches_limit: 5, // less big batch of documents to minimize changes for synchronizations to fail
    };
    if (_.isFunction(syncProcess.filter)) syncOptions.filter = syncProcess.filter
    else syncOptions.filter = function (entry) {
      return _.findWhere([entry], syncProcess.filter);
    };

    syncProcess.pouchSyncProcess = App.Collection.pouch.sync(syncProcess.pwdUrl, syncOptions)
      // replicate resumed (e.g. new changes replicating, user went back online)
      .on("active", function (yoo) {
        syncProcess.changeStatus("active");
      })
      // handle change
      .on("change", function (info) {
        // $$.log.info("Tablify '"+ App.config.name +"' sync change:", info);
      })
      // replication paused (e.g. replication up to date, user went offline)
      .on("paused", function (err) { syncProcess.changeStatus("paused", err); })
      // a document failed to replicate (e.g. due to permissions)
      .on("denied", function (err) { syncProcess.changeStatus("denied", err); })
      // handle error
      .on("error", function (err) {

        // unauthorized, propose to change credential
        if (err.error == "unauthorized") {

          var decomposedUrl = $$.url.decompose(syncProcess.pwdUrl);
          // try to find credentials for this process in another process (if not already tried)
          if (!syncProcess.possibleCredentialsList) syncProcess.possibleCredentialsList = App.getCredentialsForThisCouchServerInOtherSyncProcessOrInSession(syncProcess.id, decomposedUrl);
          // found credentials for this process in other sync process
          if (syncProcess.possibleCredentialsList.length) {
            syncProcess.resetPouchSyncWithNewCredentials(syncProcess.possibleCredentialsList.pop());
            return;
          };

        };

        // other error
        syncProcess.changeStatus("error", err);

      })
      // handle complete (probably only useful when sync is not live) // NOTE: seems better to disable it, because disabling a sync may turn status into complete after a while
      // .on("complete", function (info) { syncProcess.changeStatus("complete"); })
    ;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GIVE OPPORTUNITY TO ENTER REMOTE DB USERNAME AND PASSWORD MANUALLY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  proposeManualReloginToRemote: function (syncProcess) {
    var App = this;

    // ask credentials to user
    App.loginToRemoteDatabase(syncProcess, function (credentials) {

      // reset pouch sync with new credentials
      syncProcess.resetPouchSyncWithNewCredentials(credentials);

      // automatically try these credentials as well on other sync processes that failed
      _.each(App.syncProcesses, function (syncProcess) {
        if (syncProcess && syncProcess.status === "error") syncProcess.resetPouchSyncWithNewCredentials(credentials);
      });

    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CHECK IF OTHER PROCESS HAVE A PASSWORD FOR A DB
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get the part of url that corresponds to the couch server
    ARGUMENTS: ( !urlOrDecomposedUrl <string|$$.url.decompose·return> « db url to extract couch server id from », )
    RETURN: <string>
  */
  getCouchServerId: function (decomposedUrl) {
    if (_.isString(decomposedUrl)) decomposedUrl = $$.url.decompose(decomposedUrl);
    return decomposedUrl.domain + (decomposedUrl.path + "").replace(/[^\/]+\/?$/, "").replace(/\/$/, "");  // NOTE: same used in dato.pages/index/dato/index.js:createTablify, if modify this, that one should be modified as well
  },

  getCredentialsForThisCouchServerInOtherSyncProcessOrInSession: function (syncProcessId, decomposedUrl) {
    var App = this;

    var couchServerId = App.getCouchServerId(decomposedUrl);
    var credentialsList = [];

    //
    //                              FIND CREDENTIALS FROM SESSION STORAGE

    var tablifyStoredCredentials = Object.assign({}, $$.storage.session("tablifyCredentials"), $$.storage.local("tablifyCredentials"), App.config.synchronizationCredentialsList);

    var decomposedUrlClone = _.clone(decomposedUrl);
    if (decomposedUrlClone.username) delete decomposedUrlClone.username;
    if (decomposedUrlClone.password) delete decomposedUrlClone.password;
    var urlWithoutCredentials = $$.url.make(decomposedUrl);
    // credentials set in sessionStorage for exactly this database
    if (tablifyStoredCredentials[urlWithoutCredentials]) credentialsList.push(tablifyStoredCredentials[urlWithoutCredentials]);
    // credentials set in sessionStorage for a database on same couch server
    if (tablifyStoredCredentials[couchServerId]) credentialsList.push(tablifyStoredCredentials[couchServerId]);

    //
    //                              FIND CREDENTIALS FROM OTHER PROCESSES WITH SAME COUCH SERVER

    _.each(App.syncProcesses, function (syncProcess) {

      if (syncProcess.id == syncProcessId) return;

      var processDecomposedUrl = $$.url.decompose(syncProcess.pwdUrl);

      // if same couch server, and has username and password, add credentials to list of credentials found
      if (
        couchServerId == App.getCouchServerId(processDecomposedUrl) &&
        processDecomposedUrl.username && processDecomposedUrl.password
      ) credentialsList.push({
        username: processDecomposedUrl.username,
        password: processDecomposedUrl.password,
      });

    });

    //
    //                              RETURN ALL FOUND CREDENTIALS, DEDUPLICATED AND WITHOUT THE ACTUAL ONES

    // keep non duplicate credentials pair, that are not same as present ones
    return _.chain(credentialsList).uniq(function (a, b){
      return _.isEqual(a, b);
    }).reject(function (credentials) {
      return _.isEqual(credentials, { username: decomposedUrl.username, password: decomposedUrl.password, });
    }).value();

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  WATCH CHANGES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  watchChanges: function () {
    var App = this;

    // initialize watching changes
    App.Collection.pouch.changes({
      since: "now",
      live: true,
      include_docs: true,
    }).on("change", function (change) {

      // remove caches
      // if folder is saved, remove the cache of it's display settings
      if (App.cache.currentDisplays && App.cache.currentDisplays[change.id]) delete App.cache.currentDisplays[change.id];

      // refresh display
      App.debouncedRefreshCollection();

      // remove any deleted entry from selection
      if (change.deleted) {
        App.selected = _.reject(App.selected, function (doc) { return change.id === doc.id; });
        _.each(App.selectedCache, function (selectedList, folderId) {
          App.selectedCache[folderId] = _.reject(selectedList, function (doc) { return change.id === doc.id; })
        });
      };

      // run optional change callbacks (general "changeCallback" defined in tablify config, entry type specifically defined "changed")
      if (App.config.changeCallback) App.config.changeCallback(change.doc, change.deleted);

      // run any change event relative to this entry type
      var customChangedFunction = $$.getValue(App.getTypeConfig(change.doc.type), "event.changed");
      if (_.isFunction(customChangedFunction)) customChangedFunction.call(App, change.doc, change.deleted);

    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EDIT SYNC SETTINGS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: display dialog to edit synchronization parameters
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  editSyncSettings: function () {
    var App = this;

    var commentUrl = App.config.url ? "<br>The following sync url has already been set in the system: "+ App.config.url : "";

    dialogify.input({
      title: "Synchronization parameters",
      comment: "You can add here one or more databases urls.<br>This will allow the entries you entered to be synchronized with those remote databases."+ commentUrl,
      input: {
        type: "array",
        value: App.syncSettings(),
        prepareValue: [ undefined, ],
        object: {
          model: {
            type: "entry",
            labelLayout: "hidden",
            object: {
              structure: [
                {
                  name: "url",
                  type: "url",
                  labelLayout: "stacked",
                  help: "Url to an existing remote couch database (for example: 'https://mycouch.tld/mydb/').",
                },
                {
                  name: "username",
                  type: "normal",
                  help: "If this remote database is not public, you should fill here the name of a user on this couch server that has access to this database.",
                  labelLayout: "stacked",
                  input: { autocomplete: "new-password", },
                },
                {
                  name: "password",
                  type: "password",
                  help: "If this remote database is not public, fill here the password for the chosen user.",
                  labelLayout: "stacked",
                  input: { autocomplete: "new-password", },
                },
                {
                  name: "filter",
                  type: "object",
                  labelLayout: "stacked",
                },
              ],
            },
          },
        },
      },
      ok: function (result) {
        if (_.isUndefined(result)) App.syncSettings([])
        else if (_.isArray(result)) App.syncSettings(result);
      },
      cancel: function (result) {
        if (_.isEqual(result, App.syncSettings())) return true
        else return confirm("Close window and lose the modifications you made?");
      },
    });


  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  PROPOSE TO LOGIN TO REMOTE DB
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: propose to login to a database
    ARGUMENTS: (
      !syncProcess,
      !callback <function(<{
        ?username: <string>,
        ?password: <string>,
      }>)>,
    )
    RETURN: <void>
  */
  loginToRemoteDatabase: function (syncProcess, callback) {
    var App = this;

    var decomposedUrl = $$.url.decompose(syncProcess.pwdUrl);
    if (decomposedUrl.password) decomposedUrl.password = "***";
    var urlToDisplay = $$.url.make(decomposedUrl);

    // show remote login dialog
    dialogify.object({
      title: "Synchronization error",
      comment: [
        "The app is trying to synchronize this database with a remote one, however it doesn't seem that you have access to that remote:",
        urlToDisplay,
        "",
        decomposedUrl.username ? '<big><b class="error-text">Did you make a mistake in your username or password for this database?</b></big>' : "<big><b>You should login to synchronize with this private database.</b></big>",
      ].join("<br>"),
      cssDialog: { textAlign: "center", },
      structure: [
        {
          name: "username",
          type: "normal",
          labelLayout: "stacked",
          value: decomposedUrl.username,
          input: { autocomplete: "new-password", },
        },
        {
          name: "password",
          type: "password",
          labelLayout: "stacked",
          input: { autocomplete: "new-password", },
        },
        {
          name: "keepInStorage",
          type: "radio",
          choices: [
            "no",
            { _isChoice: true, value: "session", label: "until closing browser",      },
            { _isChoice: true, value: "local",   label: "until manually logging out", },
          ],
          label: "Remember these credentials",
          defaultValue: "session",
          changed: function () { this.doOnSiblings("remake", "userForOtherDatabasesOnSameCouchServer"); },
        },
        {
          name: "userForOtherDatabasesOnSameCouchServer",
          type: "boolean",
          defaultValue: true,
          condition: function () { return this.getSiblingValue("keepInStorage") === "session" || this.getSiblingValue("keepInStorage") === "local"; },
          label: "Use these credentials to login to other databases in same couch server",
        },
      ],
      ok: function (credentials) {
        if (credentials.keepInStorage === "session" || credentials.keepInStorage === "local") {
          var tablifyCredentials = $$.storage[credentials.keepInStorage]("tablifyCredentials") || {};
          if (decomposedUrl.username) delete decomposedUrl.username;
          if (decomposedUrl.password) delete decomposedUrl.password;
          tablifyCredentials[$$.url.make(decomposedUrl)] = credentials;
          if (credentials.userForOtherDatabasesOnSameCouchServer) tablifyCredentials[App.getCouchServerId(syncProcess.pwdUrl)] = credentials;
          $$.storage[credentials.keepInStorage]("tablifyCredentials", tablifyCredentials);
        };
        callback(credentials);
      },
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INFOS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  syncInfosDialogIsVisible: false,

  /**
    DESCRIPTION: alert sync status, infos
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  syncInfos: function () {
    var App = this;

    uify.alert({
      title: "Synchronization status",
      cssContent: { fontSize: "1em", },
      content: function ($container) {

        App.syncInfosDialogIsVisible = true;
        $container.addClass("sync_status");

        if (App.syncProcesses && App.syncProcesses.length) _.each(App.syncProcesses, function (syncProcess) {
          if (!syncProcess) syncProcess = {};
          syncProcess.$infos = $container.div({ class: "status_infos", });
          syncProcess.refreshSyncStatusInfos();
        })
        else $container.div({
          class: "status_disabled",
          text: 'The database haven\'t been set to synchronize with any remote database.',
        });

      },
      // remove all $infos elements so that refreshSyncStatusInfos doesn't do useless work on non visible dom elements
      ok: cleanSyncProcess$infosElements,
      cancel: cleanSyncProcess$infosElements,
    });

    function cleanSyncProcess$infosElements () {
      App.syncInfosDialogIsVisible = false;
      _.each(App.syncProcesses, function (syncProcess) {
        syncProcess.$infos.remove();
        delete syncProcess.$infos;
      });
    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
