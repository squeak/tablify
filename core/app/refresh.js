var _ = require("underscore");
var $$ = require("squeak");
var $ = require("yquerj");
var layouts = require("../layouts");
var confer = require("../config");
var uify = require("uify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE CUSTOM CSS RULES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makeCustomCssRules (appTablifyId, rulesList) {

  var resultString = "";

  _.each(rulesList, function (ruleObject) {

    var prepend = ".tablify--"+ appTablifyId;

    // figure out text to prepend
    if (ruleObject.selector.match(/^\&/)) var selector = ruleObject.selector.replace(/^\&/, "")
    else {
      var selector = ruleObject.selector;
      prepend += " ";
    };

    // create full rule (optionally wrapped in media query)
    if (ruleObject.mediaQuery) resultString += ruleObject.mediaQuery +" {\n";
    resultString += prepend +" "+ selector +"{"+ ruleObject.rule +"}\n";
    if (ruleObject.mediaQuery) resultString += "}\n";

  });

  return resultString;

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //
  //                              REFRESH WITH NEW CONFIG

  /**
    DESCRIPTION: modify some config elements and refresh app
    ARGUMENTS: ( !partialConfig <tablifyOptions> « only the parts present in here will be replaced in the current config » )
    RETURN: <void>
  */
  tweakConfigAndRefresh: function (partialConfig) {
    var App = this;
    // needs to modify current config object, otherwise it will not work
    _.each(partialConfig, function (value, key) {
      if ($$.isObjectLiteral(App.config[key])) _.each(value, function (subvalue, subkey) { App.config[key][subkey] = subvalue; })
      else App.config[key] = value;
    });
    // verify config (add mandatory things, correct invalid values...)
    confer.verifyConfig(App.config);
    // refresh app
    App.refresh();
  },

  //
  //                              FETCH REFETCH COLLECTION

  refreshCollection: function (callback) {
    var App = this;

    App.Collection.fetch({
      // silent: true,
      reset: true, // if not true, when refetching, will have error and not reset for example a deleted key, and will keep previously set value
      success: function (collection, response, options) {
        if (App.databaseHasBeenModified()) {
          // $$.log.info("Refreshing '"+ App.config.name +"' tablify display.");
          App.refresh(callback);
        }
        else $$.log.info("Will not refresh display of '"+ App.config.name +"' because it hasn't changed.");
      },
      error: function (collection, response, options) {
        // TODO TODO TODO REALLY RENDER WHEN FAIL??
        App.refresh();
        $$.log.detailedError(
          "tablify/appView",
          "Error fetching database "+ App.config.name,
          response
        );
      },
    });

  },

  //
  //                              REFRESH ROWS, HEADER AND FOOTER

  /**
    DESCRIPTION: refresh the app
    ARGUMENTS: ( callback <function(ø){@this=App}> )
    RETURN: <void>
  */
  refresh: function (callback) {
    var App = this;

    // change display settings from folder if necessary and then refresh
    if (App.config.pathsDatabase) App.refreshDisplayOptions(doRefresh)
    else doRefresh();

    // refreshing actions
    function doRefresh () {

      // refresh display
      App.refreshAppClasses();
      App.makeHeader();
      App.refreshBody(function () {

        // make footer
        App.makeFooter();

        // make sure header is right width
        if (App.$body[0].scrollHeight > App.$body[0].clientHeight) App.$header.addClass("tbody_has_scroll")
        else App.$header.removeClass("tbody_has_scroll");

        // make sure that selected entries are shown as selected
        App.select(_.pluck(App.selected, "id")); // NOTE: here pass ids and not models, because on refresh models may be renewed, and not be the same as before, so existing models may have disappeared

        // refresh graph panel if it is visible
        if (App.graphified) {
          if (App.graphified.isVisible) App.graphified.refresh()
          else App.graphified.toRefresh = true;
        };

        // refrehs display of any viewed entry
        App.refreshViewedEntries();

        // custom callback
        if (callback) callback.call(App);

      });

    };

  },

  //
  //                              REFRESH DISPLAY OPTIONS FROM FOLDER

  /**
    DESCRIPTION:
      check if the display options should be modified: path has changed and new path has different display options than the previous one
      ask to refresh display parameters if necessary
    ARGUMENTS: ( ?callback <function(ø)> « exectued when done » )
    RETURN: <void>
  */
  refreshDisplayOptions: function (callback) {
    var App = this;

    // get id of new path
    App.currentPathId = App.Collection.getPathIdFromPath(App.currentPath);

    if (!App.currentPathId) {
      App.currentPath = "/";
      App.currentPathId = "_/";
    };

    // reset to default db display
    if (App.currentPathId == "_/" || App.currentPathId == "_UNREACHABLE_" || App.currentPathId == "_ALL_") var newDisplay = App.config.display

    // set display from db defaults + this folder options
    else {
      var currentFolderModel = App.Collection.get(App.currentPathId);
      var newDisplay = currentFolderModel ? App.getFolderDisplayConfig(currentFolderModel) : App.config.display;
    };

    // make sure current folder has attachments and in any case check if display should be refreshed
    if (currentFolderModel) currentFolderModel.fetchWithAttachments(refreshDisplay)
    else refreshDisplay();

    function refreshDisplay () {
      // refresh display (only if the new one is different from the previous one)
      if (!_.isEqual(App.currentDisplay, newDisplay)) {
        App.currentDisplay = newDisplay;
        App.manualSortingIsEnabled = false; // reset manual sorting state
        App.refreshAfterDisplayOptionsChanged();
      };
      if (callback) callback();
    };

  },

  //
  //                              GET DISPLAY CONFIG FOR A FOLDER

  /**
    DESCRIPTION: get the display config for the given folder (complemented with global display config)
    ARGUMENTS: ( !folderModel <backboneModel> « model of the folder you want to make display config for » )
    RETURN: <tablify·options.display>
  */
  getFolderDisplayConfig: function (folderModel) {
    var App = this;

    // cache display (so that UI remembers which column the user had selected, filters...)
    if (!App.cache.currentDisplays) App.cache.currentDisplays = {};

    // if not yet cached
    if (!App.cache.currentDisplays[App.currentPathId]) {
      // get raw display for this folder
      var folderDiplayObject = App.config.folderDisplayProcess ? App.config.folderDisplayProcess(folderModel.get("display")) : folderModel.get("display");
      // make sure currentLayout is defined if layouts is
      if (folderDiplayObject && folderDiplayObject.layouts) folderDiplayObject.currentLayout = App.getCurrentLayout(folderDiplayObject);
      // create display and cache it
      App.cache.currentDisplays[App.currentPathId] = $$.defaults(App.config.display, folderDiplayObject);
    };

    return App.cache.currentDisplays[App.currentPathId];

  },

  //
  //                              REFRESH FROM CHANGES IN DISPLAY OPTIONS

  refreshAfterDisplayOptionsChanged: function () {
    var App = this;

    // refresh currenSelectedColumn
    App.currentDisplay.currentSelectedColumn = $$.getValue(App.currentDisplay, "columns[0].key");

    App.refreshCustomCssRules();
    var layoutToolbarButton = App.toolbar.get("displayLayoutChoose");
    if (layoutToolbarButton) layoutToolbarButton.refresh();

  },

  //
  //                              CUSTOM CSS RULES

  refreshCustomCssRules: function () {
    var App = this;

    // apply new custom styles
    var finalCssRules = "";

    // add custom css rules for this display
    if (App.currentDisplay.cssRules) finalCssRules += makeCustomCssRules(App.tablifyId, App.currentDisplay.cssRules);

    // add custom css rules of each column
    _.each(App.currentDisplay.columns, function (columnConfig) {
      if (!columnConfig.uuidClass) columnConfig.uuidClass = "tablify-column--"+ $$.uuid();

      if (columnConfig.cssProperties) {

        var customCssRules = [{
          selector: "."+ columnConfig.uuidClass,
          rule: columnConfig.cssProperties,
        }];

        if (customCssRules.length) finalCssRules += makeCustomCssRules(App.tablifyId, customCssRules);

      };

    });

    // remove any eventual previously applied custom styles
    $(".tablify_styling--"+ App.tablifyId).remove();

    // append rules to document
    var style = document.createElement("style");
    style.classList = ["tablify_styling--"+ App.tablifyId];
    style.innerHTML = finalCssRules;
    document.head.appendChild(style);

  },

  //
  //                              REFRESH APP CONTAINER CLASSES

  refreshAppClasses: function () {
    var App = this;
    _.each(layouts, function (yoloPlentyShit, className) {
      App.$el.removeClass("tablify-"+ className);
    });
    App.$el.addClass("tablify-"+ App.currentDisplay.currentLayout);
  },

  //
  //                              REFRESH BODY

  refreshBody: function (callback) {
    var App = this;

    App.displaySpinner();

    setTimeout(function () {
      App.makeBody();
      App.destroySpinner();
      if (App.config.renderCallback) App.config.renderCallback(App);
      if (callback) callback(App);
    }, 15);

  },

  //
  //                              SPINNER

  /**
    DESCRIPTION: create refresh spinner
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  displaySpinner: function () {
    if (this.spinner) this.destroySpinner();
    this.spinner = uify.spinner({ $container: this.$el, });
  },

  /**
    DESCRIPTION: destroy refresh spinner
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  destroySpinner: function () {
    if (this.spinner) {
      this.spinner.destroy();
      delete this.spinner;
    };
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MODIFICATIONS CHECKER
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  makeDatabaseModificationComparer: function () {
    var App = this;
    return App.Collection.map(function(model){ return model.get("_id") +"|"+ model.get("_rev"); })
  },

  /**
    DESCRIPTION: check if the database has been modified since last execution
    ARGUMENTS: ( ø )
    RETURN: <boolean>
  */
  databaseHasBeenModified: function () {
    var App = this;
    if (!App.databaseModificationLastState) {
      App.databaseModificationLastState = App.makeDatabaseModificationComparer();
      return true;
    }
    else {
      var newState = App.makeDatabaseModificationComparer();
      if (_.isEqual(App.databaseModificationLastState, newState)) return false
      else {
        App.databaseModificationLastState = newState;
        return true;
      };
    };
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  OPEN THE VIEW/EDITION WINDOW FOR ENTRIES LISTED IN HASH
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: figure out from document hash, which entries to open in view and edit window
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  openEntriesSpecifiedInHash: function () {
    var App = this;

    var hashObject = $$.url.hash.getAsObject();

    // open entries under edition
    var entriesToEdit = hashObject.e;
    App.editEntriesFromHash(entriesToEdit, true);

    // open viewed entries
    var entriesToView = hashObject.v;
    App.viewEntriesFromHash(entriesToView, true);

  },

  /**
    DESCRIPTION: open in edit window the specified list of entries
    ARGUMENTS: (
      entriesToView <<string|object>[]> « ids or partial objects of the entries to open »,
      updateHash <boolean> « if true, will also update document hash from the list of entries id passed »,
    )
    RETURN: <void>
  */
  editEntriesFromHash: function (entriesToEdit, updateHash) {
    var App = this;

    if (entriesToEdit && entriesToEdit.length) {

      // OPEN ENTRIES
      // find existing and absent entries
      var entriesToEditModels = App.findExistingModelsFromIdsOrPartialObjects(entriesToEdit);
      // open existing entries
      _.each(entriesToEditModels.present, function (model) { App.openEntryEditor(model); });
      // propose to create new entry for each object absent in the db
      _.each(entriesToEditModels.absent, function (newEntryObject) {
        if (_.isObject(newEntryObject)) App.newEntry(newEntryObject);
      });

      // UPDATE HASH
      if (updateHash) {
        var hashObject = $$.url.hash.getAsObject();
        // resave hash to make sure ids of entries are stored in hash (not some kind of matcher)
        hashObject.e = _.pluck(entriesToEditModels.present, "id");
        $$.url.hash.set(hashObject);
      };

    };

  },

  /**
    DESCRIPTION: open in view window the specified list of entries
    ARGUMENTS: (
      entriesToView <<string|object>[]> « ids or partial objects of the entries to open »,
      updateHash <boolean> « if true, will also update document hash from the list of entries id passed »,
    )
    RETURN: <void>
  */
  viewEntriesFromHash: function (entriesToView, updateHash) {
    var App = this;

    if (entriesToView && entriesToView.length) {

      // OPEN ENTRIES
      // find existing and absent entries
      var entriesToViewModels = App.findExistingModelsFromIdsOrPartialObjects(entriesToView);
      // open existing entries
      _.each(entriesToViewModels.present, function (model) { App.openEntry(model); });
      // log error some entries could not be opened because they are absent from db
      if (entriesToViewModels.absent && entriesToViewModels.absent.length) $$.log.error("The following list of entries could not be displayed because they seem absent from the database: ", entriesToViewModels.absent);

      // UPDATE HASH
      if (updateHash) {
        var hashObject = $$.url.hash.getAsObject();
        // resave hash to make sure ids of entries are stored in hash (not some kind of matcher)
        hashObject.v = _.pluck(entriesToViewModels.present, "id");
        $$.url.hash.set(hashObject);
      };

    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REFRESH VIEWED ENTRIES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: refresh view windows currently open
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  refreshViewedEntries: function () {
    var App = this;
    _.each(App.currentlyViewifiedEntries, function (viewified, modelId) {
      var model = App.Collection.get(modelId);
      if (!model) return $$.log.error("Could not properly refresh window. Failed to find in collection, the model with the following model id:", modelId);
      model.fetchWithAttachments(function () {
        viewified.refresh(model.attributes);
      });
    });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  TOGGLE DISPLAY OF HIDDEN ENTRIES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: toggle display of hidden entries
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  toggleDisplayOfHiddenEntries: function () {
    var App = this;

    // get list of hidden elements
    var $hiddenElements = App.$body.find(".tablify-hidden_element");

    // invert display status
    App.hiddenElementsVisible = _.isUndefined(App.hiddenElementsVisible) ? true : !App.hiddenElementsVisible;

    // toggle "hidden" class on hidden element
    if (App.hiddenElementsVisible) $hiddenElements.removeClass("tablify-hidden_element-hidden")
    else $hiddenElements.addClass("tablify-hidden_element-hidden");

    // if showing hidden elements, make nice transition
    if (App.hiddenElementsVisible) {
      $hiddenElements.css("opacity", 0);
      setTimeout(function () { $hiddenElements.css("opacity", 1); }, 100);
    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
