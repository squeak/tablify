var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/dom");
var $ = require("yquerj");
var Sortablejs = require("sortablejs");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: list of last visited entries
    TYPE: <{ [idOfFolderEntry]: <model[]> }>
  */
  selectedCache: {},

  /**
    DESCRIPTION: select the given entry or list of entries
    ARGUMENTS: (
      !modelModelIdOrIds <model|string|model[]|string[]>,
      ?scrollToSelected <boolean> « if true, will scroll to first selected entry »,
      ?addToCurrentSelection <boolean> « if true, will add given entries to selection »,
    )
    RETURN: <void>
  */
  select: function (modelModelIdOrIds, scrollToSelected, addToCurrentSelection) {
    var App = this;

    // remake list of selected models
    if (!addToCurrentSelection) App.selected = App.getEntryOrEntries(modelModelIdOrIds);
    else _.each(App.getEntryOrEntries(modelModelIdOrIds), function (model) {
      var newModelIndexInOldList = App.selected.findIndex(function (mod) { return model.id == mod.id });
      // if model was not in previous list, add it to new list
      if (newModelIndexInOldList == -1) App.selected.push(model)
      // else remove it from new list
      else App.selected.splice(newModelIndexInOldList, 1);
    });

    // make sure list of selected items stay sorted
    App.selected.sort(function (model) {
      return -App.currentDisplayedTable.findIndex(function (mod) { return mod.id == model.id; })
    });

    // update display
    if (App.$selected) App.$selected.removeClass("selected");
    _.each(App.selected, function (model) {
      App.$el.find("#"+ model.id).addClass("selected");
    });
    App.$selected = App.$el.find(".selected");

    // scroll to first selected entry
    if (scrollToSelected) $$.dom.scrollTo(App.$selected[0], App.$el.find("tbody"));

    // run selection callback if there's one
    if (App.config.rowSelect) _.each(App.selected, function (model) {
      App.config.rowSelect.call(App, model);
    });

    // refresh fab
    App.makeFab();
    // refresh footer
    App.makeFooter();

    // refresh entries sortable // NOTE: it's annoying to have to run all this all the time, it's heavy, but didn't find a lighter way so far, otherwise sortable multiDrag is having bugs with tablify selection system
    App.$entries.each(function () { Sortablejs.utils.deselect(this); });
    App.$selected.each(function () { Sortablejs.utils.select(this); });

    // run any eventual callback that should be run on selection
    if (App.config.onEntrySelect) App.config.onEntrySelect.call(App);

  },

  /**
    DESCRIPTION: select next entry
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  selectNext: function () {
    var nextEntry = this.getNextEntry();
    if (nextEntry) this.select(nextEntry, true);
  },

  /**
    DESCRIPTION: select previous entry
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  selectPrevious: function () {
    var previousEntry = this.getPreviousEntry();
    if (previousEntry) this.select(previousEntry, true);
  },

  /**
    DESCRIPTION: add an entry to list of selected entries
    ARGUMENTS: ( !entryId <string> )
    RETURN: <void>
  */
  addToSelection: function (entryId) {
    var App = this;

    // make sure shift doesn't make it select text
    $$.dom.clearSelection();

    // nothing was selected before
    if (!App.selected.length) App.select(entryId)

    // select all in between selected before and now
    else {
      var listOfBoundariesIndexesToSearchIn = [
        App.getIndex(entryId),
        App.getIndex(App.selected[0].id),
        App.getIndex(App.selected[App.selected.length-1].id),
      ];
      var modelsToSelect = _.filter(App.currentDisplayedTable, function (model, indexInDisplayedList) {
        return $$.isBetween(indexInDisplayedList, listOfBoundariesIndexesToSearchIn);
      });
      App.select(modelsToSelect);
    };

  },

  // removeFromSelection: function (entryId) {
  //   var App = this;
  //   App.selected = _.reject(App.selected, function (model) { return model.id == entryId; });
  // },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
