var _ = require("underscore");
var $$ = require("squeak");
var layouts = require("../layouts");
var uify = require("uify");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET AVAILABLE LAYOUTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get the list of layouts for the current display or given display object
    ARGUMENTS: ( ?displayConfig <tablify·options.display> )
    RETURN: <supportedLayout[]>
  */
  getAvailableLayouts: function (displayConfig) {
    var App = this;

    // get list of layouts
    var layoutsFunOrArray = displayConfig ? displayConfig.layouts : App.currentDisplay.layouts;
    if (_.isFunction(layoutsFunOrArray)) var passedLayouts = layoutsFunOrArray.call(App)
    else var passedLayouts = layoutsFunOrArray;

    // no passed layout, return list of all available layouts
    if (!passedLayouts) return _.keys(layouts);

    // filter out invalid layout names
    var validLayouts = _.intersection(passedLayouts, _.keys(layouts));

    // handle invalidities in list of layouts passed
    if (!validLayouts.length) {
      $$.log.error("[tablify] The display options you've set are invalid. List of possible layouts have been set to it's default value.", layoutsFunOrArray);
      return _.keys(layouts);
    }
    else {
      if (validLayouts.length != passedLayouts.length) $$.log.error("[tablify] Some layouts you chose have been removed because they are not valid layout names.", "Passed layouts: ", passedLayouts, "Final layouts: ", validLayouts);
      return validLayouts;
    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET CURRENT LAYOUT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get the current layout for the current display or given display object
    ARGUMENTS: ( ?displayConfig <tablify·options.display> )
    RETURN: <string>
  */
  getCurrentLayout: function (displayConfig) {
    var App = this;
    if (!displayConfig) displayConfig = App.currentDisplay;

    // get list of available layouts
    var availableLayouts = App.getAvailableLayouts(displayConfig);

    // no current layout defined, get first one in list of available ones
    if (!displayConfig.currentLayout) return availableLayouts[0]

    // passed layout is not in list of available layouts
    else if (_.indexOf(availableLayouts, displayConfig.currentLayout) === -1) {
      $$.log.error("[tablify] The currentLayout passed, is not a valid layout name: "+ displayConfig.currentLayout, "Valid layout names in this db/folder are: "+ _.keys(availableLayouts).join(", ") +".");
      return availableLayouts[0];
    }

    // currentLayout defined and valid
    else return displayConfig.currentLayout;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SWITCH LAYOUT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: change layout used to display list of entries
    ARGUMENTS: ( layoutName <string> « name of the new layout to use, must be a supported layout name » )
    RETURN: <void>
  */
  switchLayout: function (layoutName) {
    var App = this;

    // set new layout
    App.currentDisplay.currentLayout = layoutName;

    // display layout introduction message
    if (layouts[layoutName].switchingMessage) uify.toast({
      message: layouts[layoutName].switchingMessage,
      duration: 0,
    });

    // refresh display
    App.refresh();

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
