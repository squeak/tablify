var _ = require("underscore");
var $$ = require("squeak");
var $ = require("yquerj");
var uify = require("uify");
var tippy = require("tippy.js");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DEFAULT TUTORIAL
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get default tutorial config object
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  defaultTutorial: function () {
    var App = this;

    //
    //                              MAKE STEPS OBJECT

    var steps = [
      {
        title: "The toolbar",
        text: [
          "This is the main toolbar. On a mobile device, you will need to swipe left to display it.",
          "It let's you do most actions you would want to do on the database. Like create entries, edit them, search for entries, setup synchronization of your database...",
          "To know what a specific button does, just let the mouse on it for a few seconds, it will show you a help tooltip."
        ].join("<br>"),
        target: function (App) {
          if (!App.toolbar.isShown) App.toolbar.show();
          return App.toolbar.$toolbar;
        },
      },
    ];
    if (App.toolbar.effectiveStyle == "menu") {
      steps[0].tippy = {
        arrow: false,
        offset: [0, 100],
      };
    };

    //
    //                              POSTIFY STEPS

    if (!App.config.disableEditing) steps.push(
      {
        title: "Add entries",
        text: "Use this button to add an entry in the database.",
        target: App.tutorialTargetToolbarButton("newEntry"),
        placement: App.tutorialPlacementLeftOr("top"),
      },
      {
        title: "Edit entries",
        text: "Use this button to edit the currently selected entry.",
        target: App.tutorialTargetToolbarButton("editEntry"),
        placement: App.tutorialPlacementLeftOr("top"),
      },
      {
        title: "Advanced actions",
        text: "This button opens a menu that proposes you advanced actions on the database, like adding multiple entries at once, importing/exporting entries from/to file, batch remove entries...",
        target: App.tutorialTargetToolbarButton("advancedEdits"),
        placement: App.tutorialPlacementLeftOr("top"),
      }
    );

    //
    //                              SEARCH, LAYOUT

    steps.push({
      title: "Search entries",
      text: "Use this to search some entries within the database",
      target: App.tutorialTargetToolbarButton("searchEntries"),
      placement: App.tutorialPlacementLeftOr("top"),
    });

    var displayLayoutChooseButton = App.toolbar.get("displayLayoutChoose");
    if (displayLayoutChooseButton && displayLayoutChooseButton.$button && displayLayoutChooseButton.$button.css("display") != "none") steps.push({
      title: "Display style",
      text: "To change the way entries are displayed, in a table, a list, gallery...",
      target: App.tutorialTargetToolbarButton("displayLayoutChoose"),
      placement: App.tutorialPlacementLeftOr("top"),
    });

    //
    //                              SYNCHRONIZATION STEPS

    if (App.config.synchronizationTools) {

      // SYNC STATUS
      steps.push({
        title: "Synchronization",
        text: [
          "This icon shows you the sync status of your database.",
          "If it's grey, it means you haven't setup your database to synchronize.",
          "When the database is syncing",
          "You can also click on it to get more information on the sync status.",
        ].join("<br>"),
        target: App.tutorialTargetToolbarButton("syncStatus"),
        placement: App.tutorialPlacementLeftOr("bottom"),
      });

      // SYNC MENU
      if (App.config.synchronizationTools != "statusOnly") steps.push({
        title: "Configure remotes",
        text: [
          "This let's you setup some remote databases to synchronize/backup your entries.",
          "To setup a remote, you will need to have created some remote databases on a server, and fill here their address.",
        ].join("<br>"),
        target: App.tutorialTargetToolbarButton("syncMenu"),
        placement: App.tutorialPlacementLeftOr("bottom"),
      });

    };

    //
    //                              FOOTER

    steps.push(
      {
        title: "Status bar",
        text: "This bar shows you the name of this database, the number of entries it contains.",
        target: function (App) {
          App.tutorialToolbarIsMenuHide();
          return App.$footer;
        },
        placement: "top",
      }
    );

    //
    //                              PATHS BAR

    if (App.config.pathsDatabase) steps.push(
      {
        title: "Path bar",
        text: [
          "This database is configured to allow you to create folders, and organnize your entries in directories.",
          "This bar shows you which is the currently open folder, lets you navigate to it's parent or to the root folder.",
        ].join("<br>"),
        target: function (App) {
          App.tutorialToolbarIsMenuHide();
          return App.$pathbar;
        },
        placement: "bottom",
      },
      {
        title: "Unreachable entries",
        text: [
          "If you can't find some entries you believe you had created, maybe it's because they don't have path, or an unreachable path.",
          "Clicking on this, you can browse all entries with unreachable paths, and edit them to correct them."
        ].join("<br>"),
        target: function (App) {
          App.tutorialToolbarIsMenuHide();
          return App.$pathbar.find(".unreachable-paths-button");
        },
        placement: "bottom",
      }
    );

    //
    //                              MORE HELP

    steps.push({
      title: "More infos",
      text: "Use this to have more information, restart this tutorial, check keyboard shortcuts.",
      target: App.tutorialTargetToolbarButton("helpMenu"),
      placement: App.tutorialPlacementLeftOr("bottom"),
      nextCondition: function () {
        App.tutorialToolbarIsMenuHide();
        return true;
      },
    });

    //
    //                              RESULT

    return {
      presentationText: [
        "This tutorial will show you how to use this app.",
        '<small>You can stop it any time clicking the <small><span class="icon-cross"></span></small>.</small>',
        '<small>To reopen it later, click on <span class="icon-question2"></span> in the toolbar and chose "how to use this app".</small>',
        "<big>Ready to start?</big>",
      ].join("<br>"),
      steps: steps,
    };

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  START TUTORIAL
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: start the default tutorial or a custom one
    ARGUMENTS: ( ?customTutorial <function(defaultSteps<tutorialStep[]>):tutorialStep[]> )
    RETURN: <void>
  */
  startTutorial: function (customTutorial) {
    var App = this;

    // figure out which tutorial to do
    if (customTutorial) App.currentTutorial = customTutorial
    else App.currentTutorial = App.config.tutorial ? App.config.tutorial(App.defaultTutorial()) : App.defaultTutorial();

    // propose to start tutorial
    uify.confirm({
      content: App.currentTutorial.presentationText,
      cssDialog: { maxWidth: "50em", },
      ok: function () {
        App.makeTutorialStep(0)
      },
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  END TUTORIAL
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: end tutorial display and show a toast (different toast if tutorial was interrupted or finished)
    ARGUMENTS: ( <boolean> « pass true to say that tutorial was interrupted » )
    RETURN: <void>
  */
  endTutorial: function (interrupted) {
    var App = this;
    if (App.currentTutorialStep && App.currentTutorialStep.tip) App.currentTutorialStep.tip.destroy();
    delete App.currentTutorialStep;
    if (interrupted) return uify.toast.warning({
      message: 'Tutorial interrupted, if you wish to reopen it, you can do so from the help menu that you can open clicking <span class="icon-question2"></span> in the toolbar',
      duration: 15,
    })
    else uify.toast.success({
      message: "Ok, you're done!<br>Hope this helped you understand a bit how to use the app :)",
      duration: 10,
    });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE TUTORIAL STEP
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: display asked tutorial step
    ARGUMENTS: ( stepNumber <integer> )
    RETURN: <void>
  */
  makeTutorialStep: function (stepNumber) {
    var App = this;

    // destroy previous step if there is one
    if (App.currentTutorialStep && App.currentTutorialStep.tip) App.currentTutorialStep.tip.destroy();

    // attach current step globally
    App.currentTutorialStep = App.currentTutorial.steps[stepNumber];
    App.currentTutorialStep.index = stepNumber;

    // figure out tooltip target
    var stepTarget = $(App.currentTutorialStep.target(App))[0];

    // COULD NOT FIGURE OUT TARGET
    if (!stepTarget) {
      $$.log.detailedError(
        "tablify/makeTutorialStep",
        "Failed to create step "+ stepNumber,
        "Could not figure out it's target.",
        "Step options: ", App.currentTutorialStep
      );
      App.nextTutorialStep();
    }

    // CREATE STEP
    else {
      var fullTippyOptions = $$.defaults({
        content: _.bind(App.makeStepContent, App),
        allowHTML: true,
        placement: $$.result(App.currentTutorialStep.placement, App) || "left",
        trigger: "manual",
        delay: 100,
        theme: "tutorial",
        appendTo: $(App.config.$tutorialContainer)[0] || document.body,
        interactive: true,
        hideOnClick: false,
      }, App.currentTutorialStep.tippy);
      App.currentTutorialStep.tip = tippy(stepTarget, fullTippyOptions);
      App.currentTutorialStep.tip.show();
    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SHOW NEXT/PREVIOUS STEP
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: go to next tutorial step
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  nextTutorialStep: function () {
    var App = this;
    if (App.currentTutorialStep && App.currentTutorialStep.nextCondition && !App.currentTutorialStep.nextCondition(App)) return
    else if (!App.currentTutorialStep) return
    else if (App.currentTutorialStep.index < App.currentTutorial.steps.length -1) App.makeTutorialStep(App.currentTutorialStep.index + 1)
    else App.endTutorial();
  },

  /**
    DESCRIPTION: go to previous tutorial step
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  previousTutorialStep: function () {
    var App = this;
    if (App.currentTutorialStep && App.currentTutorialStep.previousCondition && !App.currentTutorialStep.previousCondition(App)) return
    else if (!App.currentTutorialStep || App.currentTutorialStep.index == 0) return
    else App.makeTutorialStep(App.currentTutorialStep.index - 1);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE STEP CONTENT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: make content of tutorial bubble for current step
    ARGUMENTS: ( ø )
    RETURN: <domElement>
  */
  makeStepContent: function () {
    var App = this;

    var $t = $("<div>")
    $t.addClass("tutorial-step");

    // TITLE BAR
    uify.item({
      $container: $t,
      title: App.currentTutorialStep.title,
      comment: "step "+ (App.currentTutorialStep.index + 1) +"/"+ App.currentTutorial.steps.length,
      button: {
        icomoon: "cross",
        title: "interrupt tutorial",
        click: function () { App.endTutorial(true); },
      },
    });

    // CONTENT
    $t.div({
      htmlSanitized: $$.result(App.currentTutorialStep.text),
    });

    // NEXT BUTTON
    $t.div({
      class: "navigate-step-button",
      text: (App.currentTutorial.steps.length - App.currentTutorialStep.index == 1) ? "finish" : "next",
    }).click(function () { App.nextTutorialStep(); });

    // PREVIOUS BUTTON
    if (App.currentTutorialStep.index > 0) $t.div({
      class: "navigate-step-button",
      text: "previous",
    }).click(function () { App.previousTutorialStep(); });

    // RETURN ELEMENT
    return $t[0];

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  UTILITIES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  tutorialPlacementLeftOr: function (or) {
    return function (App) {
      return App.toolbar.effectiveStyle == "menu" ? or +"-start" : "left";
    };
  },

  tutorialTargetToolbarButton: function (buttonName) {
    return function (App) {
      if (!App.toolbar.isShown) App.toolbar.show(true);
      return App.toolbar.get(buttonName).$icon;
    };
  },

  tutorialToolbarIsMenuHide: function () {
    var App = this;
    if (App.toolbar.effectiveStyle == "menu" && App.toolbar.isShown) App.toolbar.hide();
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
