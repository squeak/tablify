var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/string");
var layouts = require("../layouts");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  FILTERING
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/*
  DESCRIPTION: fitler the list of rows to display
  ARGUMENTS: ( collectionModels <backboneModel[]> ){@this=App}
  RETURN: <backboneModel[]>
*/
function filterEntries (collectionModels) {
  var App = this;

  //
  //                              MAKE MODELS LIST TO WORK ON

  var filteredListOfEntries = _.clone(collectionModels);

  //
  //                              CONSTANT FILTER

  if (App.currentDisplay.filter) filteredListOfEntries = _.filter(filteredListOfEntries, App.currentDisplay.filter);

  //
  //                              PATH FILTERING

  if (App.config.pathsDatabase) {

    // find the pathId for the current folder (= the id of the current folder, or a code for custom displays)
    var parentId = App.Collection.getPathIdFromPath(App.currentPath);

    // FILTER: DO NOT FILTER ON PATHS
    if (parentId == "_ALL_") filteredListOfEntries = filteredListOfEntries
    // FILTER: GET ONLY ENTRIES FOR WHICH PATH CANNOT RESOLVE TO ANY FOLDER
    else if (parentId == "_UNREACHABLE_") filteredListOfEntries = _.filter(filteredListOfEntries, function (model) {
      return !model.getFullPaths().length;
    })
    // FILTER: GET ONLY ENTRIES THAT HAVE AT LEAST ONE PATH THAT THE CURRENT FOLDER PRODUCES
    else if (parentId) filteredListOfEntries = _.filter(filteredListOfEntries, function (model) {
      return _.indexOf(model.get("path"), parentId) != -1;
    });

  };

  //
  //                              TEMPORARY FILTERING

  if (App.currentFilter) filteredListOfEntries = _.filter(filteredListOfEntries, App.currentFilter);

  //
  //                              RETURN

  return filteredListOfEntries;

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SORTING
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var sortCache = {};

/*
  DESCRIPTION: get the full value to use to sort list
  ARGUMENTS: (
    !App <tablifyApp>,
    !model <model>,
    !sort <key|key[]|function(model):<string|number|<string|number>[]>>,
  )
  RETURN: <<string|number>[]>
*/
function getSort (App, model, sort) {

  // TRY TO GET CACHED VALUE IF THERE IS ONE
  var sortCacheKey = _.isFunction(sort) ? sort.toString() : JSON.stringify(sort);
  if (!sortCache[sortCacheKey]) sortCache[sortCacheKey] = {};
  var entryUniqueIdentifier = JSON.stringify(model.attributes);
  if (_.has(sortCache[sortCacheKey], entryUniqueIdentifier)) return sortCache[sortCacheKey][entryUniqueIdentifier];

  // SORT BY FUNCTION (can return an array or a string)
  if (_.isFunction(sort)) var valueToSortBy = sort.call(App, model) || ""

  // SORT BY ONE UNIQUE DEEP KEY (can return an array or a string)
  else if (_.isString(sort)) valueToSortBy = model.getValue(sort)
  else if (_.isArray(sort) && sort.length == 1) valueToSortBy = model.getValue(sort[0])

  // SORT BY ARRAY OF DEEP KEYS (will return a string)
  else {
    if (!_.isArray(sort)) sort = [sort];
    if (sort.length == 1) valueToSortBy = model.getValue(sort[0])
    else {
      var valueToSortBy = "";
      _.each(sort, function (sortKey) {
        valueToSortBy += model.getValue(sortKey) || "";
      });
    };
  };

  // MAKE STRINGS LOWER CASE AND LATINIZED (otherwise sorting is putting uppercase letters first and strange letters at the end)
  if (!_.isArray(valueToSortBy)) valueToSortBy = [valueToSortBy];
  valueToSortBy = _.map(valueToSortBy, function (val) {
    if (_.isString(val)) return $$.string.latinize(val.toLocaleLowerCase())
    else return val;
  });

  // CACHE VALUE
  // cache calculated value to optimize getSort (because latinizing and everything here is a bit of heavy work)
  // the sort value will be recalculated only if _rev has changed
  sortCache[sortCacheKey][entryUniqueIdentifier] = valueToSortBy;

  // RETURN VALUE
  return valueToSortBy;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SORT ENTRIES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/*
  DESCRIPTION: sort the given list according to rules in config
  ARGUMENTS: (
    !App <tablifyApp>,
    !unsortedList <model[]>
  )
  RETURN: {
    sortValues: <<string|number>[]> « the list of each entry's made up sorting values that have been used to sort by »,
    sortedList: <model[]> « the list of models sorted »,
  }
*/
function sortEntries (App, unsortedList) {

  //
  //                              GET SORTING AND REVERSE STATUSES CURRENTLY FOR THIS DISPLAY

  var sortingAndReverseStatuses = App.getSortingAndReverse();

  //
  //                              CLONE ENTRIES WITH MULTIPLE VALUES IN THE SORTING COLUMN (so that they appear at multiple indexes in the list)

  var unsortedListWithDuplicates = [];
  _.each(unsortedList, function (model, index) {

    // get sorting value for this model
    var sortValue = getSort(App, model, sortingAndReverseStatuses.sorting);

    // add all values of this sorting to unsortedListWithDuplicates
    _.each(sortValue, function (sval) {
      unsortedListWithDuplicates.push({
        sortValue: sval,
        model: model,
      });
    });

  });

  //
  //                              SORT, REVERSE IF ASKED, AND RETURN

  var sortedList = _.sortBy(unsortedListWithDuplicates, function (obj) { return obj.sortValue; });
  if (sortingAndReverseStatuses.sortReverse) sortedList.reverse();
  return {
    sortValues: _.pluck(sortedList, "sortValue"),
    sortedList: _.pluck(sortedList, "model"),
  };

  //                              ¬
  //

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: generate the list of entries to display now (applying, filtering, sorting, limiting...)
    ARGUMENTS: ( ø )
    RETURN: {
      sortValues: <string[]> « the list of entries made up sorting values that have been used to sort by (contain all db entries) »,
      unlimitedList: <model[]> « this list of entries sorted »,
      list: <model[]> « the list of entries sorted and limited (and with rest applied if necessary) this is the one you probably want »,
      hasMoreBefore: <boolean>,
      hasMoreAfter: <boolean>,
    }
  */
  generateDisplayLists: function () {
    var App = this;

    // GET LIST OF MODELS IN COLLECTION
    var collectionModels = _.clone(App.Collection.models);

    // FILTER
    var filteredList = filterEntries.call(App, collectionModels);

    // SORT
    var sortingObject = sortEntries(App, filteredList);

    // MAKE RESULT OBJECT
    var result = {
      sortValues: sortingObject.sortValues,
      unlimitedList: sortingObject.sortedList,
      list: sortingObject.sortedList,
    };

    // START FROM GIVEN RESULT
    if (App.currentDisplay.rest) {
      result.list = _.rest(result.list, App.currentDisplay.rest);
      result.hasMoreBefore = true;
    };

    // LIMIT NUMBER OF RESULTS
    if (App.currentDisplay.limit) {
      result.list = _.first(result.list, App.currentDisplay.limit);
      if (result.unlimitedList.length > result.list.length) result.hasMoreAfter = true;
    };

    return result;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET SORTING AND REVERSE STATUSES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get sorting and reverse status from global config (and columns config if layout is table)
    ARGUMENTS: ( ø )
    RETURN: <{
      sorting: <sorting> « see README for type details »,
      sortReverse: <boolean>,
    }>
  */
  getSortingAndReverse: function () {
    var App = this;

    if (layouts[App.currentDisplay.currentLayout].getSortingAndReverse) return layouts[App.currentDisplay.currentLayout].getSortingAndReverse(App.currentDisplay)
    else return {
      sorting: App.currentDisplay.sorting || "name",
      sortReverse: App.currentDisplay.sortReverse,
    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
