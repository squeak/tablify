var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/dom");
var $ = require("yquerj");
var async = require("async");

var tablifyToolbar = require("../toolbar");
var tablifyShortcuts = require("../shortcuts");

var methodsFiles = [
  require("./batchActions"),
  require("./displayLists"),
  require("./dom"),
  require("./edition"),
  require("./importExport"),
  require("./layout"),
  require("./modes"),
  require("./paths"),
  require("./processEntries"),
  require("./refresh"),
  require("./search"),
  require("./selection"),
  require("./synchronization"),
  require("./tutorial"),
  require("./url"),
  require("./utilities"),
  require("./view"),
];

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  RUN FIRST REFRESH
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: display collection for first time, fetch and display previously opened entries, setup synchronization
  ARGUMENTS: ( App )
  RETURN: <void>
*/
function runFirstRefresh (App) {

  // inital fetch of collection
  App.refreshCollection(function () {

    // check if some entries should be opened
    App.openEntriesSpecifiedInHash();

    // live sync and watch changes
    App.syncSetup();

    // refresh app when url is modified (what was the point of this?, it makes the app refresh every time an entry is viewed, seems pointless)
    // $$.dom.onPopstate(function () {
    //   App.updateDisplayConfigFromQuery();
    //   App.refresh();
    // });

    // run init callback
    if (App.config.callback) App.config.callback(App);

  });

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (config, Collection, EntryViewMaker) {


  var methods = {
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    //
    //                              bind table to DOM element

    el: $(config.$container),

    //
    //                              at initialization listen to relevent events on the collection

    initialize: function () {
      var App = this;

      //
      //                              MAKE FULL APP OBJECT, GETTING FULL CONFIG

      App.config = config;
      App.cache = {}; // create cache object
      App.Collection = Collection;
      App.EntryViewMaker = EntryViewMaker;
      App.tablifyId = $$.uuid();

      // GET ADDITIONAL CONFIG FROM QUERY
      App.updateDisplayConfigFromQuery();
      // MAKE CURRENT DISPLAY OBJECT
      App.currentDisplay = App.config.display;
      if (!App.currentPath) App.currentPath = App.config.display.currentPath || "/";
      if (App.config.pathsDatabase) App.currentPathId = "_/";
      App.currentDisplay.currentLayout = App.getCurrentLayout();

      //
      //                              MAKE SHORTCUTS AND TOOLBAR

      App.shortcuts = tablifyShortcuts(config, App);
      App.toolbar = tablifyToolbar(config, Collection, App);

      //
      //                              MAKE DEBOUNCE REFRESH FUNCTION FROM REFRESH FUNCTION

      // make sure that collection refreshes only if refresh has last been called more than a second ago
      App.debouncedRefreshCollection = _.throttle(App.refreshCollection, 1000);

      //
      //                              APPEND TABLE

      // add class to container
      App.$el.addClass("tablify tablify--"+ App.tablifyId);
      if (App.config.class) App.$el.addClass($$.result.call(App, App.config.class));

      // display loading spinner
      App.displaySpinner()

      // create pathbar
      App.$pathbar = App.$el.div({ class: "pathbar", });
      // create table
      App.table = App.$el.table(); // TODO adapt table, body, header, footer tags type to display? (would be neater, but maybe not useful to spend time on that if it doesn't start creating issues)
      // append column headers
      App.$header = App.table.thead({ class: "table-header", });
      // append table body and page footer
      App.$body = App.table.tbody().click(function(){ App.select(); });
      App.$footer = App.$el.div({ class: "table-footer" });

      //
      //                              APPEND EDITOR DIV

      App.$editorContainer = config.$editorContainer ? config.$editorContainer : config.$container;
      App.$viewContainer = config.$viewContainer ? config.$viewContainer : config.$container;

      //
      //                              APPLY CUSTOM PROGRAMMATIC CSS RULES

      App.refreshCustomCssRules();

      //
      //                              MOUNT OPTIONAL PLUGINS (after setting up App)

      if (config.plugins) async.eachLimit(
        config.plugins,
        1,
        function (plugin, callback) { plugin.call(App, callback); },
        function (err) {
          if (err) $$.log.error("Error while loading tablify plugins: ", err, "App: ", App);
          runFirstRefresh(App);
        }
      )
      else runFirstRefresh(App);

      //                              ¬
      //

    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  };

  _.each(methodsFiles, function (methodsList) {
    _.each(methodsList, function (methodFunc, methodName) {
      methods[methodName] = methodFunc;
    });
  });

  return methods;

};
