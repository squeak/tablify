var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/url");
var viewify = require("viewify");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: open the given entry's view
    ARGUMENTS: (
      !model <backboneModel>,
      ?storeInHash <boolean|"e"|"v"> «
        if defined, will bypass default settings of storeInHash for this type,
        you can also specify in which part of hash you want to store it passing a string
      »,
      ?event <event> « the event if it was triggered by a click or something like this »,
    )
    RETURN: <void>
  */
  openEntry: function (model, storeInHash, event) {
    var App = this;

    var typeConfig = model.getTypeConfig();

    // add to list of entries opened to view in hash (except if type config requested not to)
    function addToHash () {
      if (_.isUndefined(storeInHash)) storeInHash = _.indexOf($$.getValue(typeConfig, "action.storeInHash"), "view") != -1;
      if (storeInHash) App.addInHashList(model.id, _.isString(storeInHash) ? storeInHash : "v");
    };
    // remove from list of entries opened to view in hash (except if type config requested not to)
    function removeFromHash () {
      if (storeInHash) App.removeFromHashList(model.id, _.isString(storeInHash) ? storeInHash : "v");
    };

    var customViewFunction = $$.getValue(typeConfig, "action.view");
    if (_.isFunction(customViewFunction)) customViewFunction.call(App, model, typeConfig, addToHash, removeFromHash, event)
    else App.openInViewify(model, typeConfig, addToHash, removeFromHash, event);

    // run any global entry opening callback
    if (App.config.onEntryOpen) App.config.onEntryOpen.call(App);

  },

  currentlyViewifiedEntries: {},
  /**
    DESCRIPTION: open the given entry with viewify
    ARGUMENTS: (
      !model <backboneModel>,
      !typeConfig <entryType>,
      ?addToHash <function(ø)> « will add entry id to hash when executed »,
      ?removeFromHash <function(ø)> « will remove entry id from hash when executed (should be ran when entry is closed) »,
    )
    RETURN: <void>
  */
  openInViewify: function (model, typeConfig, addToHash, removeFromHash) {
    var App = this;

    if (addToHash) addToHash();

    // if entry is already displayed, just focus it
    if (App.currentlyViewifiedEntries[model.id]) {
      App.currentlyViewifiedEntries[model.id].windified.active();
      return;
    };

    // make sure entry contain attachments
    model.fetchWithAttachments(function () {

      // MAKE VIEWIFY OPTIONS
      var viewifyOptions = $$.defaults(
        // hardcoded options
        {
          $container: App.$viewContainer,
          category: "tablify--"+ typeConfig.name,
          closed: function () {
            delete App.currentlyViewifiedEntries[model.id];
            if (removeFromHash) removeFromHash();
          },
          storage: {
            tablifyApp: App,
            typeConfig: typeConfig,
          },
          // options that should be calculated by windify (so it will be recalculated at each refreshing of the window)
          windifyOptionsMaker: function () {
            var modelSameOrUpdated = App.Collection.get(model.id);
            if (!modelSameOrUpdated) return $$.log.error("Could not properly refresh window. Failed to find in collection, the model with the following model id:", model.id);
            return modelSameOrUpdated.getWindifyDisplayValues(typeConfig);
          },
          // options that should be calculated by viewify (so it will be recalculated at each refreshing of the window)
          viewifyOptionsMaker: function () {

            // add edit button
            if (!App.config.disableEditing) {
              var editEntryButtonIndex = _.findIndex(typeConfig.view.buttons, function (butt) { return butt.name === "editEntry"; });
              var editEntryButton = {
                name: "editEntry",
                icomoon: "quill",
                key: "shift + 7", // cannot use "shift + e" because it conflicts with tablify edit shortcut | need to make a system for app to be able to analyse context in which shortcut is executed
                title: "edit this entry",
                click: function () {
                  // always get model from collection, otherwise the model might not be up to date
                  App.openEntryEditor(App.Collection.get(model.id) || model);
                },
              };
              if (editEntryButtonIndex === -1) typeConfig.view.buttons.push(editEntryButton)
              else typeConfig.view.buttons[editEntryButtonIndex] = editEntryButton;
            };

            // options passed in type config view section
            return typeConfig.view;

          },
        },

      );

      // ENTRY WITH FULL ATTACHMENTS
      viewifyOptions.entry = model.attributes; // attach it here, not before default because otherwise bug with $$.defaults function

      // CREATE VIEWIFY WINDOW
      var viewified = viewify(viewifyOptions);

      // ADD VIEWIFIED TO LIST OF CURRENTLY VIEWED ENTRIES
      App.currentlyViewifiedEntries[model.id] = viewified;

    });

  },

  /**
    DESCRIPTION: open the currently selected entry view
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  openSelectedEntries: function () {
    var App = this;
    if (App.selected && App.selected.length) _.each(App.selected, function (model) {
      App.openEntry(model);
    });
  },

  /**
    DESCRIPTION: change current table's path filtering
    ARGUMENTS: (
      !pathOrFolderModel <string|backboneModel> «
        new path/folder you want to open
        if it's a path, it should be an absolute path (= start with a "/"), or a special path (= UNREACHABLE, ALL)
        if it's a model, it should be a folder entry and therefore have a "name" key defined
      »,
      ?doNotRemoveFilter <boolean> « keep currentFilter value while navigating »,
      ?callback <function(ø)> « an optional callback to execute when path has finised opening »,
    )
    RETURN: <void>
  */
  openPath: function (pathOrFolderModel, doNotRemoveFilter, callback) {
    var App = this;

    // add current list of selected entries to selectedCache
    App.selectedCache[App.currentPathId] = App.selected;

    // remove any previously set filter (navigating between folder should discart current filtering)
    if (App.currentFilter && !doNotRemoveFilter) delete App.currentFilter;

    // no path given, open root
    if (!pathOrFolderModel) App.currentPath = "/"

    // string given
    else if (_.isString(pathOrFolderModel)) App.currentPath = pathOrFolderModel

    // model given
    else {

      // figure out if current path is a folder
      if (_.isString(App.currentPath) && App.currentPath.match(/^\//)) var currentFolderPath = App.currentPath;

      // if current path is a folder, make path from current + folder name
      if (currentFolderPath) App.currentPath = currentFolderPath + pathOrFolderModel.get("name") +"/"
      // else get the first of this folder's full paths
      else App.currentPath = pathOrFolderModel.getFullPaths()[0];

    };

    // save new path to hash
    $$.url.hash.modify({ path: App.currentPath, })

    // by default, make sure that nothing will be selected in newly opened folder
    App.select();

    // refresh display
    App.refresh(function () {
      // figure out entries to select (or if selectedCache should be cleared)
      if (App.selectedCache[App.currentPathId]) App.select(App.selectedCache[App.currentPathId]);
      // run any eventual callback
      if (callback) callback();
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
