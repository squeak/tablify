var _ = require("underscore");
var $$ = require("squeak");
var dialogify = require("dialogify");
var uify = require("uify");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ADD MULTIPLE ENTRIES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: display a dialog proposing the user to add multiple entries to database parsing them as json objects
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  batchAdd: function () {
    var App = this;

    dialogify.json({
      title: "Add multiple entries to database",
      ok: function (entries) {
        if (!confirm("Are you sure you want to add this/those entries to the database?")) return false;
        App.Collection.bulkPut(entries, {
          success: "Successfully added %%count%% entries.",
          warningSuccess: "Added %%count%% entries successfully.",
          warningFail: "Failed to add %%count%% entries.",
          error: "Error trying to add %%count%% entries.",
        });
      },
      cancel: function (entries) {
        if (entries && !confirm("Cancel and lose modifications?")) return false;
      },
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MODIFY MULTIPLE ENTRIES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: open a dialog to modify multiple entries at a time in raw view
    ARGUMENTS: (
      ?entriesToModify <couchEntry[]> « the entries you want to modify »,
      ?comingBackToModify <couchEntry[]> « if you've already started modifying the entries, the current state of modification (useful when confirm saving has been canceled to go back to modifications window) »,
    )
    RETURN: <void>
  */
  batchModify: function (entriesToModify, comingBackToModify) {
    var App = this;

    var entriesBefore = entriesToModify || _.pluck(App.selected, "attributes");

    dialogify.json({
      title: "Modifying "+ entriesBefore.length +" entries in database",
      value: comingBackToModify || entriesBefore,
      okText: "save",
      ok: function (entriesAfter) {

        // wrong input
        if (!entriesAfter) {
          uify.toast.error("Cannot apply modifications, there is probably an error in the syntax.");
          return false;
        }
        // no modifications made
        else if (_.isEqual(entriesBefore, entriesAfter)) {
          uify.toast("No modifications to make.");
          return true;
        };

        // show list of modifications
        App.calculateAndDisplayDifferences({
          entriesBefore: entriesBefore,
          entriesAfter: entriesAfter,
          ok: function (modifiedEntriesFinalList) {
            App.Collection.bulkPut(modifiedEntriesFinalList, {
              success: "Successfully modified %%count%% entries.",
              warningSuccess: "Modifed %%count%% entries successfully.",
              warningFail: "Failed to modify %%count%% entries.",
              error: "Error trying to modify %%count%% entries.",
            });
          },
          cancel: function () {
            App.batchModify(entriesBefore, entriesAfter);
          },
        });

      },
      cancel: function (entriesAfter) {
        if (!_.isEqual(entriesBefore, entriesAfter) && !confirm("Cancel and lose modifications?")) return false;
      },
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REMOVE MULTIPLE ENTRIES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: propose user to remove all currently selected entries from database
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  batchRemove: function () {
    var App = this;

    if (!App.selected || !App.selected.length) alert("No entries selected.\nPlease select at least one entry to do this.")

    else {
      var prompted = prompt("Are you sure you want to delete "+ App.selected.length +" entries.\nWrite 'yes' below to confirm.");
      if (prompted == "yes") {
        _.each(App.selected, function (model) {
          model.set("_deleted", true);
        });
        App.Collection.bulkPut(_.pluck(App.selected, "attributes"), {
          success: "Successfully removed %%count%% entries.",
          warningSuccess: "Removed %%count%% entries successfully.",
          warningFail: "Failed to remove %%count%% entries.",
          error: "Error trying to remove %%count%% entries.",
        });
      }
      else if (prompted) alert("You didn't confirm, canceled.");
    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
