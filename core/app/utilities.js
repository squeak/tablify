var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/storage");
var getType = require("../utilities/getType");
var layouts = require("../layouts");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET TYPE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION:
    ARGUMENTS: (

    )
    RETURN: <>
  */
  getTypeConfig: function (type) {
    var App = this;
    return getType(type, App.config);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FILTER, FILTERRAW, FIND, LIMIT TO BE USED TO MANIPULATE TABLIFY FROM INTERFACES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: limit the number of entries displayed
    ARGUMENTS: ( limit <number> )
    RETURN: <void>
  */
  limit: function (limit) {
    var App = this;
    App.currentDisplay.limit = limit;
    $$.storage.local("tablifyLength_"+ App.config.name, limit);
    App.refresh();
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET ENTRY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get the index in currently displayed list, of the given model
    ARGUMENTS: ( !modelId <string> )
    RETURN: <number>
  */
  getIndex: function (modelId) {
    var App = this;
    return App.currentDisplayedTable.findIndex(function (mod) { return mod.id == modelId; })
  },

  /**
    DESCRIPTION: get an entry in the database from it's id or return it if it's already a model
    ARGUMENTS: ( !modelOrId <backboneModel|string> )
    RETURN: <backboneModel>
  */
  getEntry: function (modelOrId) {
    var App = this;
    if (_.isString(modelOrId)) return App.Collection.get(modelOrId)
    else return modelOrId;
  },

  /**
    DESCRIPTION: get an entry or list of entries in the database from their id or return them if they are already models
    ARGUMENTS: ( ?modelModelIdOrIds <idOrBackboneModel|idOrBackboneModel[]> )
    RETURN: <backboneModel[]>
    TYPE:
      idOrBackboneModel = <id|backboneModel>
  */
  getEntryOrEntries: function (modelModelIdOrIds) {
    var App = this;

    if (_.isArray(modelModelIdOrIds)) var result = _.map(modelModelIdOrIds, function (modelOrId) { return App.getEntry(modelOrId); })
    else if (modelModelIdOrIds) var result = [App.getEntry(modelModelIdOrIds)]
    else var result = [];

    var toReturn = _.compact(result);

    // log error if failed to get some elements
    if (toReturn.length != result.length) $$.log.error("Getting some entries failed for at least one entry (maybe you passed the wrong identifier).\nWhat you passed:\n", modelModelIdOrIds, "\nWhat was returned:\n", toReturn);
    return toReturn;

  },

  /**
    DESCRIPTION: return the list of models of the asked entries
    ARGUMENTS: (
      !list <idOrPartialObject[]>,
      ?includeUnexisting <boolean> « if true, will create »,
    )
    RETURN: <{
      present: <backboneModel[]>,
      absent: <<string|object>[]>,
    >
    TYPE:
      idOrPartialObject = <
        |string
        |object «
          will use Collection.findWhere to find an entry matching the key-values of this given object in the database
          if no model is found, the object will be returned in the "absent" array
        »
      >
  */
  findExistingModelsFromIdsOrPartialObjects: function (list, includeUnexisting) {
    var App = this;
    var result = { present: [], absent: [], };
    _.each(list, function (idOrPartialObject) {
      // find corresponding model
      if (_.isObject(idOrPartialObject)) var foundModel = App.Collection.findWhere(idOrPartialObject)
      else var foundModel = App.Collection.get(idOrPartialObject);
      // depending if model is found or not, output in present or absent
      if (foundModel) result.present.push(foundModel)
      else result.absent.push(idOrPartialObject);
    });
    return result;
  },

  /**
    DESCRIPTION: get model of the entry next to the selected one (if already at last, return current selected entry model)
    ARGUMENTS: ( ø )
    RETURN: <model>
  */
  getNextEntry: function () {
    var App = this;

    // case if nothing selected
    if (!App.selected || !App.selected.length) {
      App.selected = [App.currentDisplayedTable[0]];
      return _.first(App.selected);
    };

    // get entry of last selected entry
    var selectedIndex = _.indexOf(App.currentDisplayedTable, App.selected[App.selected.length - 1]);

    // select next entry (or if last, return current entry)
    if (selectedIndex+1 <= App.currentDisplayedTable.length - 1) return App.currentDisplayedTable[selectedIndex+1]
    else return App.currentDisplayedTable[selectedIndex];

  },

  /**
    DESCRIPTION: get model of the entry previous to the selected one (if already at first, return current selected entry model)
    ARGUMENTS: ( ø )
    RETURN: <model>
  */
  getPreviousEntry: function () {
    var App = this;

    // case if nothing selected
    if (!App.selected || !App.selected.length) {
      App.selected = [App.currentDisplayedTable[App.currentDisplayedTable.length - 1]];
      return _.last(App.selected);
    };

    // get index of first selected entry
    var selectedIndex = _.indexOf(App.currentDisplayedTable, App.selected[0]);

    // select previous entry (or if first, do nothing)
    if (selectedIndex-1 >= 0) return App.currentDisplayedTable[selectedIndex-1]
    else return App.currentDisplayedTable[selectedIndex];

  },

  /**
    DESCRIPTION: get grouping display config from display options or with customly set layout method
    ARGUMENTS: ( keyToGet <"grouping"|"groupingCustomDisplay"> )
    RETURN: <tablifyOptions.display>
  */
  getGrouping: function (keyToGet) {
    var App = this;

    if (layouts[App.currentDisplay.currentLayout].getGrouping) return layouts[App.currentDisplay.currentLayout].getGrouping(App.currentDisplay, keyToGet)
    else return App.currentDisplay[keyToGet];

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET MANUAL SORTING INDEX FROM MODEL
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get the index of this model in the manual sorting order of current folder
    ARGUMENTS: ( modelOrEntry <backboneModel|couchEntry> )
    RETURN: <integer>
  */
  enableManualSorting: function (modelOrEntry) {
    var App = this;
    var entry = modelOrEntry.isBackboneModel ? modelOrEntry.attributes : modelOrEntry;
    App.manualSortingIsEnabled = true;
    var position = $$.getValue(entry, "system.positionIn_"+ App.currentPathId +".i");
    if (_.isUndefined(position)) return 999999999999999
    else return position;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: update tablifyApp.currentPath from url hash
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  updateDisplayConfigFromQuery: function () {
    var App = this;
    var hashObject = $$.url.hash.getAsObject();
    App.currentPath = hashObject.path || "/";
    // if (hashObject.f) App.currentFilter = hashObject.f // TODO add currentFilter support? (then should also add it to filter function)
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
