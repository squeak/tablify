var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/url");
var $ = require("yquerj");
var defaultConfig = require("./config-default");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

var confer = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  VERIFY CONFIG
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  verifyConfig: function (config) {

    //
    //                              CHECK THAT MANDATORY CONFIGURATION ELEMENTS ARE PROPERLY DEFINED

    $$.mandatory(config, {
      $container: $$.isJqueryElement,
      display: $$.isObjectLiteral,
    });

    //
    //                              REMEMBER PREVIOUSLY SET LIMIT IF ASKED

    if (!config.display.limit && config.display.rememberLimit) config.display.limit = $$.storage.local("tablifyLength_"+ config.name) || config.display.limit;

    //
    //                              MAKE SURE THERE IS SOMETHING SET TO config.display.columns (A BIT REDUNDANT WITH DEFAULT CONFIG, BUT IN CASE THE USER SETS AN EMPTY ARRAY)

    if (!_.isArray(config.display.columns) || !config.display.columns.length) config.display.columns = $$.clone(defaultConfig.display.columns, 5);
    if (!config.display.currentSelectedColumn || !_.findWhere(config.display.columns, { key: config.display.currentSelectedColumn, })) config.display.currentSelectedColumn = $$.getValue(config, "display.columns[0].key");

    //
    //                              IF pathsDatabase MAKE SURE FOLDER ENTRY TYPE IS PROPERLY SET

    if (config.pathsDatabase && config.pathsDatabaseFolderEntry) {
      var folderPresets = {
        name: config.pathsDatabaseFolderEntry,
        display: {
          _recursiveOption: true,
          title: function (entry) { return entry.name; },
          color: function (entry) { return entry.color; },
          icon: "folder",
        },
        action: {
          _recursiveOption: true,
          view: function (model) {
            var App = this;
            App.openPath(model);
          },
          storeInHash: ["edit"], // this is set so that tablify do not store in hash the fact that they are viewed, tablify makes a slighlty more pretty url
        },
      };
      var folderConfigIndex = _.findIndex(config.entryTypes, function (entryType) { return entryType.name == config.pathsDatabaseFolderEntry; });
      if (folderConfigIndex === -1) config.entryTypes.push($$.clone(folderPresets, 3))
      else config.entryTypes[folderConfigIndex] = $$.defaults(folderPresets, config.entryTypes[folderConfigIndex]);
    };

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE FULL CONFIG
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  makeFullConfig: function (passedConfig) {
    var config = $$.defaults(defaultConfig, passedConfig);
    confer.verifyConfig(config);
    return config;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = confer;
