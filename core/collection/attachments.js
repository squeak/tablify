var _ = require("underscore");
var $$ = require("squeak");
var async = require("async");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET ONE ENTRY WITH ATTACHMENTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get entry with all it's attachments
    ARGUMENTS: (
      !entry <couchEntry> « the entry you want to get full »
      !callback <function(entryWithAttachments<couchEntry>)> « executed when done »,
      ?errCallback <function(err)> « if undefined, will alert a detailed error »,
    )
    RETURN: <void>
  */
  getEntryWithAttachments: function (entry, callback, errCallback) {

    // has attachments, get full in pouch
    if (
      // has attachments
      _.keys(entry._attachments).length &&
      // but doesn't have their data
      !_.chain(entry._attachments).pluck("data").compact().value().length
    ) this.pouch.get(entry._id, { attachments: true, }).catch(function (err) {
      if (errCallback) errCallback(err)
      else $$.alert.detailedError(
        "tablify/collection·getEntryWithAttachments",
        "Cannot get entry with attachments.",
        err,
        entry
      );
    }).then(callback);

    // has no attachments or attachment was already gotten, simply callback (avoids wasting energy making useless requests)
    else callback(entry);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET MULTIPLE ENTRIES WITH ATTACHMENTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get entries with all they're attachments
    ARGUMENTS: (
      !entries <couchEntry[]> « the entry you want to get full »
      !callback <function(entry<couchEntry[]>)>,
      ?errCallback <function(err)> « if undefined, will alert a detailed error »,
    )
    RETURN: <void>
  */
  getEntriesWithAttachments: function (entries, callback, errCallback) {
    var Collection = this;

    async.map(entries, function (entry, next) {
      Collection.getEntryWithAttachments(entry, function (entryWithAttachments) {
        next(null, entryWithAttachments);
      }, function (err) {
        next(err);
      });
    }, function (err, entriesWithAttachments) {
      if (err) {
        if (errCallback) errCallback(err)
        else $$.alert.detailedError(
          "tablify/collection·getEntriesWithAttachments",
          "Cannot get entries with attachments.",
          err,
          entries
        );
      }
      else callback(entriesWithAttachments);
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
