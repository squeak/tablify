var _ = require("underscore");
var $$ = require("squeak");
var getAffectedChildren = require("../utilities/getAffectedChildren");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET FOLDER FROM PATH
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get the model of a folder from the full path it enables
    ARGUMENTS: ( !pathOfFolderToFind <string> )
    RETURN: <backboneModel|undefined>
  */
  getFolderFromPath: function (pathOfFolderToFind) {
    var Collection = this;
    return Collection.find(function (model) {
      return model.get("type") == "folder" && _.indexOf(model.getFullPaths(), pathOfFolderToFind) != -1;
    });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE PATH ID FROM PATH
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION:
      get the path id for a given path
      path id is the id of the folder entry that can produce this path
      ((there could be more than folder entry for a given path, but if that's the case, it will just get the first one matching))
    ARGUMENTS: ( ?path <string> « the path for which you want to get the path id, if undefined, will return root » )
    RETURN: <string>
  */
  getPathIdFromPath: function (path) {
    var Collection = this;

    // root or undefined path => custom id: "_/"
    if (!path || path == "/") return "_/"

    // UNREACHABLE path => custom id: "_UNREACHABLE_"
    else if (path == "UNREACHABLE") return "_UNREACHABLE_"

    // ALL path => custom id: "_ALL_"
    else if (path == "ALL") return "_ALL_"

    // current path is a valid path => get pathId from folder model
    else {
      var folderModel = Collection.getFolderFromPath(path);
      if (folderModel) return folderModel.id
      else $$.log.error("Failed to find folder for path: "+ path);
    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET LIST OF ENTRIES THAT ARE AFFECTED BY THE REMOVAL OF A FOLDER
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION:
      get the full list of children to remove when removing a given folder
      this function will get the list of children entries to remove, but if a child is a folder, it will also recursively get the list of children entries...
      for full documentation, see /core/utilities/getAffectedChildren script
    ARGUMENTS: ( !folderEntryToRemove <couchEntry> )
    RETURN: <{
      toRemove: <backboneModel[]> « the list of models of entries that will have to be removed »,
      toModify: <{
        [ idOfModelThatShouldBeModifed <string> ]: <{
          pathIdsToRemove: <string[]> « the list of ids that will have to be removed in the entry's path »,
          pathIdsLeft: <string[]> « the list of ids that will remain in this entry's path after modification (useful for internal processing) »,
          model: <backboneModel> « just a references to the model of this entry »,
        }>
      }>
    }>
  */
  getFullListOfChildrenAffectedByRemovalOfFolder: function (folderEntryToRemove) {
    var Collection = this;
    return getAffectedChildren(Collection, folderEntryToRemove);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MODIFY CHILDREN ENTRIES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION:
      remove/modify entries that are children of this folder entry
      if an entry is a child and has only this path, it will be removed,
      if it has other paths, the "path" of this folder will be removed in child
    ARGUMENTS: ( entryThatWasRemoved <couchEntry>, )
    RETURN: <void>
  */
  modifyOrRemoveChildrenEntries: function (entryThatWasRemoved) {
    var Collection = this;

    // get list of children entries that should be modified/removed
    var affectedChildrenModels = Collection.getFullListOfChildrenAffectedByRemovalOfFolder(entryThatWasRemoved);

    //
    //                              MODIFY ENTRIES TO MODIFY

    if (_.keys(affectedChildrenModels.toModify).length) {

      // set new path in all model that should be modified
      var entriesToModify = _.map(affectedChildrenModels.toModify, function (modifsObject) {
        modifsObject.model.attributes.path = _.clone(modifsObject.pathIdsLeft);
        return modifsObject.model.attributes;
      });

      Collection.bulkPut(entriesToModify, {
        success: "Modified %%count%% 'children' entries.",
        warningSuccess: "Modified %%count%% 'children' entries successfully.",
        warningFail: "Failed to remove %%count%% 'children' entries.",
        error: "Error trying to remove %%count%% 'children' entries.",
      });

    };

    //
    //                              REMOVE ENTRIES TO REMOVE

    if (affectedChildrenModels.toRemove.length) {

      // set all model to remove as deleted
      var entriesToRemove = _.map(affectedChildrenModels.toRemove, function (model) {
        model.attributes._deleted = true;
        return model.attributes;
      });

      Collection.bulkPut(entriesToRemove, {
        success: "Removed %%count%% 'children' entries.",
        warningSuccess: "Removed %%count%% 'children' entries successfully.",
        warningFail: "Failed to remove %%count%% 'children' entries.",
        error: "Error trying to remove %%count%% 'children' entries.",
      });

    };

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET ALL EXISTING PATHS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get the list of all possible paths in the database (as choice object)
    ARGUMENTS: ( ø )
    RETURN: <inputify·choiceObject[]>
  */
  getAllPathsChoices: function () {
    var Collection = this;

    // prepare list, including root path
    var allPaths = [{
      _isChoice: true,
      label: "/",
      value: "_/",
      search: "/",
    }];

    Collection.each(function (model) {
      // iterate all folders in db
      if (model.get("type") == "folder") {
        // o            >            +            #            °            ·            ^            :            |
        var paths = model.getFullPaths();

        allPaths.push({
          _isChoice: true,
          label: paths.join("<br>"),
          value: model.id,
          search: paths.join(),
        });

        // o            >            +            #            °            ·            ^            :            |
      };
    });

    // return a "kind of" sorted list
    return _.sortBy(allPaths, "search");

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
