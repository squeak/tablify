var _ = require("underscore");
var $$ = require("squeak");
var PouchDB = require("pouchdb");
var PouchFind = require("pouchdb-find"); // not necessary for tablify, but useful for compatibility with other modules that need it, in case tablify creates the pouch db for them
PouchDB.plugin(PouchFind);
var Backbone = require("backbone");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (config, ModelMaker) {
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  if (!config.pouchsInCache) config.pouchsInCache = {};
  if (config.pouchsInCache[config.name]) var pouch = config.pouchsInCache[config.name]
  else var pouch = config.pouchsInCache[config.name] = new PouchDB(config.name);

  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  var collectionOptions = {
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  GENERAL
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    //
    //                              MODEL AND URL WHERE TO FETCH DATABASE COUCH

    model: ModelMaker,
    // url: config.url,
    pouch: pouch, // set here to be able to access it from App
    config: config,

    //
    //                              POUCH SYNC

    sync: function (method, Collection, options) {

      if (method == "read") Collection.pouch.allDocs({
        include_docs: true,
      }).then(function (response) {
        options.success(response);
      }).catch(function (err) {
        $$.log.detailedError(
          "tablify/collection:sync",
          "Failed to fetch database list of docs.",
          err
        );
        options.error(err)
      })

      else $$.log.detailedError(
        "tablify/collection:sync",
        "Only read method is supported.",
        "Asked method: ", method,
        "Additional options passed: ", options
      );

    },

    //
    //                              GETTING ENTRIES CONTENTS FROM POUCH RESULT

    parse: function (result) {
      return _.pluck(result.rows, "doc");
    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  };

  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  // add other methods
  _.each([
    require("./attachments"),
    require("./paths"),
    require("./save"),
    require("./utilities"),
  ], function (methodsList) {
    _.each(methodsList, function (methodFunc, methodName) {
      collectionOptions[methodName] = methodFunc;
    });
  });

  // return Collection object
  return collectionOptions;

  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
};
