var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  PUT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: save or delete an entry to pouch (to delete, you just need to have set _deleted: true in your entry)
    ARGUMENTS: (
      !entryOrModel <couchEntry|backboneModel> « the entry/model to save (or delete) in database »,
      ?callback <function(response <couchResponse>)> « executed on put success (only) »,
    )
    RETURN: <void>
  */
  put: function (entryOrModel, callback) {
    var Collection = this;

    // get or make a model from entry passed
    if (entryOrModel.isBackboneModel) var model = entryOrModel
    else var model = Collection.newModel(entryOrModel);

    // type config
    try { var modelTitle = model.getDisplayValue("title") || model.id; }
    catch (e) { var modelTitle = model.id; };

    // put entry in pouch
    Collection.pouch.put(model.attributes).then(function (response) {
      if (model.attributes._deleted) uify.toast("Removed "+ modelTitle +" from database.");
      else uify.toast.success("Saved "+ modelTitle +" to database.");
      if (callback) callback(response);
    }).catch(function (err) {
      uify.toast.error("Error saving "+ modelTitle +" to database.\nSee console for more info.");
      $$.log.detailedError(
        "tablify/collection:put",
        "Failed to save entry to database.",
        "entry: ", model.attributes,
        "error: ", err
      );
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  BULK PUT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: bulk save an entry or list of entries to pouch
    ARGUMENTS: (
      !entries <couchEntry|couchEntry[]>,
      !message <{
        !success: <string> « the string should contain %%count%% placeholder, so that the appropriate number can be put »,
        !warningSuccess: <string> « the string should contain %%count%% placeholder, so that the appropriate number can be put »,
        !warningFail: <string> « the string should contain %%count%% placeholder, so that the appropriate number can be put »,
        !error: <string> « the string should contain %%count%% placeholder, so that the appropriate number can be put »,
      }>,
    )
    RETURN: <void>
  */
  bulkPut: function (entries, message) {
    var Collection = this;

    // make sure it's an array of entries
    if (!_.isArray(entries)) entries = [entries];

    // bulk save to pouch
    Collection.pouch.bulkDocs(entries).then(function (result) {

      var errors = _.where(result, { error: true, });
      if (!errors.length) uify.toast.success(message.success.replace("%%count%%", result.length))
      else {
        // toast warning
        uify.toast.warning([
          message.warningSuccess.replace("%%count%%", result.length-errors.length),
          message.warningFail.replace("%%count%%", errors.length),
          "(For details, check logs.)",
        ].join("<br>"));
        // log errors
        $$.log("Failures: ", errors);
        $$.log("Full response: ", result);
      };

    }).catch(function (err) {
      uify.toast.error(message.error.replace("%%count%%", entries.length));
      $$.log.error(err);
      $$.log.error("Entries: ", entries);
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
