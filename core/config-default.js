var _ = require("underscore");
var $$ = require("squeak");
var layouts = require("./layouts");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {

    synchronizationTools: true,
    toolbar: [],

    display: {
      _recursiveOption: true,
      layouts: _.keys(layouts),
      sorting: "name",
      rememberLimit: true,
      columns: [
        { key: "name", },
        { key: "path", },
        { key: "type", },
        { key: "color", },
      ],
      fab: "touchDevices",
      defaultEntryIcon: "spaceinvaders", // circle2, baffled, spaceinvaders, circle-thin
    },

    actions: {
      generalClickEvent: $$.isTouchDevice() ? "click .image" : "click",
      open: $$.isTouchDevice() ? "click .titles" : "dblclick",
      enterOrganizeSpecialMode: "longclick",
    },
    // actions: function () {
    //   if (config.display && config.display.currentLayout == "gallery") return {
    //     open: "click",
    //     edit: "click .icon.edit",
    //     select: "click .icon.edit",
    //   }
    //   else return {
    //     open: "dblclick",
    //     edit: "click .icon.edit",
    //     select: "click",
    //   };
    // },

  };
