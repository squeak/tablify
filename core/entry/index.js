var _ = require("underscore");
var $$ = require("squeak");
var $ = require("yquerj");
var layouts = require("../layouts");
var $$log = $$.logger("tablify/entry");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: create backbone view for one entry
  ARGUMENTS: ( config )
  RETURN:
*/
module.exports = function (config) {
  var entryMethods = {
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  VIEW DOM TAG
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    tagName: "div", // this will be replaced in initialize function

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  RENDERING
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    /**
      DESCRIPTION: add the entry to dom
      ARGUMENTS: ( !$container <yquerjObject>, )
      RETURN: <void>
    */
    render: function ($container) {
      var Entry = this;
      $container.append(Entry.$el);
      Entry.testAndSetHiddenEntry();
      Entry.refreshView();
    },

    /**
      DESCRIPTION: refresh display of entry in dom
      ARGUMENTS: ( ø )
      RETURN: <void>
    */
    refreshView: function () {

      var Entry = this;
      var model = Entry.model;
      var App = Entry.App;

      // empty in case of redisplaying
      Entry.$el.attr("id", model.id).empty();

      // make entry for the current display layout
      layouts[App.currentDisplay.currentLayout].makeEntry.call(App, model, Entry);

    },

    /**
      DESCRIPTION: check if entry is a hidden one, and if it's the case apply the appropriate classes to it
      ARGUMENTS: ( ø )
      RETURN: <void>
    */
    testAndSetHiddenEntry: function () {
      var Entry = this;

      // check if entry is a hidden one with function evaluation
      if (_.isFunction(Entry.App.config.display.hiddenEntries)) {
        if (Entry.App.config.display.hiddenEntries(Entry.model)) Entry.setHiddenEntryClasses();
      }
      // check if entry is a hidden one with partial object matching
      else if (
        Entry.App.config.display.hiddenEntries &&
        _.findWhere([Entry.model.attributes], Entry.App.config.display.hiddenEntries)
      ) Entry.setHiddenEntryClasses();

    },

    /**
      DESCRIPTION:
        add appropriate classes to this hidden entry
        (the generic "tablify-hidden_element" class, and if tablifyApp view is currently set to have hidden entries hidden also the "tablify-hidden_element-hidden" class)
      ARGUMENTS: ( ø )
      RETURN: <void>
    */
    setHiddenEntryClasses: function () {
      var Entry = this;
      Entry.$el.addClass("tablify-hidden_element");
      if (!Entry.App.hiddenElementsVisible) Entry.$el.addClass("tablify-hidden_element-hidden");
    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  INITIALIZE
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    initialize: function (initObject) {
      var Entry = this;
      Entry.App = initObject.App;

      // remake element, because Entry.tagName function is called before Entry.initialize and therefore doesn't have App defined
      Entry.$el = $("head")[layouts[Entry.App.currentDisplay.currentLayout].tagName]();
      Entry.resetElement();

      // make cell (and row) related events (doing it here instead of setting Entry.events, because Entry.events function is called before Entry.initialize and therefore doesn't have App defined)
      var entryEvents = Entry.getEvents();
      if (_.size(entryEvents)) Entry.delegateEvents(entryEvents);

      // The following is not useful at the moment because when there are changes all rows are reloaded (but would be nice to make it more efficient)
      // this.listenTo(this.model, "change", this.refreshView);
      // this.listenTo(this.model, "destroy", this.remove);

    },

    resetElement: function (elem) {
      var Entry = this;
      if (elem) {
        if (Entry.$el) Entry.$el.remove();
        Entry.setElement(elem);
        var entryEvents = Entry.getEvents();
      };
      Entry.$el.attr("id", Entry.model.id);
      Entry.$el.addClass("tablify-entry");
      if (Entry.model.getValue("type")) Entry.$el.addClass("tablify-entry_type-"+ Entry.model.getValue("type"));
      if (_.size(entryEvents)) Entry.delegateEvents(entryEvents);
    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  GET LIST OF EVENTS ON THIS ENTRY
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    getEvents: function () {
      var Entry = this;

      var events = {};

      // CELL EVENTS
      _.each(Entry.App.currentDisplay.columns, function (col, i) {
        // find possible events
        if (col.events) _.each(col.events, function (eventFunc, eventType) {
          // for this event type, find cell by it's class, assign event function
          events[eventType +" ."+ col.key] = eventFunc;
        });
      });

      // ROW EVENTS
      _.each($$.result(config.actions), function (actionSelector, actionMethodName) {

        // unrecognized action name
        if (_.indexOf(isValidAction, actionMethodName) === -1) $$log.error("Unrecognized entry action has been ignored.", "action name: "+ actionMethodName, "valid action names: "+ isValidAction.join(", "));

        // special longclick action
        if (actionSelector && actionSelector.match(/^longclick/)) {
          events[actionSelector.replace(/^longclick/, "mousedown")] = function (e) { this.clickStarted(e, actionMethodName); };
          events[actionSelector.replace(/^longclick/, "mouseup")] = "clickEnded";
          events[actionSelector.replace(/^longclick/, "mousemove")] = "mouseMove";
        }
        // other jquery supported actions
        else if (actionSelector) events[actionSelector] = actionMethodName;

      });

      // return list of events
      return events;

    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  SUPPORT LONGCLICK EVENT
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    clickStarted: function (e, actionMethodName) {
      var Entry = this;

      // don't wait for click ending to run function, run it after 1 second if click wasn't interrupted in the meantime
      Entry.currentClickId = $$.uuid();
      var currentClickId = Entry.currentClickId;
      setTimeout(function () {
        if (Entry.currentClickId === currentClickId) Entry[actionMethodName](e);
      }, 1000);

    },

    clickEnded: function (e) {
      var Entry = this;
      delete Entry.currentClickId;
    },

    // also cancel running action method on mouse move, otherwise dragging an element still triggers longclick event
    mouseMove: function () {
      var Entry = this;
      delete Entry.currentClickId;
    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  GENERAL CLICK EVENT
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    /**
      DESCRIPTION: normal click event, check mode to figure out what to do
      ARGUMENTS: ( e <event> )
      RETURN: <void>
    */
    generalClickEvent: function (e) {
      var Entry = this;
      if (e) e.stopPropagation()
      if (Entry.App.isSpecialMode) Entry.App.select(Entry.model.id, undefined, true)
      else Entry.select(e);
    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  SPECIAL MODES
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    /**
      DESCRIPTION:
        activate special mode which let's the user:
          - select entries by simply clicking them
          - reorder entries by dragging (in manual lists or free displays)
      ARGUMENTS: ( e <event> )
      RETURN: <void>
    */
    enterOrganizeSpecialMode: function (e) {
      var Entry = this;
      if (Entry.App.manualSortingIsEnabled) Entry.App.enterSpecialMode("organize");
    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  METHODS
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    //
    //                              EDIT ENTRY

    edit: function (e) {
      var Entry = this;
      if (!Entry.App.config.disableEditing) Entry.App.openEntryEditor(Entry.model);
    },

    //
    //                              OPEN ENTRY

    open: function (e) {
      var Entry = this;
      e.stopPropagation()
      Entry.App.openEntry(Entry.model, undefined, e);
    },

    //
    //                              SELECT ENTRY

    select: function (e) {
      var Entry = this;
      if (!e) e = { stopPropagation: function () {} };
      e.stopPropagation()
      var entryId = Entry.model.id;
      // Entry.isCurrentlySelected = true; // to enable this, make sure to implement it fully

      // with shift select all items in between
      if (e.shiftKey) Entry.App.addToSelection(entryId)
      // with ctrl, add clicked entry to selection
      else if (e.ctrlKey) Entry.App.select(entryId, undefined, true)
      // simple click just select given one
      else Entry.App.select(entryId);

    },

    // unselect: function () {
    //   var Entry = this;
    //   Entry.isCurrentlySelected = false;
    //   Entry.App.removeFromSelection(Entry.model.id);
    // },

    // toggleSelection: function () {
    //   var Entry = this;
    //   if (Entry.isCurrentlySelected) Entry.select()
    //   else Entry.unselect();
    // },

    //                              ¬
    //

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  };

  // form list of valid actions
  var isValidAction = _.without(_.keys(entryMethods), "tagName", "initialize", "getEvents");

  // return list of methods
  return entryMethods;

};
