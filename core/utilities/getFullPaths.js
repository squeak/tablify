var _ = require("underscore");
var $$ = require("squeak");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION:
    get the list of (path + name +"/") for an entry, in other words, the paths an element within this folder has
    list path of parents recursively, and make up the list of all paths with which this folder's entries can be accessed
    when there are no parents, the root path id should be (without the quotes): "_/"
  ARGUMENTS: (
    !Collection <backboneCollection>,
    !entry <couchEntry> « pass the entry/folder to get paths from »,
    ¡alreadyPassedBy <string[]> « INTERNAL, don't set this! It's used internally to make sure loop paths are ignored  »,
  )
  RETURN: <string[]> « array of paths »
  EXAMPLE:
    getFullPaths({ path: ["_/", "someLongCOmplICatedUUID", "someOtherlongCompLiCATEDuuid"], name: "foobar", });
    // [ "/foobar/", "/elsewhere/foobar/", "/something/foobar/", "/deeper/something/foobar/" ]
    // in this example:
    //   - "someLongCOmplICatedUUID"       folder looks like this: { _id: "someLongCOmplICatedUUID",       name: "elsewhere", path: ["_/"],                                  }
    //   - "someOtherlongCompLiCATEDuuid"  folder looks like this: { _id: "someOtherlongCompLiCATEDuuid",  name: "something", path: ["_/", "yETanOTHerLongCOmplICatedUUID"], }
    //   - "yETanOTHerLongCOmplICatedUUID" folder looks like this: { _id: "yETanOTHerLongCOmplICatedUUID", name: "deeper",    path: ["_/"],                                  }
*/
function getFullPaths (Collection, entry, alreadyPassedBy) {
  var fullPaths = [];

  // initialize list of folders that are already a part of the paths searched for
  if (!alreadyPassedBy) alreadyPassedBy = [];

  // make sure the path hasn't already been visited
  // if it hasn't, add it to list of visited ones
  if (_.indexOf(alreadyPassedBy, entry._id) == -1) alreadyPassedBy.push(entry._id)
  // if it has, just stop recursing and return no paths
  else return [];

  _.each(entry.path, function (parentId) {
    var parentFullPaths;

    // parent folder is root
    if (parentId == "_/") parentFullPaths = ["/"]
    else {
      var parentModel = Collection.get(parentId);
      // can't find parent folder
      if (!parentModel) $$.log.error("Could not find parent model for path id: "+ parentId)
      // get paths from parent folder
      else parentFullPaths = getFullPaths(Collection, parentModel.attributes, _.clone(alreadyPassedBy)); // the cloning of "alreadyPassedBy" is very important, so that every subpath has it's own thread of comparison if an entry is already present in it
    };

    // add paths with this parent
    _.each(parentFullPaths, function (path) {
      fullPaths.push(path + entry.name +"/")
    });

  });

  return _.uniq(fullPaths);
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = getFullPaths;
