var _ = require("underscore");
var $$ = require("squeak");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DEFAULT TYPE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function getDefaultType () {
  return {
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    //
    //                              DISPLAY

    display: {
      _recursiveOption: true,
      title: "name",
    },

    //
    //                              ACTIONS

    action: {
      _recursiveOption: true,
      storeInHash: ["edit", "view"],
    },

    //
    //                              EVENTS

    event: {
      _recursiveOption: true,
    },

    //
    //                              EDITION

    edit: {
      _recursiveOption: true,
      structure: [
        {
          key: "type",
          inputifyType: "normal",
        },
        {
          key: "name",
          inputifyType: "normal",
        },
        {
          key: "path",
          inputifyType: "paths",
        },
      ],
    },

    //
    //                              VIEW

    view: {
      _recursiveOption: true,
      fields: [
        {
          display: function (entry, $fieldValue, $field, deepKey) {
            $fieldValue.div({
              html: [
                "This is the default view display, you can customize view for this type of entries editing it's type.",
                "",
                'To do this, click the <span class="icon-sliders"></span> button in the top of this window, and then edit the "view" section in the window that will open.',
              ].join("<br>"),
            }).css({
              padding: "0.8em",
              textAlign: "center",
            });
          },
        },
        { deepKey: "type", labelLayout: "inline", },
        { deepKey: "name", labelLayout: "inline", },
        { deepKey: "path", labelLayout: "inline", },
      ],
    },

    //                              ¬
    //

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  };
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (type, appConfig) {
  if (appConfig.entryTypes && appConfig.entryTypes.length == 1) var modelType = appConfig.entryTypes[0]
  else var modelType = _.findWhere(appConfig.entryTypes, { name: type, });
  var fullType = $$.defaults(getDefaultType(), appConfig.defaultType, modelType);
  // no modelType was found, specify that the return type is the default one
  if (!modelType) fullType.isDefaultType = true;
  // return full type
  return fullType;
};
