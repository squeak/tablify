var _ = require("underscore");
var $$ = require("squeak");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION:
    get the full list of children to remove when removing a given folder
    this function will get the list of children entries to remove, but if a child is a folder, it will also recursively get the list of children entries...
  ARGUMENTS: (
    !Collection <backboneCollection> « where to look for entries that could be affected »,
    !folderEntryToRemove <couchEntry> « the model of then entry that will be removed »,
    ¡toRemoveAndModify <see return type> « INTERNAL !!! this should be undefined, or this function will fail to return what you want !!! »,
  )
  RETURN: <{
    removedChecked: <string[]> « for internal usage: list of ids of removed entries that have already been processed by this function (useful to avoid endless loops in case some paths produce endless loops) »,
    toRemove: <backboneModel[]> « the list of models of entries that will have to be removed »,
    toModify: <{
      [ idOfModelThatShouldBeModifed <string> ]: <{
        pathIdsToRemove: <string[]> « the list of ids that will have to be removed in the entry's path »,
        pathIdsLeft: <string[]> « the list of ids that will remain in this entry's path after modification (useful for internal processing) »,
        model: <backboneModel> « just a references to the model of this entry »,
      }>
    }>
  }>
  EXAMPLES:
    The following examples are an attempt to demonstrate the versatility of this function when it comes to handling complex or even absurd path organnization graphs.
    To see those examples in action (and some others), you can execute the /tests/getAffectedChildren script.

    _EXAMPLE 1_: advanced use cases
    Let's say we have the following list of entries in a database:
      [
        { _id: "id1",  type: "folder", path: [ "id3",          ] },
        { _id: "id27", type: "folder", path: [ "_/",           ] },
        { _id: "id2",  type: "folder", path: [ "id27",         ] },
        { _id: "id3",  type: "folder", path: [ "id2",  "id27", ] },
        { _id: "id4",  type: "folder", path: [ "id1",  "id3",  ] },
        { _id: "id5",  type: "folder", path: [ "_/",   "id4",  ] },
      ]
    The user asked to remove entry "id27".
    To check affected children, the method will do the following:
      (1) create the result object described previously
      (2) add "id27" to the "removedChecked" list
      (3) find all entries that have "id27" in their path
      (4) fill the result object accordingly:
          - for each entry in which the path should be removed:
            + create a "toModify·modificationObject" object for this entry id (if it doesn't exist already)
            + add "id27" to list of "pathIdsToRemove"
            + remove "id27" to "pathIdsLeft" list
          - so in this case, at this stage, it will look like this:
          {
            removedChecked: [ "id27" ],
            toRemove: [],
            toModify: {
              "id2": {
                pathIdsToRemove: [ "id27" ],
                pathIdsLeft: [],
                model: { model of "id2" },
              },
              "id3": {
                pathIdsToRemove: [ "id27" ],
                pathIdsLeft: [ "id2" ],
                model: { model of "id3" },
              }
            }
          }
      (5) iterate "toModify" list and for each entry in which "pathIdsLeft" is empty
          - add it to "toRemove" list
          - remove it from "toModify" list
          - if it's a folder, execute the whole method on it: this means it will restart all steps from (2) to (5) for each removed folder recursively, additionnally populating the result object
          - at this stage the result object will sucessively look like this:
          {
            removedChecked: [ "id27", "id2" ],
            toRemove: [ { model of "id2" } ],
            toModify: {
              "id3": {
                pathIdsToRemove: [ "id27", "id2" ],
                pathIdsLeft: [],
                model: { model of "id3" },
              }
            }
          }
          then:
          {
            removedChecked: [ "id27", "id2", "id3" ],
            toRemove: [ { model of "id2" }, { model of "id3" } ],
            toModify: {
              "id1": {
                pathIdsToRemove: [ "id3" ],
                pathIdsLeft: [],
                model: { model of "id1" },
              },
              "id4": {
                pathIdsToRemove: [ "id3" ],
                pathIdsLeft: [ "id1" ],
                model: { model of "id4" },
              }
            }
          }
          then:
          {
            removedChecked: [ "id27", "id2", "id3", "id1" ],
            toRemove: [ { model of "id2" }, { model of "id3" }, { model of "id1" } ],
            toModify: {
              "id4": {
                pathIdsToRemove: [ "id3", "id1" ],
                pathIdsLeft: [],
                model: { model of "id4" },
              }
            }
          }
          then:
          {
            removedChecked: [ "id27", "id2", "id3", "id1", "id4" ],
            toRemove: [ { model of "id2" }, { model of "id3" }, { model of "id1" }, { model of "id4" } ],
            toModify: {
              "id5": {
                pathIdsToRemove: [ "id4" ],
                pathIdsLeft: [ "_/" ],
                model: { model of "id5" },
              }
            }
          }
          at this stage, there will be no entry in toModify that has to be removed.
      (6) the function can now return the result object:
          {
            removedChecked: [ "id27", "id2", "id3", "id1", "id4" ],
            toRemove: [ { model of "id2" }, { model of "id3" }, { model of "id1" }, { model of "id4" } ],
            toModify: {
              "id5": {
                pathIdsToRemove: [ "id4" ],
                pathIdsLeft: [ "_/" ],
                model: { model of "id5" },
              }
            }
          }
      _NOTES_:
        This is obviously a very complex example of tree, and it's very unlikely that someone organnizes a database this way.
        This example's goal is only to demonstrate the versatility of the function, and how it should support all types of structures.
        But wait there is more to come ;)

    _EXAMPLE 2_: absurd use cases? loops
    Consider the following list of entries:
    [
      { _id: "id1", type: "folder", path: [ "_/"         ] },
      { _id: "id2", type: "folder", path: [ "id1", "id3" ] },
      { _id: "id3", type: "folder", path: [ "id2"        ] },
    ]
    Let's say the user asks to remove "id2" entry.
    Same thing, to check affected children, the method will do the following:
      (1), (2), (3) behave as expected
      (4) the filled result will look like:
        {
          removedChecked: [ "id2" ],
          toRemove: [],
          toModify: {
            "id3": {
              pathIdsToRemove: [ "id2" ],
              pathIdsLeft: [],
              model: { model of "id3" },
            }
          }
        }
        then:
        {
          removedChecked: [ "id2", "id3" ],
          toRemove: [ { model of "id3" } ],
          toModify: {}
        }
        note here that "id2" hasn't been added to "toModify" list, because it's already in "removedChecked" list and is therefore ignored when looking for entries that could be modified
    (5) and (6) behave as expected
*/
function getAffectedChildren (Collection, folderEntryToRemove, toRemoveAndModify) {

  //
  //                              INITIALIZE

  // get/make result object
  var r = toRemoveAndModify || {
    removedChecked: [],
    toRemove: [],
    toModify: {},
  };

  // if this entry has already been done, just skip // maybe this is not useful, because the looping problem is solved when finding entries to modify, but let's leave it here ;)
  if (_.indexOf(r.removedChecked, folderEntryToRemove.id) != -1) return
  // continue adding this entry as checked for removal
  else r.removedChecked.push(folderEntryToRemove._id);

  //
  //                              CHECK ALL ENTRIES THAT SHOULD BE MODIFIED

  // check all collection to find models that should be removed or modified
  Collection.each(function (model) {
    var paths = model.get("path") || [];
    if (
      // if id of this model is present in path of entry
      _.indexOf(paths, folderEntryToRemove._id) != -1 &&
      // and entry hasn't already been marked for removal (useful for looping issues: e.g. A is in B that is in C that is in A...)
      _.indexOf(r.removedChecked, model.id) == -1
      // then this entry should be modified
    ) {
      // o            >            +            #            °            ·            ^            :            |


      // if the model was not yet in the list of entries to modify, add it
      if (!r.toModify[model.id]) r.toModify[model.id] = {
        pathIdsToRemove: [],
        pathIdsLeft: _.clone(paths),
        model: model,
      };

      // add the id of the current folder to remove in list of pathsIds to remove
      r.toModify[model.id].pathIdsToRemove.push(folderEntryToRemove._id);

      // remove the id of the current folder to remove from list of pathIds left
      r.toModify[model.id].pathIdsLeft = _.without(r.toModify[model.id].pathIdsLeft, folderEntryToRemove._id);

      // o            >            +            #            °            ·            ^            :            |
    };
  });

  //
  //                              BUILD LIST OF MODIFIED ENTRIES THAT SHOULD IN FACT BE REMOVED

  var additionalFoldersToRemove = [];

  _.each(r.toModify, function (modificationObject, idOfEntry) {
    // if last path of model has been removed
    if (!modificationObject.pathIdsLeft.length) {

      var model = r.toModify[idOfEntry].model;

      // add model to toRemove list and remove it from toModify list
      r.toRemove.push(model);
      delete r.toModify[idOfEntry];

      // if it's a folder, add it to list of folders to remove, for further inspections
      if (model.get("type") == "folder") additionalFoldersToRemove.push(model);

    };
  });

  //
  //                              CALCULATE COLATERALS OF REMOVING THOSE ADDITIONAL FOLDERS

  // this is each is separate from the previous one, because it's important that the r.toModify and r.toRemove list are fully up to date before starting calculating children modifications
  _.each(additionalFoldersToRemove, function (model) {
    // check for entries that could be affected by the deletion of this subfolder
     getAffectedChildren(Collection, model.attributes, r);
  });

  //
  //                              RETURN RESULT

  // return the final result (or partial result if it's in a sub-execution, but only final is useful)
  return r;

  //                              ¬
  //

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = getAffectedChildren;
